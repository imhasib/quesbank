package com.cn.quesbank.web.rest;

import com.cn.quesbank.QuesBankApp;

import com.cn.quesbank.domain.Question;
import com.cn.quesbank.repository.QuestionRepository;
import com.cn.quesbank.service.QuestionService;
import com.cn.quesbank.service.mapper.QuestionMapper;
import com.cn.quesbank.web.rest.errors.ExceptionTranslator;
import com.cn.quesbank.service.QuestionQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.cn.quesbank.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.cn.quesbank.domain.enumeration.Status;
/**
 * Test class for the QuestionResource REST controller.
 *
 * @see QuestionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuesBankApp.class)
public class QuestionResourceIntTest {

    private static final String DEFAULT_QUESTION = "AAAAAAAAAA";
    private static final String UPDATED_QUESTION = "BBBBBBBBBB";

    private static final String DEFAULT_ANSWERS = "[" +
        "   {" +
        "      \"answer\":\"Ans 1\"," +
        "      \"correct\":true" +
        "   }," +
        "   {" +
        "      \"answer\":\"Ans 2\"," +
        "      \"correct\":false" +
        "   }," +
        "   {" +
        "      \"answer\":\"Ans 3\"," +
        "      \"correct\":false" +
        "   }," +
        "   {" +
        "      \"answer\":\"Ans 4\"," +
        "      \"correct\":false" +
        "   }" +
        "]";
    private static final String UPDATED_ANSWERS = "[" +
        "   {" +
        "      \"answer\":\"Ans 11\"," +
        "      \"correct\":true" +
        "   }," +
        "   {" +
        "      \"answer\":\"Ans 12\"," +
        "      \"correct\":false" +
        "   }," +
        "   {" +
        "      \"answer\":\"Ans 13\"," +
        "      \"correct\":true" +
        "   }," +
        "   {" +
        "      \"answer\":\"Ans 14\"," +
        "      \"correct\":false" +
        "   }" +
        "]";

    private static final Status DEFAULT_STATUS = Status.C;
    private static final Status UPDATED_STATUS = Status.V;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private QuestionQueryService questionQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private QuestionMapper questionMapper;

    @Autowired
    private EntityManager em;

    private MockMvc restQuestionMockMvc;

    private Question question;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final QuestionResource questionResource = new QuestionResource(questionService, questionQueryService, questionMapper);
        this.restQuestionMockMvc = MockMvcBuilders.standaloneSetup(questionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Question createEntity(EntityManager em) {
        Question question = new Question()
            .question(DEFAULT_QUESTION)
            .answers(DEFAULT_ANSWERS)
            .status(DEFAULT_STATUS);
        return question;
    }

    @Before
    public void initTest() {
        question = createEntity(em);
    }

    @Test
    @Transactional
    public void createQuestion() throws Exception {
        int databaseSizeBeforeCreate = questionRepository.findAll().size();

        // Create the Question
        restQuestionMockMvc.perform(post("/api/questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionMapper.toDto(question))))
            .andExpect(status().isCreated());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeCreate + 1);
        Question testQuestion = questionList.get(questionList.size() - 1);
        assertThat(testQuestion.getQuestion()).isEqualTo(DEFAULT_QUESTION);
        assertThat(testQuestion.getAnswers()).isNotEmpty();
        assertThat(testQuestion.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createQuestionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = questionRepository.findAll().size();

        // Create the Question with an existing ID
        question.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuestionMockMvc.perform(post("/api/questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(question)))
            .andExpect(status().isBadRequest());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkQuestionIsRequired() throws Exception {
        int databaseSizeBeforeTest = questionRepository.findAll().size();
        // set the field null
        question.setQuestion(null);

        // Create the Question, which fails.

        restQuestionMockMvc.perform(post("/api/questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(question)))
            .andExpect(status().isBadRequest());

        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAnswersIsRequired() throws Exception {
        int databaseSizeBeforeTest = questionRepository.findAll().size();
        // set the field null
        question.setAnswers(null);

        // Create the Question, which fails.

        restQuestionMockMvc.perform(post("/api/questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(question)))
            .andExpect(status().isBadRequest());

        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = questionRepository.findAll().size();
        // set the field null
        question.setStatus(null);

        // Create the Question, which fails.

        restQuestionMockMvc.perform(post("/api/questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(question)))
            .andExpect(status().isBadRequest());

        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllQuestions() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        // Get all the questionList
        restQuestionMockMvc.perform(get("/api/questions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(question.getId().intValue())))
            .andExpect(jsonPath("$.[*].question").value(hasItem(DEFAULT_QUESTION.toString())))
            .andExpect(jsonPath("$.[*].answers").isArray())
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getQuestion() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        // Get the question
        restQuestionMockMvc.perform(get("/api/questions/{id}", question.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(question.getId().intValue()))
            .andExpect(jsonPath("$.question").value(DEFAULT_QUESTION.toString()))
            .andExpect(jsonPath("$.answers").isArray())
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getAllQuestionsByQuestionIsEqualToSomething() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        // Get all the questionList where question equals to DEFAULT_QUESTION
        defaultQuestionShouldBeFound("question.equals=" + DEFAULT_QUESTION);

        // Get all the questionList where question equals to UPDATED_QUESTION
        defaultQuestionShouldNotBeFound("question.equals=" + UPDATED_QUESTION);
    }

    @Test
    @Transactional
    public void getAllQuestionsByQuestionIsInShouldWork() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        // Get all the questionList where question in DEFAULT_QUESTION or UPDATED_QUESTION
        defaultQuestionShouldBeFound("question.in=" + DEFAULT_QUESTION + "," + UPDATED_QUESTION);

        // Get all the questionList where question equals to UPDATED_QUESTION
        defaultQuestionShouldNotBeFound("question.in=" + UPDATED_QUESTION);
    }

    @Test
    @Transactional
    public void getAllQuestionsByQuestionIsNullOrNotNull() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        // Get all the questionList where question is not null
        defaultQuestionShouldBeFound("question.specified=true");

        // Get all the questionList where question is null
        defaultQuestionShouldNotBeFound("question.specified=false");
    }

//    @Test
//    @Transactional
//    public void getAllQuestionsByAnswersIsEqualToSomething() throws Exception {
//        // Initialize the database
//        questionRepository.saveAndFlush(question);
//
//        // Get all the questionList where answers equals to DEFAULT_ANSWERS
//        defaultQuestionShouldBeFound("answers.equals=" + DEFAULT_ANSWERS);
//
//        // Get all the questionList where answers equals to UPDATED_ANSWERS
//        defaultQuestionShouldNotBeFound("answers.equals=" + UPDATED_ANSWERS);
//    }

//    @Test
//    @Transactional
//    public void getAllQuestionsByAnswersIsInShouldWork() throws Exception {
//        // Initialize the database
//        questionRepository.saveAndFlush(question);
//
//        // Get all the questionList where answers in DEFAULT_ANSWERS or UPDATED_ANSWERS
//        defaultQuestionShouldBeFound("answers.in=" + DEFAULT_ANSWERS + "," + UPDATED_ANSWERS);
//
//        // Get all the questionList where answers equals to UPDATED_ANSWERS
//        defaultQuestionShouldNotBeFound("answers.in=" + UPDATED_ANSWERS);
//    }

    @Test
    @Transactional
    public void getAllQuestionsByAnswersIsNullOrNotNull() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        // Get all the questionList where answers is not null
        defaultQuestionShouldBeFound("answers.specified=true");

        // Get all the questionList where answers is null
        defaultQuestionShouldNotBeFound("answers.specified=false");
    }

    @Test
    @Transactional
    public void getAllQuestionsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        // Get all the questionList where status equals to DEFAULT_STATUS
        defaultQuestionShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the questionList where status equals to UPDATED_STATUS
        defaultQuestionShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllQuestionsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        // Get all the questionList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultQuestionShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the questionList where status equals to UPDATED_STATUS
        defaultQuestionShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllQuestionsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        questionRepository.saveAndFlush(question);

        // Get all the questionList where status is not null
        defaultQuestionShouldBeFound("status.specified=true");

        // Get all the questionList where status is null
        defaultQuestionShouldNotBeFound("status.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultQuestionShouldBeFound(String filter) throws Exception {
        restQuestionMockMvc.perform(get("/api/questions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(question.getId().intValue())))
            .andExpect(jsonPath("$.[*].question").value(hasItem(DEFAULT_QUESTION.toString())))
            .andExpect(jsonPath("$.[*].answers").isArray())
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultQuestionShouldNotBeFound(String filter) throws Exception {
        restQuestionMockMvc.perform(get("/api/questions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingQuestion() throws Exception {
        // Get the question
        restQuestionMockMvc.perform(get("/api/questions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
//
//    @Test
//    @Transactional
//    public void updateQuestion() throws Exception {
//        // Initialize the database
//        questionService.save(question);
//
//        int databaseSizeBeforeUpdate = questionRepository.findAll().size();
//
//        // Update the question
//        Question updatedQuestion = questionRepository.findOne(question.getId());
//        // Disconnect from session so that the updates on updatedQuestion are not directly saved in db
//        em.detach(updatedQuestion);
//        updatedQuestion
//            .question(UPDATED_QUESTION)
//            .answers(UPDATED_ANSWERS)
//            .status(UPDATED_STATUS);
//
//        restQuestionMockMvc.perform(put("/api/questions")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(questionMapper.toDto(updatedQuestion))))
//            .andExpect(status().isOk());
//
//        // Validate the Question in the database
//        List<Question> questionList = questionRepository.findAll();
//        assertThat(questionList).hasSize(databaseSizeBeforeUpdate);
//        Question testQuestion = questionList.get(questionList.size() - 1);
//        assertThat(testQuestion.getQuestion()).isEqualTo(UPDATED_QUESTION);
//        assertThat(testQuestion.getAnswers()).isNotEmpty();
//        assertThat(testQuestion.getStatus()).isEqualTo(UPDATED_STATUS);
//    }

    @Test
    @Transactional
    public void updateNonExistingQuestion() throws Exception {
        int databaseSizeBeforeUpdate = questionRepository.findAll().size();

        // Create the Question

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restQuestionMockMvc.perform(put("/api/questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionMapper.toDto(question))))
            .andExpect(status().isCreated());

        // Validate the Question in the database
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteQuestion() throws Exception {
//        // Initialize the database
//        questionService.save(question);

        int databaseSizeBeforeDelete = questionRepository.findAll().size();

        // Get the question
        restQuestionMockMvc.perform(delete("/api/questions/{id}", question.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Question> questionList = questionRepository.findAll();
        assertThat(questionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Question.class);
        Question question1 = new Question();
        question1.setId(1L);
        Question question2 = new Question();
        question2.setId(question1.getId());
        assertThat(question1).isEqualTo(question2);
        question2.setId(2L);
        assertThat(question1).isNotEqualTo(question2);
        question1.setId(null);
        assertThat(question1).isNotEqualTo(question2);
    }
}

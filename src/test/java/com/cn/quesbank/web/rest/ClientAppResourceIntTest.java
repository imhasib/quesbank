package com.cn.quesbank.web.rest;

import com.cn.quesbank.QuesBankApp;
import com.cn.quesbank.domain.ClientApp;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.repository.ClientAppRepository;
import com.cn.quesbank.service.ClientAppService;
import com.cn.quesbank.service.dto.ClientAppDTO;
import com.cn.quesbank.service.mapper.ClientAppMapper;
import com.cn.quesbank.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.cn.quesbank.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Test class for the ClientAppResource REST controller.
 *
 * @see ClientAppResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuesBankApp.class)
public class ClientAppResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Status DEFAULT_STATUS = Status.C;
    private static final Status UPDATED_STATUS = Status.C;

    @Autowired
    private ClientAppRepository clientAppRepository;

    @Autowired
    private ClientAppMapper clientAppMapper;

    @Autowired
    private ClientAppService clientAppService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClientAppMockMvc;

    private ClientApp clientApp;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientAppResource clientAppResource = new ClientAppResource(clientAppService);
        this.restClientAppMockMvc = MockMvcBuilders.standaloneSetup(clientAppResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientApp createEntity(EntityManager em) {
        ClientApp clientApp = new ClientApp()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .status(DEFAULT_STATUS);
        return clientApp;
    }

    @Before
    public void initTest() {
        clientApp = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientApp() throws Exception {
        int databaseSizeBeforeCreate = clientAppRepository.findAll().size();

        // Create the ClientApp
        ClientAppDTO clientAppDTO = clientAppMapper.toDto(clientApp);
        restClientAppMockMvc.perform(post("/api/client-apps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientAppDTO)))
            .andExpect(status().isCreated());

        // Validate the ClientApp in the database
        List<ClientApp> clientAppList = clientAppRepository.findAll();
        assertThat(clientAppList).hasSize(databaseSizeBeforeCreate + 1);
        ClientApp testClientApp = clientAppList.get(clientAppList.size() - 1);
        assertThat(testClientApp.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testClientApp.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testClientApp.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createClientAppWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientAppRepository.findAll().size();

        // Create the ClientApp with an existing ID
        clientApp.setId(1L);
        ClientAppDTO clientAppDTO = clientAppMapper.toDto(clientApp);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientAppMockMvc.perform(post("/api/client-apps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientAppDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientApp in the database
        List<ClientApp> clientAppList = clientAppRepository.findAll();
        assertThat(clientAppList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientAppRepository.findAll().size();
        // set the field null
        clientApp.setName(null);

        // Create the ClientApp, which fails.
        ClientAppDTO clientAppDTO = clientAppMapper.toDto(clientApp);

        restClientAppMockMvc.perform(post("/api/client-apps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientAppDTO)))
            .andExpect(status().isBadRequest());

        List<ClientApp> clientAppList = clientAppRepository.findAll();
        assertThat(clientAppList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientAppRepository.findAll().size();
        // set the field null
        clientApp.setStatus(null);

        // Create the ClientApp, which fails.
        ClientAppDTO clientAppDTO = clientAppMapper.toDto(clientApp);

        restClientAppMockMvc.perform(post("/api/client-apps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientAppDTO)))
            .andExpect(status().isBadRequest());

        List<ClientApp> clientAppList = clientAppRepository.findAll();
        assertThat(clientAppList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientApps() throws Exception {
        // Initialize the database
        clientAppRepository.saveAndFlush(clientApp);

        // Get all the clientAppList
        restClientAppMockMvc.perform(get("/api/client-apps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientApp.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getClientApp() throws Exception {
        // Initialize the database
        clientAppRepository.saveAndFlush(clientApp);

        // Get the clientApp
        restClientAppMockMvc.perform(get("/api/client-apps/{id}", clientApp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientApp.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingClientApp() throws Exception {
        // Get the clientApp
        restClientAppMockMvc.perform(get("/api/client-apps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientApp() throws Exception {
        // Initialize the database
        clientAppRepository.saveAndFlush(clientApp);
        int databaseSizeBeforeUpdate = clientAppRepository.findAll().size();

        // Update the clientApp
        ClientApp updatedClientApp = clientAppRepository.findOne(clientApp.getId());
        // Disconnect from session so that the updates on updatedClientApp are not directly saved in db
        em.detach(updatedClientApp);
        updatedClientApp
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .status(UPDATED_STATUS);
        ClientAppDTO clientAppDTO = clientAppMapper.toDto(updatedClientApp);

        restClientAppMockMvc.perform(put("/api/client-apps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientAppDTO)))
            .andExpect(status().isOk());

        // Validate the ClientApp in the database
        List<ClientApp> clientAppList = clientAppRepository.findAll();
        assertThat(clientAppList).hasSize(databaseSizeBeforeUpdate);
        ClientApp testClientApp = clientAppList.get(clientAppList.size() - 1);
        assertThat(testClientApp.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testClientApp.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testClientApp.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingClientApp() throws Exception {
        int databaseSizeBeforeUpdate = clientAppRepository.findAll().size();

        // Create the ClientApp
        ClientAppDTO clientAppDTO = clientAppMapper.toDto(clientApp);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClientAppMockMvc.perform(put("/api/client-apps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientAppDTO)))
            .andExpect(status().isCreated());

        // Validate the ClientApp in the database
        List<ClientApp> clientAppList = clientAppRepository.findAll();
        assertThat(clientAppList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClientApp() throws Exception {
        // Initialize the database
        clientAppRepository.saveAndFlush(clientApp);
        int databaseSizeBeforeDelete = clientAppRepository.findAll().size();

        // Get the clientApp
        restClientAppMockMvc.perform(delete("/api/client-apps/{id}", clientApp.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ClientApp> clientAppList = clientAppRepository.findAll();
        assertThat(clientAppList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientApp.class);
        ClientApp clientApp1 = new ClientApp();
        clientApp1.setId(1L);
        ClientApp clientApp2 = new ClientApp();
        clientApp2.setId(clientApp1.getId());
        assertThat(clientApp1).isEqualTo(clientApp2);
        clientApp2.setId(2L);
        assertThat(clientApp1).isNotEqualTo(clientApp2);
        clientApp1.setId(null);
        assertThat(clientApp1).isNotEqualTo(clientApp2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientAppDTO.class);
        ClientAppDTO clientAppDTO1 = new ClientAppDTO();
        clientAppDTO1.setId(1L);
        ClientAppDTO clientAppDTO2 = new ClientAppDTO();
        assertThat(clientAppDTO1).isNotEqualTo(clientAppDTO2);
        clientAppDTO2.setId(clientAppDTO1.getId());
        assertThat(clientAppDTO1).isEqualTo(clientAppDTO2);
        clientAppDTO2.setId(2L);
        assertThat(clientAppDTO1).isNotEqualTo(clientAppDTO2);
        clientAppDTO1.setId(null);
        assertThat(clientAppDTO1).isNotEqualTo(clientAppDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(clientAppMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(clientAppMapper.fromId(null)).isNull();
    }
}

package com.cn.quesbank.web.rest;

import com.cn.quesbank.QuesBankApp;
import com.cn.quesbank.domain.Article;
import com.cn.quesbank.repository.ArticleRepository;
import com.cn.quesbank.service.ArticleQueryService;
import com.cn.quesbank.service.ArticleService;
import com.cn.quesbank.service.dto.ArticleDTO;
import com.cn.quesbank.service.mapper.ArticleMapper;
import com.cn.quesbank.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.cn.quesbank.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ArticleResource REST controller.
 *
 * @see ArticleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuesBankApp.class)
public class ArticleResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_SUMMARY = "AAAAAAAAAA";
    private static final String UPDATED_SUMMARY = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final String DEFAULT_LINKS = "[" +
        "{\"title\":\"Link title\"," +
        "\"url\":\"https://localhost\"}" +
        "]";
    private static final String UPDATED_LINKS = "[" +
        "{" +
        "\"title\":\"Link title updated\"," +
        "\"url\":\"https://localhost/updated\"}" +
        "]";

    private static final String DEFAULT_TAGS = "[\"Physics\"]";
    private static final String UPDATED_TAGS = "[\"English\"]";

//    private static final Long DEFAULT_PARENT = 1L;
//    private static final Long UPDATED_PARENT = 2L;
//
//    private static final Long DEFAULT_ROOT = 1L;
//    private static final Long UPDATED_ROOT = 2L;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ArticleQueryService articleQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restArticleMockMvc;

    private Article article;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ArticleResource articleResource = new ArticleResource(articleService, articleQueryService);
        this.restArticleMockMvc = MockMvcBuilders.standaloneSetup(articleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Article createEntity(EntityManager em) {
        Article article = new Article()
            .title(DEFAULT_TITLE)
            .summary(DEFAULT_SUMMARY)
            .content(DEFAULT_CONTENT)
            .links(DEFAULT_LINKS)
            .tags(DEFAULT_TAGS);
//            .parent(DEFAULT_PARENT)
//            .root(DEFAULT_ROOT);
        return article;
    }

    @Before
    public void initTest() {
        article = createEntity(em);
    }

    @Test
    @Transactional
    public void createArticle() throws Exception {
        int databaseSizeBeforeCreate = articleRepository.findAll().size();

        // Create the Article
        ArticleDTO articleDTO = articleMapper.toDto(article);
        restArticleMockMvc.perform(post("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(articleDTO)))
            .andExpect(status().isCreated());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeCreate + 1);
        Article testArticle = articleList.get(articleList.size() - 1);
        assertThat(testArticle.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testArticle.getSummary()).isEqualTo(DEFAULT_SUMMARY);
        assertThat(testArticle.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testArticle.getLinks()).isEqualTo(DEFAULT_LINKS);
        assertThat(testArticle.getTags()).isEqualTo(DEFAULT_TAGS);
//        assertThat(testArticle.getParentId()).isEqualTo(DEFAULT_PARENT);
//        assertThat(testArticle.getRoot()).isEqualTo(DEFAULT_ROOT);
    }

    @Test
    @Transactional
    public void createArticleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = articleRepository.findAll().size();

        // Create the Article with an existing ID
        article.setId(1L);
        ArticleDTO articleDTO = articleMapper.toDto(article);

        // An entity with an existing ID cannot be created, so this API call must fail
        restArticleMockMvc.perform(post("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(articleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = articleRepository.findAll().size();
        // set the field null
        article.setTitle(null);

        // Create the Article, which fails.
        ArticleDTO articleDTO = articleMapper.toDto(article);

        restArticleMockMvc.perform(post("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(articleDTO)))
            .andExpect(status().isBadRequest());

        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContentIsRequired() throws Exception {
        int databaseSizeBeforeTest = articleRepository.findAll().size();
        // set the field null
        article.setContent(null);

        // Create the Article, which fails.
        ArticleDTO articleDTO = articleMapper.toDto(article);

        restArticleMockMvc.perform(post("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(articleDTO)))
            .andExpect(status().isBadRequest());

        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllArticles() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList
        restArticleMockMvc.perform(get("/api/articles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(article.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].links").isArray())
            .andExpect(jsonPath("$.[*].tags").isArray());
//            .andExpect(jsonPath("$.[*].parent").value(hasItem(DEFAULT_PARENT.intValue())))
//            .andExpect(jsonPath("$.[*].root").value(hasItem(DEFAULT_ROOT.intValue())));
    }

    @Test
    @Transactional
    public void getArticle() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get the article
        restArticleMockMvc.perform(get("/api/articles/{id}", article.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(article.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.summary").value(DEFAULT_SUMMARY.toString()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.links").isArray())
            .andExpect(jsonPath("$.tags").isArray());
//            .andExpect(jsonPath("$.parent").value(DEFAULT_PARENT.intValue()))
//            .andExpect(jsonPath("$.root").value(DEFAULT_ROOT.intValue()));
    }

    @Test
    @Transactional
    public void getAllArticlesByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where title equals to DEFAULT_TITLE
        defaultArticleShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the articleList where title equals to UPDATED_TITLE
        defaultArticleShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllArticlesByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultArticleShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the articleList where title equals to UPDATED_TITLE
        defaultArticleShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllArticlesByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where title is not null
        defaultArticleShouldBeFound("title.specified=true");

        // Get all the articleList where title is null
        defaultArticleShouldNotBeFound("title.specified=false");
    }

    @Test
    @Transactional
    public void getAllArticlesBySummaryIsEqualToSomething() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where summary equals to DEFAULT_SUMMARY
        defaultArticleShouldBeFound("summary.equals=" + DEFAULT_SUMMARY);

        // Get all the articleList where summary equals to UPDATED_SUMMARY
        defaultArticleShouldNotBeFound("summary.equals=" + UPDATED_SUMMARY);
    }

    @Test
    @Transactional
    public void getAllArticlesBySummaryIsInShouldWork() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where summary in DEFAULT_SUMMARY or UPDATED_SUMMARY
        defaultArticleShouldBeFound("summary.in=" + DEFAULT_SUMMARY + "," + UPDATED_SUMMARY);

        // Get all the articleList where summary equals to UPDATED_SUMMARY
        defaultArticleShouldNotBeFound("summary.in=" + UPDATED_SUMMARY);
    }

//    @Test
//    @Transactional
//    public void getAllArticlesBySummaryIsNullOrNotNull() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where summary is not null
//        defaultArticleShouldBeFound("summary.specified=true");
//
//        // Get all the articleList where summary is null
//        defaultArticleShouldNotBeFound("summary.specified=false");
//    }

    @Test
    @Transactional
    public void getAllArticlesByContentIsEqualToSomething() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where content equals to DEFAULT_CONTENT
        defaultArticleShouldBeFound("content.equals=" + DEFAULT_CONTENT);

        // Get all the articleList where content equals to UPDATED_CONTENT
        defaultArticleShouldNotBeFound("content.equals=" + UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void getAllArticlesByContentIsInShouldWork() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where content in DEFAULT_CONTENT or UPDATED_CONTENT
        defaultArticleShouldBeFound("content.in=" + DEFAULT_CONTENT + "," + UPDATED_CONTENT);

        // Get all the articleList where content equals to UPDATED_CONTENT
        defaultArticleShouldNotBeFound("content.in=" + UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void getAllArticlesByContentIsNullOrNotNull() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where content is not null
        defaultArticleShouldBeFound("content.specified=true");

        // Get all the articleList where content is null
        defaultArticleShouldNotBeFound("content.specified=false");
    }

    @Test
    @Transactional
    public void getAllArticlesByLinksIsEqualToSomething() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where links equals to DEFAULT_LINKS
        defaultArticleShouldBeFound("links.equals=" + DEFAULT_LINKS);

        // Get all the articleList where links equals to UPDATED_LINKS
        defaultArticleShouldNotBeFound("links.equals=" + UPDATED_LINKS);
    }

//    @Test
//    @Transactional
//    public void getAllArticlesByLinksIsInShouldWork() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where links in DEFAULT_LINKS or UPDATED_LINKS
//        defaultArticleShouldBeFound("links.in=" + DEFAULT_LINKS + "," + UPDATED_LINKS);
//
//        // Get all the articleList where links equals to UPDATED_LINKS
//        defaultArticleShouldNotBeFound("links.in=" + UPDATED_LINKS);
//    }

//    @Test
//    @Transactional
//    public void getAllArticlesByLinksIsNullOrNotNull() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where links is not null
//        defaultArticleShouldBeFound("links.specified=true");
//
//        // Get all the articleList where links is null
//        defaultArticleShouldNotBeFound("links.specified=false");
//    }

    @Test
    @Transactional
    public void getAllArticlesByTagsIsEqualToSomething() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where tags equals to DEFAULT_TAGS
        defaultArticleShouldBeFound("tags.equals=" + DEFAULT_TAGS);

        // Get all the articleList where tags equals to UPDATED_TAGS
        defaultArticleShouldNotBeFound("tags.equals=" + UPDATED_TAGS);
    }

    @Test
    @Transactional
    public void getAllArticlesByTagsIsInShouldWork() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where tags in DEFAULT_TAGS or UPDATED_TAGS
        defaultArticleShouldBeFound("tags.in=" + DEFAULT_TAGS + "," + UPDATED_TAGS);

        // Get all the articleList where tags equals to UPDATED_TAGS
        defaultArticleShouldNotBeFound("tags.in=" + UPDATED_TAGS);
    }

    @Test
    @Transactional
    public void getAllArticlesByTagsIsNullOrNotNull() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);

        // Get all the articleList where tags is not null
        defaultArticleShouldBeFound("tags.specified=true");

        // Get all the articleList where tags is null
        defaultArticleShouldNotBeFound("tags.specified=false");
    }

//    @Test
//    @Transactional
//    public void getAllArticlesByParentIsEqualToSomething() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where parent equals to DEFAULT_PARENT
//        defaultArticleShouldBeFound("parent.equals=" + DEFAULT_PARENT);
//
//        // Get all the articleList where parent equals to UPDATED_PARENT
//        defaultArticleShouldNotBeFound("parent.equals=" + UPDATED_PARENT);
//    }
//
//    @Test
//    @Transactional
//    public void getAllArticlesByParentIsInShouldWork() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where parent in DEFAULT_PARENT or UPDATED_PARENT
//        defaultArticleShouldBeFound("parent.in=" + DEFAULT_PARENT + "," + UPDATED_PARENT);
//
//        // Get all the articleList where parent equals to UPDATED_PARENT
//        defaultArticleShouldNotBeFound("parent.in=" + UPDATED_PARENT);
//    }

//    @Test
//    @Transactional
//    public void getAllArticlesByParentIsNullOrNotNull() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where parent is not null
//        defaultArticleShouldBeFound("parent.specified=true");
//
//        // Get all the articleList where parent is null
//        defaultArticleShouldNotBeFound("parent.specified=false");
//    }

//    @Test
//    @Transactional
//    public void getAllArticlesByParentIsGreaterThanOrEqualToSomething() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where parent greater than or equals to DEFAULT_PARENT
//        defaultArticleShouldBeFound("parent.greaterOrEqualThan=" + DEFAULT_PARENT);
//
//        // Get all the articleList where parent greater than or equals to UPDATED_PARENT
//        defaultArticleShouldNotBeFound("parent.greaterOrEqualThan=" + UPDATED_PARENT);
//    }
//
//    @Test
//    @Transactional
//    public void getAllArticlesByParentIsLessThanSomething() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where parent less than or equals to DEFAULT_PARENT
//        defaultArticleShouldNotBeFound("parent.lessThan=" + DEFAULT_PARENT);
//
//        // Get all the articleList where parent less than or equals to UPDATED_PARENT
//        defaultArticleShouldBeFound("parent.lessThan=" + UPDATED_PARENT);
//    }


//    @Test
//    @Transactional
//    public void getAllArticlesByRootIsEqualToSomething() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where root equals to DEFAULT_ROOT
//        defaultArticleShouldBeFound("root.equals=" + DEFAULT_ROOT);
//
//        // Get all the articleList where root equals to UPDATED_ROOT
//        defaultArticleShouldNotBeFound("root.equals=" + UPDATED_ROOT);
//    }
//
//    @Test
//    @Transactional
//    public void getAllArticlesByRootIsInShouldWork() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where root in DEFAULT_ROOT or UPDATED_ROOT
//        defaultArticleShouldBeFound("root.in=" + DEFAULT_ROOT + "," + UPDATED_ROOT);
//
//        // Get all the articleList where root equals to UPDATED_ROOT
//        defaultArticleShouldNotBeFound("root.in=" + UPDATED_ROOT);
//    }

//    @Test
//    @Transactional
//    public void getAllArticlesByRootIsNullOrNotNull() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where root is not null
//        defaultArticleShouldBeFound("root.specified=true");
//
//        // Get all the articleList where root is null
//        defaultArticleShouldNotBeFound("root.specified=false");
//    }

//    @Test
//    @Transactional
//    public void getAllArticlesByRootIsGreaterThanOrEqualToSomething() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where root greater than or equals to DEFAULT_ROOT
//        defaultArticleShouldBeFound("root.greaterOrEqualThan=" + DEFAULT_ROOT);
//
//        // Get all the articleList where root greater than or equals to UPDATED_ROOT
//        defaultArticleShouldNotBeFound("root.greaterOrEqualThan=" + UPDATED_ROOT);
//    }
//
//    @Test
//    @Transactional
//    public void getAllArticlesByRootIsLessThanSomething() throws Exception {
//        // Initialize the database
//        articleRepository.saveAndFlush(article);
//
//        // Get all the articleList where root less than or equals to DEFAULT_ROOT
//        defaultArticleShouldNotBeFound("root.lessThan=" + DEFAULT_ROOT);
//
//        // Get all the articleList where root less than or equals to UPDATED_ROOT
//        defaultArticleShouldBeFound("root.lessThan=" + UPDATED_ROOT);
//    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultArticleShouldBeFound(String filter) throws Exception {
        restArticleMockMvc.perform(get("/api/articles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(article.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].links").isArray())
            .andExpect(jsonPath("$.[*].tags").isArray());
//            .andExpect(jsonPath("$.[*].parent").value(hasItem(DEFAULT_PARENT.intValue())))
//            .andExpect(jsonPath("$.[*].root").value(hasItem(DEFAULT_ROOT.intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultArticleShouldNotBeFound(String filter) throws Exception {
        restArticleMockMvc.perform(get("/api/articles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingArticle() throws Exception {
        // Get the article
        restArticleMockMvc.perform(get("/api/articles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateArticle() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);
        int databaseSizeBeforeUpdate = articleRepository.findAll().size();

        // Update the article
        Article updatedArticle = articleRepository.findOne(article.getId());
        // Disconnect from session so that the updates on updatedArticle are not directly saved in db
        em.detach(updatedArticle);
        updatedArticle
            .title(UPDATED_TITLE)
            .summary(UPDATED_SUMMARY)
            .content(UPDATED_CONTENT)
            .links(UPDATED_LINKS)
            .tags(UPDATED_TAGS);
//            .parent(UPDATED_PARENT)
//            .root(UPDATED_ROOT);
        ArticleDTO articleDTO = articleMapper.toDto(updatedArticle);

        restArticleMockMvc.perform(put("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(articleDTO)))
            .andExpect(status().isOk());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeUpdate);
        Article testArticle = articleList.get(articleList.size() - 1);
        assertThat(testArticle.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testArticle.getSummary()).isEqualTo(UPDATED_SUMMARY);
        assertThat(testArticle.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testArticle.getLinks()).isEqualTo(UPDATED_LINKS);
        assertThat(testArticle.getTags()).isEqualTo(UPDATED_TAGS);
//        assertThat(testArticle.getParentId()).isEqualTo(UPDATED_PARENT);
//        assertThat(testArticle.getRoot()).isEqualTo(UPDATED_ROOT);
    }

    @Test
    @Transactional
    public void updateNonExistingArticle() throws Exception {
        int databaseSizeBeforeUpdate = articleRepository.findAll().size();

        // Create the Article
        ArticleDTO articleDTO = articleMapper.toDto(article);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restArticleMockMvc.perform(put("/api/articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(articleDTO)))
            .andExpect(status().isCreated());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteArticle() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(article);
        int databaseSizeBeforeDelete = articleRepository.findAll().size();

        // Get the article
        restArticleMockMvc.perform(delete("/api/articles/{id}", article.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Article.class);
        Article article1 = new Article();
        article1.setId(1L);
        Article article2 = new Article();
        article2.setId(article1.getId());
        assertThat(article1).isEqualTo(article2);
        article2.setId(2L);
        assertThat(article1).isNotEqualTo(article2);
        article1.setId(null);
        assertThat(article1).isNotEqualTo(article2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ArticleDTO.class);
        ArticleDTO articleDTO1 = new ArticleDTO();
        articleDTO1.setId(1L);
        ArticleDTO articleDTO2 = new ArticleDTO();
        assertThat(articleDTO1).isNotEqualTo(articleDTO2);
        articleDTO2.setId(articleDTO1.getId());
        assertThat(articleDTO1).isEqualTo(articleDTO2);
        articleDTO2.setId(2L);
        assertThat(articleDTO1).isNotEqualTo(articleDTO2);
        articleDTO1.setId(null);
        assertThat(articleDTO1).isNotEqualTo(articleDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(articleMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(articleMapper.fromId(null)).isNull();
    }
}

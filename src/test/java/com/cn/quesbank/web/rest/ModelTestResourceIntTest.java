package com.cn.quesbank.web.rest;

import com.cn.quesbank.QuesBankApp;

import com.cn.quesbank.domain.ModelTest;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.repository.ModelTestRepository;
import com.cn.quesbank.service.ModelTestService;
import com.cn.quesbank.service.dto.ModelTestDTO;
import com.cn.quesbank.service.mapper.ModelTestMapper;
import com.cn.quesbank.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.cn.quesbank.web.rest.TestUtil.sameInstant;
import static com.cn.quesbank.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.cn.quesbank.domain.enumeration.ModelTestType;
/**
 * Test class for the ModelTestResource REST controller.
 *
 * @see ModelTestResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuesBankApp.class)
public class ModelTestResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final ModelTestType DEFAULT_TYPE = ModelTestType.D;
    private static final ModelTestType UPDATED_TYPE = ModelTestType.W;

    private static final String DEFAULT_SUBJECTS = "AAAAAAAAAA";
    private static final String UPDATED_SUBJECTS = "BBBBBBBBBB";

    private static final Status DEFAULT_STATUS = Status.C;
    private static final Status UPDATED_STATUS = Status.V;

    private static final ZonedDateTime DEFAULT_EXPIRY_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_EXPIRY_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_AVAILABLE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_AVAILABLE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private ModelTestRepository modelTestRepository;

    @Autowired
    private ModelTestMapper modelTestMapper;

    @Autowired
    private ModelTestService modelTestService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restModelTestMockMvc;

    private ModelTest modelTest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ModelTestResource modelTestResource = new ModelTestResource(modelTestService);
        this.restModelTestMockMvc = MockMvcBuilders.standaloneSetup(modelTestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModelTest createEntity(EntityManager em) {
        ModelTest modelTest = new ModelTest()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .type(DEFAULT_TYPE)
            .subjects(DEFAULT_SUBJECTS)
            .status(DEFAULT_STATUS)
            .expiryDate(DEFAULT_EXPIRY_DATE)
            .availableDate(DEFAULT_AVAILABLE_DATE);
        return modelTest;
    }

    @Before
    public void initTest() {
        modelTest = createEntity(em);
    }

    @Test
    @Transactional
    public void createModelTest() throws Exception {
        int databaseSizeBeforeCreate = modelTestRepository.findAll().size();

        // Create the ModelTest
        ModelTestDTO modelTestDTO = modelTestMapper.toDto(modelTest);
        restModelTestMockMvc.perform(post("/api/model-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelTestDTO)))
            .andExpect(status().isCreated());

        // Validate the ModelTest in the database
        List<ModelTest> modelTestList = modelTestRepository.findAll();
        assertThat(modelTestList).hasSize(databaseSizeBeforeCreate + 1);
        ModelTest testModelTest = modelTestList.get(modelTestList.size() - 1);
        assertThat(testModelTest.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testModelTest.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testModelTest.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testModelTest.getSubjects()).isEqualTo(DEFAULT_SUBJECTS);
        assertThat(testModelTest.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testModelTest.getExpiryDate()).isEqualTo(DEFAULT_EXPIRY_DATE);
        assertThat(testModelTest.getAvailableDate()).isEqualTo(DEFAULT_AVAILABLE_DATE);
    }

    @Test
    @Transactional
    public void createModelTestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = modelTestRepository.findAll().size();

        // Create the ModelTest with an existing ID
        modelTest.setId(1L);
        ModelTestDTO modelTestDTO = modelTestMapper.toDto(modelTest);

        // An entity with an existing ID cannot be created, so this API call must fail
        restModelTestMockMvc.perform(post("/api/model-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelTestDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelTest in the database
        List<ModelTest> modelTestList = modelTestRepository.findAll();
        assertThat(modelTestList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelTestRepository.findAll().size();
        // set the field null
        modelTest.setType(null);

        // Create the ModelTest, which fails.
        ModelTestDTO modelTestDTO = modelTestMapper.toDto(modelTest);

        restModelTestMockMvc.perform(post("/api/model-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelTestDTO)))
            .andExpect(status().isBadRequest());

        List<ModelTest> modelTestList = modelTestRepository.findAll();
        assertThat(modelTestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelTestRepository.findAll().size();
        // set the field null
        modelTest.setStatus(null);

        // Create the ModelTest, which fails.
        ModelTestDTO modelTestDTO = modelTestMapper.toDto(modelTest);

        restModelTestMockMvc.perform(post("/api/model-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelTestDTO)))
            .andExpect(status().isBadRequest());

        List<ModelTest> modelTestList = modelTestRepository.findAll();
        assertThat(modelTestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkExpiryDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelTestRepository.findAll().size();
        // set the field null
        modelTest.setExpiryDate(null);

        // Create the ModelTest, which fails.
        ModelTestDTO modelTestDTO = modelTestMapper.toDto(modelTest);

        restModelTestMockMvc.perform(post("/api/model-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelTestDTO)))
            .andExpect(status().isBadRequest());

        List<ModelTest> modelTestList = modelTestRepository.findAll();
        assertThat(modelTestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllModelTests() throws Exception {
        // Initialize the database
        modelTestRepository.saveAndFlush(modelTest);

        // Get all the modelTestList
        restModelTestMockMvc.perform(get("/api/model-tests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modelTest.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].subjects").value(hasItem(DEFAULT_SUBJECTS.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].expiryDate").value(hasItem(sameInstant(DEFAULT_EXPIRY_DATE))))
            .andExpect(jsonPath("$.[*].availableDate").value(hasItem(sameInstant(DEFAULT_AVAILABLE_DATE))));
    }

    @Test
    @Transactional
    public void getModelTest() throws Exception {
        // Initialize the database
        modelTestRepository.saveAndFlush(modelTest);

        // Get the modelTest
        restModelTestMockMvc.perform(get("/api/model-tests/{id}", modelTest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(modelTest.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.subjects").value(DEFAULT_SUBJECTS.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.expiryDate").value(sameInstant(DEFAULT_EXPIRY_DATE)))
            .andExpect(jsonPath("$.availableDate").value(sameInstant(DEFAULT_AVAILABLE_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingModelTest() throws Exception {
        // Get the modelTest
        restModelTestMockMvc.perform(get("/api/model-tests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModelTest() throws Exception {
        // Initialize the database
        modelTestRepository.saveAndFlush(modelTest);
        int databaseSizeBeforeUpdate = modelTestRepository.findAll().size();

        // Update the modelTest
        ModelTest updatedModelTest = modelTestRepository.findOne(modelTest.getId());
        // Disconnect from session so that the updates on updatedModelTest are not directly saved in db
        em.detach(updatedModelTest);
        updatedModelTest
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .type(UPDATED_TYPE)
            .subjects(UPDATED_SUBJECTS)
            .status(UPDATED_STATUS)
            .expiryDate(UPDATED_EXPIRY_DATE)
            .availableDate(UPDATED_AVAILABLE_DATE);
        ModelTestDTO modelTestDTO = modelTestMapper.toDto(updatedModelTest);

        restModelTestMockMvc.perform(put("/api/model-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelTestDTO)))
            .andExpect(status().isOk());

        // Validate the ModelTest in the database
        List<ModelTest> modelTestList = modelTestRepository.findAll();
        assertThat(modelTestList).hasSize(databaseSizeBeforeUpdate);
        ModelTest testModelTest = modelTestList.get(modelTestList.size() - 1);
        assertThat(testModelTest.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testModelTest.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testModelTest.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testModelTest.getSubjects()).isEqualTo(UPDATED_SUBJECTS);
        assertThat(testModelTest.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testModelTest.getExpiryDate()).isEqualTo(UPDATED_EXPIRY_DATE);
        assertThat(testModelTest.getAvailableDate()).isEqualTo(UPDATED_AVAILABLE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingModelTest() throws Exception {
        int databaseSizeBeforeUpdate = modelTestRepository.findAll().size();

        // Create the ModelTest
        ModelTestDTO modelTestDTO = modelTestMapper.toDto(modelTest);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restModelTestMockMvc.perform(put("/api/model-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelTestDTO)))
            .andExpect(status().isCreated());

        // Validate the ModelTest in the database
        List<ModelTest> modelTestList = modelTestRepository.findAll();
        assertThat(modelTestList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteModelTest() throws Exception {
        // Initialize the database
        modelTestRepository.saveAndFlush(modelTest);
        int databaseSizeBeforeDelete = modelTestRepository.findAll().size();

        // Get the modelTest
        restModelTestMockMvc.perform(delete("/api/model-tests/{id}", modelTest.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ModelTest> modelTestList = modelTestRepository.findAll();
        assertThat(modelTestList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelTest.class);
        ModelTest modelTest1 = new ModelTest();
        modelTest1.setId(1L);
        ModelTest modelTest2 = new ModelTest();
        modelTest2.setId(modelTest1.getId());
        assertThat(modelTest1).isEqualTo(modelTest2);
        modelTest2.setId(2L);
        assertThat(modelTest1).isNotEqualTo(modelTest2);
        modelTest1.setId(null);
        assertThat(modelTest1).isNotEqualTo(modelTest2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelTestDTO.class);
        ModelTestDTO modelTestDTO1 = new ModelTestDTO();
        modelTestDTO1.setId(1L);
        ModelTestDTO modelTestDTO2 = new ModelTestDTO();
        assertThat(modelTestDTO1).isNotEqualTo(modelTestDTO2);
        modelTestDTO2.setId(modelTestDTO1.getId());
        assertThat(modelTestDTO1).isEqualTo(modelTestDTO2);
        modelTestDTO2.setId(2L);
        assertThat(modelTestDTO1).isNotEqualTo(modelTestDTO2);
        modelTestDTO1.setId(null);
        assertThat(modelTestDTO1).isNotEqualTo(modelTestDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(modelTestMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(modelTestMapper.fromId(null)).isNull();
    }
}

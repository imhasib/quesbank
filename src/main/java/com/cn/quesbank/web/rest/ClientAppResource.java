package com.cn.quesbank.web.rest;

import com.cn.quesbank.domain.ClientApp;
import com.cn.quesbank.security.AuthoritiesConstants;
import com.cn.quesbank.service.CategoryService;
import com.cn.quesbank.service.ClientAppQueryService;
import com.cn.quesbank.service.ClientAppService;
import com.cn.quesbank.service.QuestionService;
import com.cn.quesbank.service.dto.ClientAppCriteria;
import com.cn.quesbank.service.dto.ClientAppDTO;
import com.cn.quesbank.service.dto.ClientAppExportDTO;
import com.cn.quesbank.service.mapper.ClientAppMapper;
import com.cn.quesbank.web.rest.errors.BadRequestAlertException;
import com.cn.quesbank.web.rest.util.HeaderUtil;
import com.cn.quesbank.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientApp.
 */
@RestController
@RequestMapping("/api")
public class ClientAppResource {

    private final Logger log = LoggerFactory.getLogger(ClientAppResource.class);

    private static final String ENTITY_NAME = "clientApp";

    private final ClientAppService clientAppService;
    @Inject
    private ClientAppMapper clientAppMapper;
    @Inject
    private CategoryService categoryService;
    @Inject
    private QuestionService questionService;
    @Inject
    private ClientAppQueryService clientAppQueryService;


    public ClientAppResource(ClientAppService clientAppService) {
        this.clientAppService = clientAppService;
    }

    /**
     * POST  /client-apps : Create a new clientApp.
     *
     * @param clientAppDTO the clientAppDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientAppDTO, or with status 400 (Bad Request) if the clientApp has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-apps")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.CLIENT_APP_WRITE + "')")
    public ResponseEntity<ClientAppDTO> createClientApp(@Valid @RequestBody ClientAppDTO clientAppDTO) throws URISyntaxException {
        log.debug("REST request to save ClientApp : {}", clientAppDTO);
        if (clientAppDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientApp cannot already have an ID", ENTITY_NAME, "idexists");
        }

        ClientApp clientApp = clientAppMapper.toEntity(clientAppDTO);
        ClientAppDTO result = clientAppMapper.toDto(clientAppService.save(clientApp));

        return ResponseEntity.created(new URI("/api/client-apps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-apps : Updates an existing clientApp.
     *
     * @param clientAppDTO the clientAppDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientAppDTO,
     * or with status 400 (Bad Request) if the clientAppDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientAppDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-apps")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.CLIENT_APP_WRITE + "')")
    public ResponseEntity<ClientAppDTO> updateClientApp(@Valid @RequestBody ClientAppDTO clientAppDTO) throws URISyntaxException {
        log.debug("REST request to update ClientApp : {}", clientAppDTO);
        if (clientAppDTO.getId() == null) {
            return createClientApp(clientAppDTO);
        }

        Optional<ClientAppDTO> result = clientAppService.update(clientAppDTO);

        return ResponseUtil.wrapOrNotFound(result, HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientAppDTO.getId().toString()));
    }

    /**
     * GET  /client-apps : get all the clientApps.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clientApps in body
     */
    @GetMapping("/client-apps")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.CLIENT_APP_READ + "')")
    public ResponseEntity<List<ClientAppDTO>> getAllClientApps(Pageable pageable, ClientAppCriteria criteria) {
        log.debug("REST request to get a page of ClientApps");

        Page<ClientAppDTO> page = clientAppQueryService.findByCriteria(criteria, pageable).map(clientAppMapper::toDto);
//        Page<ClientAppDTO> page = clientAppService.findByOwner(pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-apps");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-apps/:id : get the "id" clientApp.
     *
     * @param id the id of the clientAppDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientAppDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-apps/{id}")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.CLIENT_APP_READ + "')")
    public ResponseEntity<ClientAppDTO> getClientApp(@PathVariable Long id) {
        log.debug("REST request to get ClientApp : {}", id);
        ClientApp clientApp = clientAppService.findOneWithSharedWith(id);
        ClientAppDTO clientAppDTO = clientAppMapper.toDtoWithSharedWith(clientApp);

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientAppDTO));
    }

    /**
     * DELETE  /client-apps/:id : delete the "id" clientApp.
     *
     * @param id the id of the clientAppDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-apps/{id}")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.CLIENT_APP_WRITE + "')")
    public ResponseEntity<Void> deleteClientApp(@PathVariable Long id) throws URISyntaxException {
        log.debug("REST request to delete ClientApp : {}", id);
        clientAppService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

//    @GetMapping("client-app/{categoryId}/questions")
//    @Timed
//    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.CLIENT_APP_READ + "')")
//    public ResponseEntity<List<QuestionDTO>> getQuestionList(Pageable pageable, @PathVariable Long categoryId) {
//        log.debug("REST request to get ClientApp : {}", categoryId);
//
//        Page<Question> pageOfQues = questionService.getQuestionsByCategoryId(pageable, categoryId);
//
//        questionMapper.setIdToTagMap(Utils.toMap(questionMapper.margeAllTagsFromQuestions(pageOfQues.getContent())));
//
//        Page<QuestionDTO> page = pageOfQues.map(question -> questionMapper.toDto(question));
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/clientApps");
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
//    }

    @GetMapping("client-app/{id}/export-questions")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.CLIENT_APP_WRITE + "')")
    public ResponseEntity<InputStreamResource> exportQuestionList(@PathVariable Long id) throws IOException {
        log.debug("REST request to export all Questions of a ClientApp : {}", id);

        ClientApp clientApp = clientAppService.findOne(id);
        ClientAppExportDTO dto = clientAppService.prepareClientAppData(clientApp);

        byte[] buf = new byte[0];
        ObjectMapper mapper = new ObjectMapper();
        try {
            buf = mapper.writeValueAsBytes(dto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        String fileName = clientApp.getPackageName() + ".json";

        return ResponseEntity
            .ok()
            .contentLength(buf.length)
            .contentType(MediaType.parseMediaType("application/octet-stream; charset=utf-8"))
            .header("Content-Disposition", "attachment; filename=" + fileName)
            .header("Content-Transfer-Encoding", "binary")
            .body(new InputStreamResource(new ByteArrayInputStream(buf)));
    }
}

package com.cn.quesbank.web.rest;

import com.cn.quesbank.config.Constants;
import com.cn.quesbank.domain.Category;
import com.cn.quesbank.domain.Tag;
import com.cn.quesbank.domain.enumeration.CategoryType;
import com.cn.quesbank.security.AuthoritiesConstants;
import com.cn.quesbank.service.CategoryQueryService;
import com.cn.quesbank.service.CategoryService;
import com.cn.quesbank.service.TagService;
import com.cn.quesbank.service.dto.CategoryCriteria;
import com.cn.quesbank.service.dto.CategoryDTO;
import com.cn.quesbank.service.dto.QuestionDTO;
import com.cn.quesbank.service.mapper.CategoryMapper;
import com.cn.quesbank.service.util.Utils;
import com.cn.quesbank.web.rest.errors.BadRequestAlertException;
import com.cn.quesbank.web.rest.errors.ParentIdNotFoundException;
import com.cn.quesbank.web.rest.inout.CategoryAddQuestionsInput;
import com.cn.quesbank.web.rest.util.HeaderUtil;
import com.cn.quesbank.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing Category.
 */
@RestController
@RequestMapping("/api")
public class CategoryResource {
    private final Logger log = LoggerFactory.getLogger(CategoryResource.class);

    private static final String ENTITY_NAME = "category";
    private CategoryService categoryService;
    private CategoryQueryService categoryQueryService;

    @Inject
    private CategoryMapper categoryMapper;
    @Inject
    private TagService tagService;

    public CategoryResource(CategoryService categoryService, CategoryQueryService categoryQueryService) {
        this.categoryService = categoryService;
        this.categoryQueryService = categoryQueryService;
    }

    /**
     * POST  /categories : Create a new category.
     *
     * @param categoryDTO the categoryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new categoryDTO, or with status 400 (Bad Request) if the category has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/categories")
    @Timed
    public ResponseEntity<CategoryDTO> createCategory(@Valid @RequestBody CategoryDTO categoryDTO) throws URISyntaxException {
        log.debug("REST request to save Category : {}", categoryDTO);
        if (categoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new category cannot already have an ID", ENTITY_NAME, "idexists");
        }

        if (categoryDTO.getParentId() == null) {
            throw new ParentIdNotFoundException(ENTITY_NAME);
        }

        CategoryDTO result = categoryService.save(categoryDTO);
        return ResponseEntity.created(new URI("/api/categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /categories : Updates an existing category.
     *
     * @param categoryDTO the categoryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated categoryDTO,
     * or with status 400 (Bad Request) if the categoryDTO is not valid,
     * or with status 500 (Internal Server Error) if the categoryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/categories")
    @Timed
    public ResponseEntity<CategoryDTO> updateCategory(@Valid @RequestBody CategoryDTO categoryDTO) throws URISyntaxException {
        log.debug("REST request to update Category : {}", categoryDTO);
        if (categoryDTO.getId() == null) {
            return createCategory(categoryDTO);
        }

        Optional<CategoryDTO> result = categoryService.update(categoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, categoryDTO.getId().toString()))
            .body(result.get());
    }

    /**
     * GET  /categories : get all the categories.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of categories in body
     */
    @GetMapping("/categories")
    @Timed
    public ResponseEntity<List<CategoryDTO>> getAllCategories(CategoryCriteria criteria, Pageable pageable) throws URISyntaxException {
        log.debug("REST request to get Categories by criteria: {}", criteria);

        Page<Category> pageOfCat = categoryQueryService.findByCriteria(criteria, pageable);
        categoryMapper.setIdToTagMap(Utils.toMap(categoryMapper.margeAllTagsFromCategory(pageOfCat.getContent())));
        Page<CategoryDTO> page = categoryQueryService.findByCriteria(criteria, pageable).map(categoryMapper::toDtoWithTags);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /categories/:id : get the "id" category.
     *
     * @param id the id of the categoryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the categoryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/categories/{id}")
    @Timed
    public ResponseEntity<CategoryDTO> getCategory(@PathVariable Long id) throws URISyntaxException, IOException {
        log.debug("REST request to get Category : {}", id);
        Category category = categoryService.findOne(id);

        Set<Long> tagIds = Utils.parseTagIds(category.getTags());

        CategoryDTO categoryDTO = new CategoryDTO(category);
        Set<Tag> tags = tagService.findTagsByIds(tagIds);
        categoryDTO.setTags(tags);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(categoryDTO));
    }

    /**
     * DELETE  /categories/:id : delete the "id" category.
     *
     * @param id the id of the categoryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/categories/{id}")
    @Timed
    public ResponseEntity<Void> deleteCategory(@PathVariable Long id) {
        log.debug("REST request to delete Category : {}", id);
        categoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();

    }

    /**
     * GET get category ancestors
     *
     * @param id
     * @return
     */
    @GetMapping("/categories/{id}/path")
    @Timed
    public ResponseEntity<List<CategoryDTO>> getPath(@PathVariable Long id) {
        log.debug("REST request to get Category tree: {}", id);
        Category category = categoryService.findOne(id);
        String pathStr = category.getPath();
        if (CategoryType.CA.equals(category.getType())) {
            pathStr = pathStr.replace(Constants.CATEGORY_DEFAULT_CLIENT_APP_PATH, "");
        }

        Set<Long> ids = Utils.parseCategoryIds(pathStr);
        Set<Category> path = categoryService.findCategoriesByIds(ids);
        List<CategoryDTO> categoryDTOS = categoryMapper.toDtos(path);

        return new ResponseEntity<>(categoryDTOS, HttpStatus.OK);
    }

    /**
     * GET  /categories/count : count all the categories.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the count in body
     */
    @GetMapping("/categories/count")
    @Timed
    public ResponseEntity<Long> countCategories(CategoryCriteria criteria) {
        log.debug("REST request to count Categories by criteria: {}", criteria);
        return ResponseEntity.ok().body(categoryQueryService.countByCriteria(criteria));
    }

    @PostMapping("/category/add-questions")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.CLIENT_APP_READ + "')")
    public ResponseEntity<List<QuestionDTO>> addQuestionsToClientApp(@RequestBody CategoryAddQuestionsInput input) {
        Category clientApp = categoryService.addQuestions(input.getCategoryId(), input.getQuestionIds());

        HttpHeaders headers = HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientApp.getId().toString());
        return new ResponseEntity<>(headers, HttpStatus.OK);
    }
}

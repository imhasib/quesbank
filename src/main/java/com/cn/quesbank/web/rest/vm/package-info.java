/**
 * View Models used by Spring MVC REST controllers.
 */
package com.cn.quesbank.web.rest.vm;

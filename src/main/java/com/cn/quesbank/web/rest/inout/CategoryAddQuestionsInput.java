package com.cn.quesbank.web.rest.inout;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Hasib on 2019-03-02.
 */
public class CategoryAddQuestionsInput {
    @NotNull
    private Long categoryId;
    @NotNull
    private List<Long> questionIds;

    public CategoryAddQuestionsInput() {
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public List<Long> getQuestionIds() {
        return questionIds;
    }

    public void setQuestionIds(List<Long> questionIds) {
        this.questionIds = questionIds;
    }

    @Override
    public String toString() {
        return "CategoryAddQuestionsInput{" +
            "categoryId=" + categoryId +
            ", questionIds=" + questionIds +
            '}';
    }
}

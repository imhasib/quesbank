package com.cn.quesbank.web.rest.errors;

/**
 * Created by imhas on 2019-05-02.
 */
public class InvalidAnswerException extends BadRequestAlertException {

    public InvalidAnswerException(String entityName) {
        super("Invalid Answers.", entityName, "parent.id.not.found");
    }

    public InvalidAnswerException(String message, String entityName) {
        super(message, entityName, "parent.id.not.found");
    }
}

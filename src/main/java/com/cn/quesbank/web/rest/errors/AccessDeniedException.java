package com.cn.quesbank.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class AccessDeniedException extends BadRequestAlertException {

    public AccessDeniedException(String entityName) {
        super(ErrorConstants.ACCESS_DENIED_TYPE, "Access Denied", entityName, "error");
    }
}

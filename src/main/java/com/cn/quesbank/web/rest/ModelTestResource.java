package com.cn.quesbank.web.rest;

import com.cn.quesbank.domain.ModelTest;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.service.ModelTestQueryService;
import com.cn.quesbank.service.dto.*;
import com.cn.quesbank.service.mapper.ModelTestMapper;
import com.cn.quesbank.service.mapper.QuestionMapper;
import com.cn.quesbank.web.rest.inout.ModelTestAddQuestionsInput;
import com.codahale.metrics.annotation.Timed;
import com.cn.quesbank.service.ModelTestService;
import com.cn.quesbank.web.rest.errors.BadRequestAlertException;
import com.cn.quesbank.web.rest.util.HeaderUtil;
import com.cn.quesbank.web.rest.util.PaginationUtil;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ModelTest.
 */
@RestController
@RequestMapping("/api")
public class ModelTestResource {

    private final Logger log = LoggerFactory.getLogger(ModelTestResource.class);

    private static final String ENTITY_NAME = "modelTest";

    private final ModelTestService modelTestService;

    @Inject
    private ModelTestMapper modelTestMapper;

    @Inject
    private ModelTestQueryService modelTestQueryService;

    public ModelTestResource(ModelTestService modelTestService) {
        this.modelTestService = modelTestService;
    }

    /**
     * POST  /model-tests : Create a new modelTest.
     *
     * @param modelTestDTO the modelTestDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new modelTestDTO, or with status 400 (Bad Request) if the modelTest has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/model-tests")
    @Timed
    public ResponseEntity<ModelTestDTO> createModelTest(@Valid @RequestBody ModelTestDTO modelTestDTO) throws URISyntaxException {
        log.debug("REST request to save ModelTest : {}", modelTestDTO);
        if (modelTestDTO.getId() != null) {
            throw new BadRequestAlertException("A new modelTest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ModelTestDTO result = modelTestMapper.toDto(modelTestService.save(modelTestMapper.toEntity(modelTestDTO)));
        return ResponseEntity.created(new URI("/api/model-tests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /model-tests : Updates an existing modelTest.
     *
     * @param modelTestDTO the modelTestDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated modelTestDTO,
     * or with status 400 (Bad Request) if the modelTestDTO is not valid,
     * or with status 500 (Internal Server Error) if the modelTestDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/model-tests")
    @Timed
    public ResponseEntity<ModelTestDTO> updateModelTest(@Valid @RequestBody ModelTestDTO modelTestDTO) throws URISyntaxException {
        log.debug("REST request to update ModelTest : {}", modelTestDTO);
        if (modelTestDTO.getId() == null) {
            return createModelTest(modelTestDTO);
        }
        ModelTestDTO result = modelTestMapper.toDto(modelTestService.save(modelTestMapper.toEntity(modelTestDTO)));
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, modelTestDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /model-tests : get all the modelTests.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of modelTests in body
     */
    @GetMapping("/model-tests")
    @Timed
    public ResponseEntity<List<ModelTestDTO>> getAllModelTests(ModelTestCriteria criteria, Pageable pageable) {
        log.debug("REST request to get a page of ModelTests");
        Page<ModelTestDTO> page = modelTestQueryService.findByCriteria(criteria, pageable).map(modelTestMapper::toDto);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/model-tests");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /model-tests/:id : get the "id" modelTest.
     *
     * @param id the id of the modelTestDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the modelTestDTO, or with status 404 (Not Found)
     */
    @GetMapping("/model-tests/{id}")
    @Timed
    public ResponseEntity<ModelTestDTO> getModelTest(@PathVariable Long id) {
        log.debug("REST request to get ModelTest : {}", id);
        ModelTest modelTest = modelTestService.findOneWithQuestions(id);
        ModelTestDTO modelTestDTO = modelTestMapper.toDto(modelTest);
        modelTestDTO.setQuestionIds(QuestionDTOMin.toIds(modelTest.getQuestions()));
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(modelTestDTO));
    }

    /**
     * DELETE  /model-tests/:id : delete the "id" modelTest.
     *
     * @param id the id of the modelTestDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/model-tests/{id}")
    @Timed
    public ResponseEntity<Void> deleteModelTest(@PathVariable Long id) {
        log.debug("REST request to delete ModelTest : {}", id);
        modelTestService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/model-test/add-questions")
    @Timed
    public ResponseEntity<List<QuestionDTO>> addQuestionsToModelTest(@RequestBody ModelTestAddQuestionsInput input) {
        ModelTest modelTest = modelTestService.addQuestions(input.getModelTestId(), input.getQuestionIds());

        HttpHeaders headers = HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, modelTest.getId().toString());
        return new ResponseEntity<>(headers, HttpStatus.OK);
    }


    @GetMapping("/model-tests/deletedIds")
    @Timed
    public ResponseEntity<DeletedModelTestDTOMin> getAllDeletedQuestions(@RequestParam(required = false) ZonedDateTime lastModifiedDate, Pageable pageable) {
        log.debug("REST request to get ids of deleted Questions");

        ModelTestCriteria criteria = new ModelTestCriteria();
        ModelTestCriteria.StatusFilter statusFilter = new ModelTestCriteria.StatusFilter();
        statusFilter.setEquals(Status.D);
        criteria.setStatus(statusFilter);
        if(lastModifiedDate != null) {
            criteria.setLastModifiedDate(new InstantFilter().setGreaterThan(lastModifiedDate.toInstant()));
        }

        Page<Long> page = modelTestQueryService.findByCriteria(criteria, pageable).map(new Converter<ModelTest, Long>() {
            @Override
            public Long convert(ModelTest question) {
                return question.getId();
            }
        });

        DeletedModelTestDTOMin deletedModelTestDTOMin = new DeletedModelTestDTOMin();
        deletedModelTestDTOMin.setIds(page.getContent());

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/model-tests");
        return new ResponseEntity<>(deletedModelTestDTOMin, headers, HttpStatus.OK);
    }
}

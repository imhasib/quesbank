package com.cn.quesbank.web.rest.errors;

/**
 * Created by imhas on 2019-05-01.
 */
public class ParentIdNotFoundException extends BadRequestAlertException {

    public ParentIdNotFoundException(String entityName) {
        super("Parent not found.", entityName, "parent.id.not.found");
    }
}

package com.cn.quesbank.web.rest;

import com.cn.quesbank.domain.Question;
import com.cn.quesbank.domain.Tag;
import com.cn.quesbank.security.AuthoritiesConstants;
import com.cn.quesbank.service.QuestionQueryService;
import com.cn.quesbank.service.QuestionService;
import com.cn.quesbank.service.TagService;
import com.cn.quesbank.service.dto.QuestionCriteria;
import com.cn.quesbank.service.dto.QuestionDTO;
import com.cn.quesbank.service.mapper.QuestionMapper;
import com.cn.quesbank.service.util.Utils;
import com.cn.quesbank.web.rest.errors.BadRequestAlertException;
import com.cn.quesbank.web.rest.util.HeaderUtil;
import com.cn.quesbank.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing Question.
 */
@RestController
@RequestMapping("/api")
public class QuestionResource {

    private final Logger log = LoggerFactory.getLogger(QuestionResource.class);

    private static final String ENTITY_NAME = "question";

    @Inject
    private TagService tagService;
    @Inject
    private QuestionMapper questionMapper;
    private final QuestionService questionService;
    private final QuestionQueryService questionQueryService;

    public QuestionResource(QuestionService questionService, QuestionQueryService questionQueryService, QuestionMapper questionMapper) {
        this.questionService = questionService;
        this.questionQueryService = questionQueryService;
        this.questionMapper = questionMapper;
    }

    /**
     * POST  /questions : Create a new question.
     *
     * @param questionDTO the question to create
     * @return the ResponseEntity with status 201 (Created) and with body the new question, or with status 400 (Bad Request) if the question has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/questions")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.QUESTION_WRITE + "')")
    public ResponseEntity<QuestionDTO> createQuestion(@Valid @RequestBody QuestionDTO questionDTO) throws URISyntaxException {
        log.debug("REST request to save Question : {}", questionDTO);
        if (questionDTO.getId() != null) {
            throw new BadRequestAlertException("A new question cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Question question = questionService.save(questionDTO);
        QuestionDTO result = questionMapper.toDto(question);
        return ResponseEntity.created(new URI("/api/questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /questions : Updates an existing question.
     *
     * @param questionDTO the question to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated question,
     * or with status 400 (Bad Request) if the question is not valid,
     * or with status 500 (Internal Server Error) if the question couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/questions")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.QUESTION_WRITE + "')")
    public ResponseEntity<QuestionDTO> updateQuestion(@Valid @RequestBody QuestionDTO questionDTO) throws URISyntaxException {
        log.debug("REST request to update Question : {}", questionDTO);
        if (questionDTO.getId() == null) {
            return createQuestion(questionDTO);
        }

        Question question = questionService.save(questionDTO);
        QuestionDTO result = questionMapper.toDto(question);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * GET  /questions : get all the questions.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of questions in body
     */
    @GetMapping("/questions")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.QUESTION_READ + "')")
    public ResponseEntity<List<QuestionDTO>> getAllQuestions(QuestionCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Questions by criteria: {}", criteria);

        Page<Question> pageOfQues = questionQueryService.findByCriteria(criteria, pageable);

        questionMapper.setIdToTagMap(Utils.toMap(questionMapper.margeAllTagsFromQuestions(pageOfQues.getContent())));

        Page<QuestionDTO> page = pageOfQues.map(questionMapper::toDto);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/questions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /questions/:id : get the "id" question.
     *
     * @param id the id of the question to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the question, or with status 404 (Not Found)
     */
    @GetMapping("/questions/{id}")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.QUESTION_READ + "')")
    public ResponseEntity<QuestionDTO> getQuestion(@PathVariable Long id) throws IOException {
        log.debug("REST request to get Question : {}", id);
        Question question = questionService.findOne(id);
        QuestionDTO questionDTO = questionMapper.toDto(question);

        Set<Long> tagIds = Utils.parseTagIds(question.getTags());
        Set<Tag> tags = tagService.findTagsByIds(tagIds);
        questionDTO.setTags(tags);

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(questionDTO));
    }

    /**
     * DELETE  /questions/:id : delete the "id" question.
     *
     * @param id the id of the question to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/questions/{id}")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.QUESTION_DELETE + "')")
    public ResponseEntity<Void> deleteQuestion(@PathVariable Long id) throws URISyntaxException {
        log.debug("REST request to delete Question : {}", id);
        questionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}

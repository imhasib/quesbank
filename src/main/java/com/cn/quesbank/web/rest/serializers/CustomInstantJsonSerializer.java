package com.cn.quesbank.web.rest.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class CustomInstantJsonSerializer extends StdSerializer<Instant> {

    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    public CustomInstantJsonSerializer() {
        this(null);
    }

    protected CustomInstantJsonSerializer(Class<Instant> t) {
        super(t);
    }

    @Override
    public void serialize(Instant value, JsonGenerator gen, SerializerProvider arg2) throws IOException, JsonProcessingException {
        gen.writeString(formatter.format(Date.from(value)));
    }
}

package com.cn.quesbank.web.rest;

import com.cn.quesbank.domain.Category;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.service.CategoryQueryService;
import com.cn.quesbank.service.dto.CategoryCriteria;
import com.cn.quesbank.service.dto.CategoryDTOMin;
import com.cn.quesbank.service.dto.DeletedCategoriesDTOMin;
import com.cn.quesbank.service.util.Utils;
import com.cn.quesbank.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.service.filter.InstantFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/min")
public class CategoryResourceMin {

    private final Logger log = LoggerFactory.getLogger(CategoryResourceMin.class);

    private CategoryQueryService categoryQueryService;

    public CategoryResourceMin(CategoryQueryService categoryQueryService) {
        this.categoryQueryService = categoryQueryService;
    }

    /**
     * GET  /categories : get all the categories.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of categories in body
     */
    @GetMapping("/categories")
    @Timed
    public ResponseEntity<List<CategoryDTOMin>> getAllCategories(CategoryCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Categories by criteria: {}", criteria);
        Page<CategoryDTOMin> page = categoryQueryService.findByCriteria(criteria, pageable).map(new Converter<Category, CategoryDTOMin>() {
            @Override
            public CategoryDTOMin convert(Category category) {
                CategoryDTOMin dto = new CategoryDTOMin(category);

                Set<Long> tagIds = Utils.parseTagIds(category.getTags());
                dto.setTagIds(tagIds);

                return dto;
            }
        });
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("categories/deletedIds")
    @Timed
    public ResponseEntity<DeletedCategoriesDTOMin> getAllDeletedCategories(@RequestParam(required = false) ZonedDateTime lastModifiedDate, Pageable pageable) {
        log.debug("REST request to get ids of deleted Categories");

        CategoryCriteria criteria = new CategoryCriteria();
        CategoryCriteria.StatusFilter statusFilter = new CategoryCriteria.StatusFilter();
        statusFilter.setEquals(Status.D);
        criteria.setStatus(statusFilter);
        if(lastModifiedDate != null) {
            criteria.setLastModifiedDate(new InstantFilter().setGreaterThan(lastModifiedDate.toInstant()));
        }

        Page<Long> page = categoryQueryService.findByCriteria(criteria, pageable).map(new Converter<Category, Long>() {
            @Override
            public Long convert(Category category) {
                return category.getId();
            }
        });

        DeletedCategoriesDTOMin deletedCategoriesDTOMin = new DeletedCategoriesDTOMin();
        deletedCategoriesDTOMin.setIds(page.getContent());

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/categories");
        return new ResponseEntity<>(deletedCategoriesDTOMin, headers, HttpStatus.OK);
    }
}


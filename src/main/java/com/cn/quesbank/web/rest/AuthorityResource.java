package com.cn.quesbank.web.rest;

import com.cn.quesbank.domain.Authority;
import com.cn.quesbank.repository.AuthorityRepository;
import com.cn.quesbank.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import com.cn.quesbank.service.RoleService;
import com.cn.quesbank.web.rest.errors.BadRequestAlertException;
import com.cn.quesbank.web.rest.util.HeaderUtil;
import com.cn.quesbank.web.rest.util.PaginationUtil;
import com.cn.quesbank.service.dto.RoleDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Role.
 */
@RestController
@RequestMapping("/api")
public class AuthorityResource {

    private final Logger log = LoggerFactory.getLogger(AuthorityResource.class);
    private static final String ENTITY_NAME = "authority";
    @Inject
    private AuthorityRepository authorityRepository;

    @GetMapping("/authorities")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER_WRITE + "')")
    public ResponseEntity<List<Authority>> getAllRoles(Pageable pageable) {
        log.debug("REST request to get a page of Roles");
        Page<Authority> page = authorityRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/authorities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}


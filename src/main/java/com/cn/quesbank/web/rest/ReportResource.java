package com.cn.quesbank.web.rest;

import com.cn.quesbank.security.AuthoritiesConstants;
import com.cn.quesbank.service.ReportService;
import com.cn.quesbank.service.dto.ReportDTO;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ReportResource {

    private final Logger log = LoggerFactory.getLogger(ReportResource.class);

    @Inject
    private ReportService reportService;

    @GetMapping("/report")
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.APP_ADMINISTRATION + "')")
    public ResponseEntity<ReportDTO> getReport() {
        log.debug("REST request to get report");
        ReportDTO reportDTO = reportService.getRepot();


        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(reportDTO));
    }
}

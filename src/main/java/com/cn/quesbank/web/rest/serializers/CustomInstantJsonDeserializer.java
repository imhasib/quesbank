package com.cn.quesbank.web.rest.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class CustomInstantJsonDeserializer extends StdDeserializer<Instant> {

    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    protected CustomInstantJsonDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Instant deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Date date = null;
        try {
            date = formatter.parse(jsonParser.getText());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date != null ? date.toInstant() : null;
    }
}

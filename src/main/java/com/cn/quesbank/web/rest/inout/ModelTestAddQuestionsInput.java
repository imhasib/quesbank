package com.cn.quesbank.web.rest.inout;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Hasib on 2018-11-12.
 */
public class ModelTestAddQuestionsInput {
    @NotNull
    private Long modelTestId;
    @NotNull
    private List<Long> questionIds;

    public ModelTestAddQuestionsInput() {
    }

    public Long getModelTestId() {
        return modelTestId;
    }

    public void setModelTestId(Long modelTestId) {
        this.modelTestId = modelTestId;
    }

    public List<Long> getQuestionIds() {
        return questionIds;
    }

    public void setQuestionIds(List<Long> questionIds) {
        this.questionIds = questionIds;
    }

    @Override
    public String toString() {
        return "ModelTestAddQuestionsInput{" +
            "modelTestId=" + modelTestId +
            ", questionIds=" + questionIds +
            '}';
    }
}

package com.cn.quesbank.web.rest.errors;

/**
 * Created by imhas on 2019-05-03.
 */
public class DeleteCategoryException extends BadRequestAlertException {

    public DeleteCategoryException(String message, String entityName) {
        super(message, entityName, "category.delete.question.found.exception");
    }
}

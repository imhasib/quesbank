package com.cn.quesbank.web.rest;

import com.cn.quesbank.domain.Question;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.service.QuestionQueryService;
import com.cn.quesbank.service.QuestionService;
import com.cn.quesbank.service.dto.DeletedQuestionsDTOMin;
import com.cn.quesbank.service.dto.QuestionCriteria;
import com.cn.quesbank.service.dto.QuestionDTOMin;
import com.cn.quesbank.service.dto.QuestionListDTOMin;
import com.cn.quesbank.service.mapper.QuestionMapper;
import com.cn.quesbank.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.service.filter.InstantFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.time.ZonedDateTime;

/**
 * Created by Hasib on 2018-11-06.
 */
@RestController
@RequestMapping("/api/min")
public class QuestionResourceMin {

    private final Logger log = LoggerFactory.getLogger(QuestionResourceMin.class);

    private static final String ENTITY_NAME = "question";

    private final QuestionService questionService;

    @Inject
    private QuestionMapper questionMapper;

    private final QuestionQueryService questionQueryService;

    public QuestionResourceMin(QuestionService questionService, QuestionQueryService questionQueryService, QuestionMapper questionMapper) {
        this.questionService = questionService;
        this.questionQueryService = questionQueryService;
        this.questionMapper = questionMapper;
    }

    /**
     * GET  /questions : get all the questions.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of questions in body
     */
    @GetMapping("/questions")
    @Timed
    public ResponseEntity<QuestionListDTOMin> getAllQuestions(QuestionCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Questions by criteria: {}", criteria);
        Page<QuestionDTOMin> page = questionQueryService.findByCriteria(criteria, pageable).map(QuestionDTOMin::new);

        QuestionListDTOMin questionListDTOMin = new QuestionListDTOMin(page.getContent());

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/min/questions");
        return new ResponseEntity<>(questionListDTOMin, headers, HttpStatus.OK);
    }

    @GetMapping("questions/deletedIds")
    @Timed
    public ResponseEntity<DeletedQuestionsDTOMin> getAllDeletedQuestions(@RequestParam(required = false) ZonedDateTime lastModifiedDate, Pageable pageable) {
        log.debug("REST request to get ids of deleted Questions");

        QuestionCriteria criteria = new QuestionCriteria();
        QuestionCriteria.StatusFilter statusFilter = new QuestionCriteria.StatusFilter();
        statusFilter.setEquals(Status.D);
        criteria.setStatus(statusFilter);
        if(lastModifiedDate != null) {
            criteria.setLastModifiedDate(new InstantFilter().setGreaterThan(lastModifiedDate.toInstant()));
        }

        Page<Long> page = questionQueryService.findByCriteria(criteria, pageable).map(new Converter<Question, Long>() {
            @Override
            public Long convert(Question question) {
                return question.getId();
            }
        });

        DeletedQuestionsDTOMin deletedQuestionsDTOMin = new DeletedQuestionsDTOMin();
        deletedQuestionsDTOMin.setIds(page.getContent());

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/min/questions");
        return new ResponseEntity<>(deletedQuestionsDTOMin, headers, HttpStatus.OK);
    }
}

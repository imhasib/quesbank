package com.cn.quesbank.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ANONYMOUS = "anonymous";
    public static final String USER = "user";

    public static final String SYSTEM_ADMINISTRATION = "system.administration";
    public static final String APP_ADMINISTRATION = "app.administration";

    public static final String USER_READ = "user.read";
    public static final String USER_WRITE = "user.write";

    public static final String ROLE_READ = "role.read";
    public static final String ROLE_WRITE = "role.write";

    public static final String CLIENT_APP_ADMINISTRATION = "clientApp.administration";
    public static final String CLIENT_APP_READ = "clientApp.read";
    public static final String CLIENT_APP_WRITE = "clientApp.write";

    public static final String QUESTION_READ = "question.read";
    public static final String QUESTION_WRITE = "question.write";
    public static final String QUESTION_DELETE = "question.delete";


    private AuthoritiesConstants() {
    }
}

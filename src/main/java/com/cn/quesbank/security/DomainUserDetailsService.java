package com.cn.quesbank.security;

import com.cn.quesbank.domain.Authority;
import com.cn.quesbank.domain.Role;
import com.cn.quesbank.domain.User;
import com.cn.quesbank.repository.RoleRepository;
import com.cn.quesbank.repository.UserRepository;
import com.cn.quesbank.service.mapper.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.*;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class DomainUserDetailsService implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(DomainUserDetailsService.class);

    private final UserRepository userRepository;
    @Inject
    private UserMapper userMapper;

    public DomainUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) {
        log.debug("Authenticating {}", login);
        String lowercaseLogin = login.toLowerCase(Locale.ENGLISH);
        Optional<User> userFromDatabase = userRepository.findOneWithRolesByLoginOrEmail(lowercaseLogin, lowercaseLogin);
        return userFromDatabase.map(user -> {
            if (!user.getActivated()) {
                throw new UserNotActivatedException("User " + lowercaseLogin + " was not activated");
            }

            return new org.springframework.security.core.userdetails.User(user.getId().toString(),
                user.getPassword(),
                getGrantedAuthorities(userMapper.authoritiesFromRoles(user.getRoles())));
        }).orElseThrow(() -> new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the " +
        "database"));
    }

    private Set<GrantedAuthority> getGrantedAuthorities(Set<String> authorities) {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (String authority : authorities) {
            grantedAuthorities.add(new SimpleGrantedAuthority(authority));
        }
        return grantedAuthorities;
    }
}

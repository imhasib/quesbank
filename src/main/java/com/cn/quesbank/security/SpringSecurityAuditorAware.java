package com.cn.quesbank.security;

import com.cn.quesbank.config.Constants;
import com.cn.quesbank.domain.User;
import com.cn.quesbank.service.mapper.UserMapper;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Implementation of AuditorAware based on Spring Security.
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<User> {

    @Inject
    private UserMapper userMapper;

    @Override
    public User getCurrentAuditor() {
        Long requesterId = SecurityUtils.getCurrentUserId().orElse(Constants.SYSTEM_ACCOUNT_ID);

        return userMapper.userFromId(requesterId);
    }
}

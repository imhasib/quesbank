package com.cn.quesbank.domain;

import com.cn.quesbank.domain.enumeration.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.cn.quesbank.domain.enumeration.ModelTestType;
import org.springframework.data.repository.cdi.Eager;


/**
 * A ModelTest.
 */
@Entity
@Table(name = "model_test")
public class ModelTest extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(columnDefinition = "TEXT", name = "description")
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private ModelTestType type = ModelTestType.D;

    @Column(name = "subjects", columnDefinition = "TEXT")
    private String subjects;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, length = 10)
    private Status status = Status.V;

    @NotNull
    @Column(name = "expiry_date", nullable = false)
    private ZonedDateTime expiryDate;

    @Column(name = "available_date")
    private ZonedDateTime availableDate;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "model_test_question",
        joinColumns = @JoinColumn(name = "model_test_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "question_id", referencedColumnName = "id")
    )
    private Set<Question> questions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ModelTest name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public ModelTest description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ModelTestType getType() {
        return type;
    }

    public ModelTest type(ModelTestType type) {
        this.type = type;
        return this;
    }

    public void setType(ModelTestType type) {
        this.type = type;
    }

    public String getSubjects() {
        return subjects;
    }

    public ModelTest subjects(String subjects) {
        this.subjects = subjects;
        return this;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ModelTest status(Status status) {
        this.status = status;
        return this;
    }

    public ZonedDateTime getExpiryDate() {
        return expiryDate;
    }

    public ModelTest expiryDate(ZonedDateTime expiryDate) {
        this.expiryDate = expiryDate;
        return this;
    }

    public void setExpiryDate(ZonedDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public ZonedDateTime getAvailableDate() {
        return availableDate;
    }

    public ModelTest availableDate(ZonedDateTime availableDate) {
        this.availableDate = availableDate;
        return this;
    }

    public void setAvailableDate(ZonedDateTime availableDate) {
        this.availableDate = availableDate;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public ModelTest questions(Set<Question> questions) {
        this.questions = questions;
        return this;
    }

    public ModelTest addQuestions(Question question) {
        this.questions.add(question);
        return this;
    }

    public ModelTest removeQuestions(Question question) {
        this.questions.remove(question);
        return this;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModelTest modelTest = (ModelTest) o;
        if (modelTest.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelTest.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelTest{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", type='" + getType() + "'" +
            ", subjects='" + getSubjects() + "'" +
            ", status='" + getStatus() + "'" +
            ", expiryDate='" + getExpiryDate() + "'" +
            ", availableDate='" + getAvailableDate() + "'" +
            "}";
    }
}

package com.cn.quesbank.domain;


import com.cn.quesbank.domain.enumeration.Status;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;


/**
 * A Question.
 */
@Entity
@Table(name = "question")
public class Question extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(columnDefinition = "TEXT", name = "question", nullable = false)
    private String question;

    @NotNull
    @Column(columnDefinition = "TEXT", name = "answers", nullable = false)
    private String answers;

    @Column(columnDefinition = "TEXT", name = "tags")
    private String tags;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, length = 10)
    private Status status = Status.C;

    @Column(name = "weight")
    private Double weight;

    @Column(name = "verified_by", length = 50)
    private String verifiedBy;

    @Column(name = "verified_date")
    private Instant verifiedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public Question question(String question) {
        this.question = question;
        return this;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswers() {
        return answers;
    }

    public Question answers(String answers) {
        this.answers = answers;
        return this;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Question tags(String tags) {
        this.tags = tags;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public Question status(Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public Instant getVerifiedDate() {
        return verifiedDate;
    }

    public void setVerifiedDate(Instant verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Question question = (Question) o;
        if (question.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), question.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Question{" +
            "id=" + id +
            ", question='" + question + '\'' +
            ", answers='" + answers + '\'' +
            ", tags='" + tags + '\'' +
            ", status=" + status +
            ", weight=" + weight +
            ", verifiedBy='" + verifiedBy + '\'' +
            ", verifiedDate=" + verifiedDate +
            '}';
    }
}

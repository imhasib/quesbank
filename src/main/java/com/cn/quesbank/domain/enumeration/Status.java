package com.cn.quesbank.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    C, V, F, P, D
}

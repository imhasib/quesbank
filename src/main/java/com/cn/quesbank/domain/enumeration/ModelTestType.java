package com.cn.quesbank.domain.enumeration;

/**
 * The ModelTestType enumeration.
 */
public enum ModelTestType {
    D, W, M
}

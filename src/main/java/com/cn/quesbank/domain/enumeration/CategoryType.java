package com.cn.quesbank.domain.enumeration;
/*
    Elaboration:
    # A Article
    # E for Exam
    # R for Regular
    # CA CLIENT APP
    # H Home
*/

public enum CategoryType {
    A, E, R, CA, H
}

package com.cn.quesbank.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";
    //matches numbers only
    public static final String PHONE_NUMBER_REGEX = "^[0-9]*$";

    public static final long SYSTEM_ACCOUNT_ID = 1L;
    public static final long ADMIN_ACCOUNT_ID = 2L;

    public static final String ANONYMOUS_USER = "anonymoususer";

    public static final String DEFAULT_LANGUAGE = "en";

    public static final String CATEGORY_PATH_SEPARATOR = "/";
    public static final String TAG_ID_SEPARATOR = "/";

    public static final long CATEGORY_DEFAULT_HOME_PARENT_ID = 1L;
    public static final long CATEGORY_DEFAULT_CLIENT_APP_PARENT_ID = 2L;
    public static final long CATEGORY_DEFAULT_EXAM_PARENT_ID = 3L;
    public static final long CATEGORY_DEFAULT_OTHER_PARENT_ID = 5L;
    public static final String CATEGORY_DEFAULT_CLIENT_APP_PATH = CATEGORY_PATH_SEPARATOR +
        CATEGORY_DEFAULT_HOME_PARENT_ID +
        CATEGORY_PATH_SEPARATOR +
        CATEGORY_DEFAULT_CLIENT_APP_PARENT_ID +
        CATEGORY_PATH_SEPARATOR;


    private Constants() {
    }
}

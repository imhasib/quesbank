package com.cn.quesbank.repository;

import com.cn.quesbank.domain.ModelTest;
import com.cn.quesbank.domain.Question;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.Optional;
import java.util.Set;


/**
 * Spring Data JPA repository for the ModelTest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModelTestRepository extends JpaRepository<ModelTest, Long>, JpaSpecificationExecutor<ModelTest> {

    @EntityGraph(attributePaths = "questions")
    Optional<ModelTest> findOneWithQuestionsById(@Param("id") Long id);
}

package com.cn.quesbank.repository;

import com.cn.quesbank.domain.Authority;
import com.cn.quesbank.domain.Role;
import com.cn.quesbank.domain.User;
import com.cn.quesbank.domain.enumeration.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;
import java.time.Instant;
import java.util.Set;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(Instant dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmailIgnoreCase(String email);

    Optional<User> findOneByLogin(String login);

    Optional<User> findOneById(long id);

    @EntityGraph(attributePaths = "roles")
    Optional<User> findOneWithRolesById(Long id);

    @EntityGraph(attributePaths = "roles")
    Optional<User> findOneWithRolesByLogin(String login);

    @EntityGraph(attributePaths = "roles")
    Optional<User> findOneWithRolesByLoginOrEmail(String login, String email);

    Page<User> findAllByLoginNot(Pageable pageable, String login);
    Page<User> findAllByStatusNot(Pageable pageable, Status status);

    Page<User> findAllByRolesIn(Pageable pageable, Set<Role> roles);

    Page<User> findDistinctByRolesNotIn(Pageable pageable, Set<Role> roles);

    @Query("SELECT DISTINCT u FROM User u WHERE u.id NOT IN(select nu.id FROM User nu INNER JOIN nu.roles ur WHERE ur.id IN (:roleIds))")
    Page<User> findAllExceptRole(Pageable pageable, @Param("roleIds") Set<Long> roleIds);

    Page<User> findAllByRolesIsLessThan(Pageable pageable);

    Set<User> findByEmailIgnoreCaseContaining(String email);




}

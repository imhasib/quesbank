package com.cn.quesbank.repository;

import com.cn.quesbank.domain.Question;
import com.cn.quesbank.repository.DAO.CountQuestionsByCreatedDateDAO;
import com.cn.quesbank.repository.DAO.CountQuestionsByUsersDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data JPA repository for the Question entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long>, JpaSpecificationExecutor<Question> {

    // Link: https://stackoverflow.com/questions/44389534/spring-data-jpql-group-by-year-and-month-of-a-date-column
    // Note: DATE_FORMAT aggregation function caused illegal argument exception to create CountQuestionsByCreatedDateDAO object
    @Query(value = "select " +
        "new com.cn.quesbank.repository.DAO.CountQuestionsByCreatedDateDAO(q.createdDate, count(q)) " +
        "from Question q " +
        "group by DATE_FORMAT(q.createdDate, '%Y-%m-%d')")
    List<CountQuestionsByCreatedDateDAO> countQuestionsGroupByDay();

    @Query(value = "select " +
        "new com.cn.quesbank.repository.DAO.CountQuestionsByUsersDAO(q.createdBy.email, count(q)) " +
        "from Question q " +
        "group by q.createdBy")
    List<CountQuestionsByUsersDAO> countQuestionsGroupByCreatedBy();

    @Query(value = "select " +
        "new com.cn.quesbank.repository.DAO.CountQuestionsByUsersDAO(q.createdBy.email, count(q)) " +
        "from Question q " +
        "where q.createdDate between :startDate and :endDate " +
        "group by q.createdBy")
    List<CountQuestionsByUsersDAO> countQuestionsGroupByCreatedBy(@Param("startDate") Instant startDate, @Param("endDate")Instant endDate);

    Long countByCreatedDateBetween(Instant from, Instant to);
}

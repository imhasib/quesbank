package com.cn.quesbank.repository;

import com.cn.quesbank.domain.Category;
import com.cn.quesbank.domain.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;


/**
 * Spring Data JPA repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>, JpaSpecificationExecutor<Category> {
    List<Category> findByParent_Id(Long id);

    Set<Category> findByIdIn(Set<Long> ids);

    List<Category> findByPathLike(String subPath);

    @Query("select question from Category c join c.questions question where c.id = :categoryId AND question.status!='D'")
    Page<Question> findQuestionsByCategoryId(Pageable pageable, @Param("categoryId") long categoryId);

    @Query("select question from Category c join c.questions question where c.id = :categoryId AND question.status!='D'")
    List<Question> findQuestionsByCategoryId(@Param("categoryId") long categoryId);

    @Query("select count(question) from Category c join c.questions question where c = :category AND question.status!='D'")
    int countQuestionsByCategory(@Param("category") Category category);
}

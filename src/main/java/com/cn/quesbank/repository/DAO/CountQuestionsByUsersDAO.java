package com.cn.quesbank.repository.DAO;

/**
 * Created by Hasib on 2/16/2018.
 */
public class CountQuestionsByUsersDAO {
    String createdBy;
    Long count;

    public CountQuestionsByUsersDAO(String user, Long numberOfQuestions) {
        this.createdBy = user;
        this.count = numberOfQuestions;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "CountQuestionsByUsersDAO{" +
            "createdBy='" + createdBy + '\'' +
            ", count=" + count +
            '}';
    }
}

package com.cn.quesbank.repository.DAO;

import com.cn.quesbank.web.rest.serializers.CustomInstantJsonDeserializer;
import com.cn.quesbank.web.rest.serializers.CustomInstantJsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.time.Instant;

/**
 * Created by Hasib on 2/17/2018.
 */
public class CountQuestionsByCreatedDateDAO {
    @JsonDeserialize(using = CustomInstantJsonDeserializer.class)
    @JsonSerialize(using = CustomInstantJsonSerializer.class)
    Instant createdDate;
    Long count;

    public CountQuestionsByCreatedDateDAO(Instant date, Long count) {
        this.createdDate = date;
        this.count = count;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "CountQuestionsByCreatedDateDAO{" +
            "createdDate=" + createdDate +
            ", count=" + count +
            '}';
    }
}

package com.cn.quesbank.repository;

import com.cn.quesbank.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;


/**
 * Spring Data JPA repository for the Tag entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TagRepository extends JpaRepository<Tag, Long>, JpaSpecificationExecutor<Tag> {

    List<Tag> findByNameIgnoreCaseContaining(String username);

    Tag findOneByName(String tag);

    Set<Tag> findByNameIn(Set<String> tags);

    Set<Tag> findByIdIn(Set<Long> ids);
}


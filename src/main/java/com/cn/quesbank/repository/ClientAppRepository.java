package com.cn.quesbank.repository;

import com.cn.quesbank.domain.ClientApp;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ClientApp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientAppRepository extends JpaRepository<ClientApp, Long>, JpaSpecificationExecutor<ClientApp> {
//    @Query("select distinct client_app from ClientApp client_app left join fetch client_app.sharedWith")
//    List<ClientApp> findAllWithEagerRelationships();


//    @Query("select client_app from ClientApp client_app left join fetch client_app.categories c where client_app.id =:id and c.status not in (:statusList)")
//    ClientApp findOneWithCategoriesStatusNotIn(@Param("id") Long id, @Param("statusList") List<Status> statusList);

//    @Query("select client_app from ClientApp client_app left join client_app.category.questions q where client_app.createdBy = :createdBy and q.id = :questionId")
//    List<ClientApp> findByCreatedByAndQuestionId(@Param("createdBy") long createdBy, @Param("questionId") long questionId);

    @Query("select client_app from ClientApp client_app left join fetch client_app.sharedWith where client_app.id =:id")
    ClientApp findOneWithSharedWith(@Param("id") Long id);

//    Page<ClientApp> findByCreatedBy(Pageable pageable, String login);

    @Query("SELECT DISTINCT client_app FROM ClientApp client_app LEFT JOIN client_app.sharedWith shared_with WHERE client_app.createdBy.id =:userId OR shared_with.id = :userId")
    Page<ClientApp> findByCreatedByOrSharedWith(Pageable pageable, @Param("userId") Long userId);

    @Query("SELECT DISTINCT count(client_app) FROM ClientApp client_app LEFT JOIN client_app.sharedWith shared_with WHERE client_app.category.id = :categoryId AND (client_app.createdBy.id =:userId OR shared_with.id = :userId)")
    int countClientAppByCategoryAndCreatedByOrSharedWith(@Param("categoryId") Long categoryId, @Param("userId") Long userId);

    @Query("SELECT DISTINCT count(client_app) FROM ClientApp client_app LEFT JOIN client_app.sharedWith shared_with LEFT JOIN client_app.category.questions question WHERE client_app.category.id = :categoryId AND (client_app.createdBy.id =:userId OR shared_with.id = :userId) AND question.id=:questionId")
    int countByCreatedByAndSharedWithAndCategoryAndQuestionId(@Param("categoryId") Long categoryId, @Param("userId") Long userId, @Param("questionId") Long questionId);

    @Query("SELECT DISTINCT count(client_app) FROM ClientApp client_app LEFT JOIN client_app.sharedWith shared_with LEFT JOIN client_app.category.questions question WHERE (client_app.createdBy.id =:userId OR shared_with.id = :userId) AND question.id=:questionId")
    int countByCreatedByAndSharedWithAndQuestionId(@Param("userId") Long userId, @Param("questionId") Long questionId);

//    ClientApp findOneByIdAndCreatedByIn(String id, List<User> owners);

//    @Query("select question from ClientApp ca join ca.category.questions question where ca = :clientApp")
//    Page<Question> findQuestionsByClientApp(Pageable pageable, @Param("clientApp") ClientApp clientApp);
}

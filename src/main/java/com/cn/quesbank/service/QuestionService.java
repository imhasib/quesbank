package com.cn.quesbank.service;

import com.cn.quesbank.domain.Question;
import com.cn.quesbank.domain.Tag;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.repository.CategoryRepository;
import com.cn.quesbank.repository.ClientAppRepository;
import com.cn.quesbank.repository.QuestionRepository;
import com.cn.quesbank.security.AuthoritiesConstants;
import com.cn.quesbank.security.SecurityUtils;
import com.cn.quesbank.service.dto.AnswerDTO;
import com.cn.quesbank.service.dto.QuestionDTO;
import com.cn.quesbank.service.mapper.QuestionMapper;
import com.cn.quesbank.service.util.Utils;
import com.cn.quesbank.web.rest.errors.AccessDeniedException;
import com.cn.quesbank.web.rest.errors.InvalidAnswerException;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.Set;


/**
 * Service Implementation for managing Question.
 */
@Service
@Transactional
public class QuestionService {

    private final Logger log = LoggerFactory.getLogger(QuestionService.class);
    private final int MIN_ANSWERS = 4;
    private static final String ENTITY_NAME = "Question";

    private final QuestionRepository questionRepository;
    @Inject
    private QuestionMapper questionMapper;
    @Inject
    private TagService tagService;
    @Inject
    private ClientAppRepository clientAppRepository;
    @Inject
    private CategoryRepository categoryRepository;

    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }


    /**
     * Save a question.
     *
     * @param questionDTO the entity to save
     * @return the persisted entity
     */
    public Question save(QuestionDTO questionDTO) {
        Question question = null;
        Long quesId = questionDTO.getId();

        // Validation checking
        if (questionDTO.getAnswers().size() < MIN_ANSWERS) {
            throw new InvalidAnswerException("Question does not have minimum number of answers", ENTITY_NAME);
        }
        boolean hasCorrectAnswer = false;
        for (AnswerDTO answerDTO : questionDTO.getAnswers()) {
            if (StringUtils.isEmpty(answerDTO.getO())) { // getO(): get Option
                throw new InvalidAnswerException("One or more answer is empty", ENTITY_NAME);
            }
            if (answerDTO.isC() == true) { // Is Correct
                hasCorrectAnswer = true;
            }
        }
        if (!hasCorrectAnswer) {
            throw new InvalidAnswerException("Question should have atleast one correct answer", ENTITY_NAME);
        }


        if(quesId == null) {
            question = questionMapper.toEntity(questionDTO);
        } else {
            question = questionRepository.findOne(quesId);
            // Checking authorization during update.
            if(!SecurityUtils.isCurrentUserInAuthority(AuthoritiesConstants.QUESTION_WRITE)
                && !isRequesterOwnerOfClientApp(question)) {
                throw new AccessDeniedException(ENTITY_NAME);
            }

            question.setQuestion(questionDTO.getQuestion());
            try {
                question.setAnswers(Utils.answersToJSON(questionDTO.getAnswers()));
            } catch (JsonProcessingException e) {
                throw new InvalidAnswerException("Answers cant be parsed", ENTITY_NAME);
            }
            question.setStatus(questionDTO.getStatus());
        }

        Set<Tag> tags = tagService.storeTagsIfNotExist(questionDTO.getTags());
        question.setTags(Utils.toTagIdsStr(tags));

        return questionRepository.save(question);
    }

    /**
     * Get all the questions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Question> findAll(Pageable pageable) {
        log.debug("Request to get all Questions");
        return questionRepository.findAll(pageable);
    }

    /**
     * Get one question by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Question findOne(Long id) {
        log.debug("Request to get Question : {}", id);
        return questionRepository.findOne(id);
    }

    /**
     * Delete the question by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Question : {}", id);
        Question question = questionRepository.findOne(id);

        if(question != null) {
            long requesterId = SecurityUtils.getCurrentUserId().get();

            if (isRequesterOwnerOfClientApp(question)) {
                question.setStatus(Status.D);
            } else {
                throw new AccessDeniedException(this.ENTITY_NAME);
            }
        }

        questionRepository.save(question);
    }

    private boolean isRequesterOwnerOfClientApp(Question question) {
        boolean isOwner = false;
        long requesterId = SecurityUtils.getCurrentUserId().get();

        int count = clientAppRepository.countByCreatedByAndSharedWithAndQuestionId(requesterId, question.getId());

        if (SecurityUtils.isCurrentUserInAuthority(AuthoritiesConstants.CLIENT_APP_ADMINISTRATION)
            || requesterId == question.getCreatedBy().getId()
            || count > 0) {
            isOwner = true;
        }

        return isOwner;
    }

    /**
     * Clean the question by id.
     *
     * @param id the id of the entity
     */
    public void clean(Long id) {
        log.debug("Request to delete Question : {}", id);
        questionRepository.delete(id);
    }

    public Page<Question> getQuestionsByCategoryId(Pageable page, Long categoryId) {

        Page<Question> questions = categoryRepository.findQuestionsByCategoryId(page, categoryId);

        return questions;
    }
}

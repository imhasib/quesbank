package com.cn.quesbank.service;


import com.cn.quesbank.domain.*;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.repository.QuestionRepository;
import com.cn.quesbank.service.dto.QuestionCriteria;
import com.cn.quesbank.service.util.Utils;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.StringFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Service for executing complex queries for Question entities in the database.
 * The main input is a {@link QuestionCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Question} or a {@link Page} of {@link Question} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class QuestionQueryService extends QueryService<Question> {

    private final Logger log = LoggerFactory.getLogger(QuestionQueryService.class);


    private final QuestionRepository questionRepository;

    @Inject
    private CategoryService categoryService;

    public QuestionQueryService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    /**
     * Return a {@link List} of {@link Question} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Question> findByCriteria(QuestionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Question> specification = createSpecification(criteria);
        return questionRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Question} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Question> findByCriteria(QuestionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Question> specification = createSpecification(criteria);
        return questionRepository.findAll(specification, page);
    }

    /**
     * Function to convert QuestionCriteria to a {@link Specifications}
     */
    private Specifications<Question> createSpecification(QuestionCriteria criteria) {
        Specifications<Question> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getCategoryId() != null) {
                specification = Specifications.where(new QuestionsOfCategorySpecification(criteria.getCategoryId().getEquals()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Question_.id));
            }
            if (criteria.getQuestion() != null) {
                specification = specification.and(buildStringSpecification(criteria.getQuestion(), Question_.question));
            }
            if (criteria.getAnswers() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAnswers(), Question_.answers));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), Question_.status));
            }
            if(criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), Question_.createdDate));
            }
            if(criteria.getCreatedBy() != null) {
                specification = specification.and(questionCreatedByIsEqual(criteria.getCreatedBy().getEquals()));
            }

            if(criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), Question_.lastModifiedDate));
            }

            if(criteria.getCategories() != null) {
                Set<Long> categoryIds = Utils.parseObjectIds(criteria.getCategories().getContains(), ",");

                Set<Category> categories = categoryService.findCategoriesByIds(categoryIds);

                for(Category category: categories) {
                    Set<Long> tagIds = Utils.parseTagIds(category.getTags());

                    for(Long tagId: tagIds) {
                        StringFilter tagFilter = new StringFilter();
                        tagFilter.setContains(Utils.wrapTags(tagId.toString()));
                        specification = specification.or(buildStringSpecification(tagFilter, Question_.tags));
                    }
                }
            }

            if(criteria.getTags() != null) {
                Set<Long> tagIds = Utils.parseObjectIds(criteria.getTags().getContains(), ",");
                for(Long tagId: tagIds) {
                    StringFilter tagFilter = new StringFilter();
                    tagFilter.setContains(Utils.wrapTags(tagId.toString()));
                    specification = specification.or(buildStringSpecification(tagFilter, Question_.tags));
                }
            }
        }
        return specification;
    }


    public Specification<Question> questionCreatedByIsEqual(String login) {
        return new Specification<Question>() {
            @Override
            public Predicate toPredicate(Root<Question> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Question, User> createdBy = root.join(Question_.createdBy, JoinType.INNER);

                return criteriaBuilder.equal(createdBy.get(User_.email), login);
            }
        };
    }

    private class QuestionsOfCategorySpecification implements Specification<Question> {

        private Long categoryId;

        public QuestionsOfCategorySpecification(Long categoryId) {
            this.categoryId = categoryId;
        }

        @Override
        public Predicate toPredicate(Root<Question> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
            criteriaQuery.distinct(true);
            Root<Question> questionRoot = root;
            Root<Category> categoryRoot = criteriaQuery.from(Category.class);
            Expression<Collection<Question>> categoryQuestions = categoryRoot.get("questions");

            return criteriaBuilder.and(
                criteriaBuilder.and(criteriaBuilder.equal(categoryRoot.get("id"), this.categoryId), criteriaBuilder.isMember(questionRoot, categoryQuestions)),
                criteriaBuilder.notEqual(questionRoot.get("status"), Status.D));
        }
    }

}

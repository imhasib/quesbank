package com.cn.quesbank.service.exceptions;

/**
 * Created by burhan on 12/8/17.
 */
public class ValidationException extends Exception {
    public ValidationException() {
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}

package com.cn.quesbank.service.util;

import com.cn.quesbank.config.Constants;
import com.cn.quesbank.domain.Question;
import com.cn.quesbank.domain.Tag;
import com.cn.quesbank.service.dto.AnswerDTO;
import com.cn.quesbank.service.dto.ClientAppSettings;
import com.cn.quesbank.service.dto.LinkDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.*;

/**
 * Created by Hasib on 12/8/17.
 */
public class Utils {
    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    public static List<AnswerDTO> parseAnswers(Question question) throws IOException {
        List<AnswerDTO> answers = new ArrayList<>();

        if (!StringUtils.isEmpty(question.getAnswers())) {
            ObjectMapper objectMapper = new ObjectMapper();
            TypeFactory typeFactory = objectMapper.getTypeFactory();
            answers = objectMapper.readValue(question.getAnswers(), typeFactory.constructCollectionType(List.class, AnswerDTO.class));
        }
        return answers;
    }

    public static String answersToJSON(List<AnswerDTO> answers) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(answers);
    }

    public static String clientAppSettingsToJson(ClientAppSettings settings) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(settings);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static ClientAppSettings parseClientAppSettings(String settingsStr) {
        ClientAppSettings settings = new ClientAppSettings();

        if (!StringUtils.isEmpty(settingsStr)) {
            ObjectMapper objectMapper = new ObjectMapper();
            TypeFactory typeFactory = objectMapper.getTypeFactory();
            try {
                settings = objectMapper.readValue(settingsStr, ClientAppSettings.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return settings;
    }

    public static String toTagIdsStr(Set<Tag> tags) {
        String tagsStr = "/";
        for(Tag tag: tags) {
            tagsStr += tag.getId() + Constants.TAG_ID_SEPARATOR;
        }

        return tagsStr;
    }

    public static String wrapTags(String tagsStr) {
        return Constants.TAG_ID_SEPARATOR + tagsStr + Constants.TAG_ID_SEPARATOR;
    }

    public static Set<Long> parseObjectIds(String str, String separator) {
        Set<Long> objectIds = new HashSet<>();

        if (!StringUtils.isEmpty(str)) {
            String[] objectArray = str.split(separator);
            for(int i=0;i<objectArray.length;i++) {
                String objectIdStr = objectArray[i];
                if(!StringUtils.isEmpty(objectIdStr)) {
                    try{
                        objectIds.add(Long.parseLong(objectIdStr));
                    } catch (NumberFormatException e) {
                        log.error("Id can't be parsed as Long");
                    }
                }
            }
        }

        return objectIds;
    }

    public static Set<Long> parseTagIds(String tagsStr) {
        return parseObjectIds(tagsStr, Constants.TAG_ID_SEPARATOR);
    }

    public static Set<Long> parseCategoryIds(String catIdsStr) {
        return parseObjectIds(catIdsStr, Constants.CATEGORY_PATH_SEPARATOR);
    }

    public static Long parseCategoryRoot(String catIdsStr) {
        catIdsStr = catIdsStr.replaceAll(Constants.CATEGORY_DEFAULT_CLIENT_APP_PATH, "");
        String[] ids = catIdsStr.split(Constants.CATEGORY_PATH_SEPARATOR);

        long rootCat = -1;
        try {
            rootCat = Long.parseLong(ids[0]);
        } catch (NumberFormatException e) {
            log.error("parsing error. Root category not found. category itself is root category.");
        }

        return rootCat;
    }

    public static List<LinkDTO> parseArticlesLinks(String linksStr) throws IOException {
        List<LinkDTO> links = new ArrayList<LinkDTO>();

        if(!StringUtils.isEmpty(linksStr)) {
            ObjectMapper objectMapper = new ObjectMapper();
            TypeFactory typeFactory = objectMapper.getTypeFactory();
            links = objectMapper.readValue(linksStr, typeFactory.constructCollectionType(List.class, LinkDTO.class));
        }
        return links;
    }

    public static String articleLinksToJSON(List<LinkDTO> links) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(links);
    }

    public static HashMap<Long, Tag> toMap(Set<Tag> tags) {
        HashMap<Long, Tag> idTagMap = new HashMap<>();
        if(tags != null) {
            for(Tag tag: tags) {
                idTagMap.put(tag.getId(), tag);
            }
        }

        return idTagMap;
    }

    public static Set<Tag> getTagsFromMap(HashMap<Long, Tag> idTagMap, String tagIdstr) {
        Set<Tag> tags = new HashSet<>();

        if(idTagMap!=null && !idTagMap.isEmpty()) {
            Set<Long> tagIds = Utils.parseTagIds(tagIdstr);
            for(Long id: tagIds) {
                Tag tag = idTagMap.get(id);
                if(tag != null) {
                    tags.add(tag);
                }
            }
        }

        return tags;
    }
}

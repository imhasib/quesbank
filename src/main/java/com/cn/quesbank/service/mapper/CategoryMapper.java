package com.cn.quesbank.service.mapper;

import com.cn.quesbank.domain.Category;
import com.cn.quesbank.domain.Tag;
import com.cn.quesbank.service.TagService;
import com.cn.quesbank.service.dto.CategoryDTO;
import com.cn.quesbank.service.util.Utils;
import org.mapstruct.Mapper;

import javax.inject.Inject;
import java.util.*;

/**
 * Mapper for the entity Category and its DTO CategoryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public class CategoryMapper {
    @Inject
    TagService tagService;

    HashMap<Long, Tag> idToTagMap = new HashMap<>();

    public void setIdToTagMap(HashMap<Long, Tag> idToTagMap) {
        this.idToTagMap = idToTagMap;
    }

    public Set<Tag> margeAllTagsFromCategory(List<Category> categories) {
        Set<Long> tagIds = new HashSet<>();
        for (Category category : categories) {
            tagIds.addAll(Utils.parseTagIds(category.getTags()));
        }

        return tagService.findTagsByIds(tagIds);
    }

    public CategoryDTO toDto(Category category) {
        if(category != null) {
            return new CategoryDTO(category);
        }

        return null;
    }

    public CategoryDTO toDtoWithTags(Category category) {
        if(category != null) {
            CategoryDTO categoryDTO = new CategoryDTO(category);
            categoryDTO.setTags(Utils.getTagsFromMap(this.idToTagMap, category.getTags()));
            return categoryDTO;
        }

        return null;
    }

    public List<CategoryDTO> toDtos(Iterable<Category> categories) {
        List<CategoryDTO> categoryDTOS = new ArrayList<>();
        for (Category category : categories) {
            if (category != null) {
                categoryDTOS.add(toDto(category));
            }
        }
        return categoryDTOS;
    }

    public Category toEntity(CategoryDTO categoryDTO) {
        if (categoryDTO == null) {
            return null;
        } else {
            Category category = new Category();
            category.setId(categoryDTO.getId());
            category.setName(categoryDTO.getName());
            category.setDescription(categoryDTO.getDescription());

            Category parent = new Category();
            parent.setId(categoryDTO.getParentId());
            category.setParent(parent);

            category.setType(categoryDTO.getType());
            category.setIcon(categoryDTO.getIcon());
            category.setPosition(categoryDTO.getPosition());
            return category;
        }
    }
}

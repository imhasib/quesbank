package com.cn.quesbank.service.mapper;

import com.cn.quesbank.domain.Question;
import com.cn.quesbank.domain.Tag;
import com.cn.quesbank.service.TagService;
import com.cn.quesbank.service.dto.QuestionDTO;
import com.cn.quesbank.service.util.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Hasib on 12/8/17.
 */
@Service
public class QuestionMapper {

    @Inject
    TagService tagService;

    HashMap<Long, Tag> idToTagMap = new HashMap<>();

    public HashMap<Long, Tag> getIdToTagMap() {
        return idToTagMap;
    }

    public void setIdToTagMap(HashMap<Long, Tag> idToTagMap) {
        this.idToTagMap = idToTagMap;
    }

    public QuestionDTO toDto(Question question) {
        if(question != null) {
            QuestionDTO questionDTO = new QuestionDTO(question);
            questionDTO.setTags(Utils.getTagsFromMap(idToTagMap, question.getTags()));
            return questionDTO;
        }

        return null;
    }

    //TODO: Project can't be compiled after uncomment this block.
//    public QuestionDTO toDtoWithTags(Question question) {
//        if(question != null) {
//            QuestionDTO questionDTO = new QuestionDTO(question);
//            questionDTO.setTags(Utils.getTagsFromMap(idToTagMap, question.getTags()));
//            return questionDTO;
//        }
//
//        return null;
//    }

    public Set<QuestionDTO> toDtos(Set<Question> questions) {
        Set<QuestionDTO> questionDTOS = new HashSet<>();

        for (Question question : questions) {
            if (question != null) {
                QuestionDTO questionDTO = new QuestionDTO(question);
                questionDTO.setTags(Utils.getTagsFromMap(idToTagMap, question.getTags()));
                questionDTOS.add(questionDTO);
            }
        }
        return questionDTOS;
    }

    public Set<Tag>  margeAllTagsFromQuestions(List<Question> questions) {
        Set<Long> tagIds = new HashSet<>();
        for (Question question : questions) {
            tagIds.addAll(Utils.parseTagIds(question.getTags()));
        }

        return tagService.findTagsByIds(tagIds);
    }

    public Question toEntity(QuestionDTO questionDTO) {
        if (questionDTO == null) {
            return null;
        } else {
            Question question = new Question();
            question.setId(questionDTO.getId());
            question.setQuestion(questionDTO.getQuestion());
            question.setStatus(questionDTO.getStatus());

            try {
                question.setAnswers(Utils.answersToJSON(questionDTO.getAnswers()));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            return question;
        }
    }

    public List<Question> toEntities(List<QuestionDTO> questionDTOs) {
        return questionDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }
}

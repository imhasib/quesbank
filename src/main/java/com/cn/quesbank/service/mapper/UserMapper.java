package com.cn.quesbank.service.mapper;

import com.cn.quesbank.domain.Authority;
import com.cn.quesbank.domain.Role;
import com.cn.quesbank.domain.User;
import com.cn.quesbank.service.dto.UserDTO;

import org.springframework.stereotype.Service;
import org.thymeleaf.util.SetUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Mapper for the entity User and its DTO called UserDTO.
 *
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
public class UserMapper {

    public UserDTO toDto(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setMobile(user.getMobile());
        userDTO.setActivated(user.getActivated());
        userDTO.setImageUrl(user.getImageUrl());
        userDTO.setLangKey(user.getLangKey());
        userDTO.setCreatedBy(user.getCreatedBy().getLogin());
        userDTO.setCreatedDate(user.getCreatedDate());
        userDTO.setLastModifiedBy(user.getLastModifiedBy().getLogin());
        userDTO.setLastModifiedDate(user.getLastModifiedDate());
        userDTO.setRoles(user.getRoles());
        userDTO.setAuthorities(authoritiesFromRoles(user.getRoles()));

        return userDTO;
    }

    public List<UserDTO> usersToUserDTOs(List<User> users) {
        return users.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }

    public User userDTOToUser(UserDTO userDTO) {
        if (userDTO == null) {
            return null;
        } else {
            User user = new User();
            user.setId(userDTO.getId());
            user.setLogin(userDTO.getLogin());
            user.setFirstName(userDTO.getFirstName());
            user.setLastName(userDTO.getLastName());
            user.setEmail(userDTO.getEmail());
            user.setMobile(userDTO.getMobile());
            user.setImageUrl(userDTO.getImageUrl());
            user.setActivated(userDTO.isActivated());
            user.setLangKey(userDTO.getLangKey());
            Set<Role> roles = userDTO.getRoles();
            if (SetUtils.isEmpty(roles)) {
                user.setRoles(roles);
            }
            return user;
        }
    }

    public List<User> userDTOsToUsers(List<UserDTO> userDTOs) {
        return userDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::userDTOToUser)
            .collect(Collectors.toList());
    }

    public User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }

    public Set<String> authoritiesFromRoles (Set<Role> roles) {
        Set<String> authorities = new HashSet<>();
//        roles.stream().map(role -> role.getAuthorities().stream().map(authority -> authorities.add(authority.getName())));
        for (Role role : roles) {
            for(Authority authority : role.getAuthorities()) {
                authorities.add(authority.getName());
            }
        }
        return authorities;
    }

    public Set<Role> rolesFromStrings(Set<String> strings) {
        return strings.stream().map(string -> {
            Role role = new Role();
            role.setName(string);
            return role;
        }).collect(Collectors.toSet());
    }
}

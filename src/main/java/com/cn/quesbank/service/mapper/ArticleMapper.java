package com.cn.quesbank.service.mapper;

import com.cn.quesbank.domain.*;
import com.cn.quesbank.service.dto.ArticleDTO;

import com.cn.quesbank.service.util.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.mapstruct.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Mapper for the entity Article and its DTO ArticleDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ArticleMapper {

    default ArticleDTO toDto(Article article) {
        if(article == null) {
               return null;
        }

        ArticleDTO articleDTO = new ArticleDTO();
        articleDTO.setId(article.getId());
        articleDTO.setTitle(article.getTitle());
        articleDTO.setSummary(article.getSummary());
        articleDTO.setContent(article.getContent());
        try {
            articleDTO.setLinks(Utils.parseArticlesLinks(article.getLinks()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return articleDTO;
    }

    default Article toEntity(ArticleDTO articleDTO) {
        if (articleDTO == null) {
            return null;
        } else {
            Article article = new Article();
            article.setId(articleDTO.getId());
            article.setTitle(articleDTO.getTitle());
            article.setSummary(articleDTO.getSummary());
            article.setContent(articleDTO.getContent());
            try {
                article.setLinks(Utils.articleLinksToJSON(articleDTO.getLinks()));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            return article;
        }
    }

    default List<Article> toEntity(List<ArticleDTO> articleDTOS) {
        List<Article> articles = new ArrayList<>();
        for(ArticleDTO articleDTO : articleDTOS) {
            articles.add(toEntity(articleDTO));
        }

        return articles;
    }

    default List<ArticleDTO> toDto(List<Article> articles) {
        List<ArticleDTO> articleDTOS = new ArrayList<ArticleDTO>();

        for(Article article : articles) {
            articleDTOS.add(toDto(article));
        }

        return articleDTOS;
    }

    default Article fromId(Long id) {
        if (id == null) {
            return null;
        }
        Article article = new Article();
        article.setId(id);
        return article;
    }
}

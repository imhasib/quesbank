package com.cn.quesbank.service.mapper;

import com.cn.quesbank.domain.*;
import com.cn.quesbank.service.dto.ModelTestDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ModelTest and its DTO ModelTestDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ModelTestMapper extends EntityMapper<ModelTestDTO, ModelTest> {

    @Mapping(target = "questions", ignore = true)
    ModelTest toEntity(ModelTestDTO modelTestDTO);

    default ModelTest fromId(Long id) {
        if (id == null) {
            return null;
        }
        ModelTest modelTest = new ModelTest();
        modelTest.setId(id);
        return modelTest;
    }
}

package com.cn.quesbank.service.mapper;

import com.cn.quesbank.domain.ClientApp;
import com.cn.quesbank.domain.User;
import com.cn.quesbank.service.dto.ClientAppDTO;
import com.cn.quesbank.service.dto.ClientAppSettings;
import com.cn.quesbank.service.dto.UserDTOLite;
import com.cn.quesbank.service.util.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.SetUtils;

import javax.inject.Inject;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper for the entity ClientApp and its DTO ClientAppDTO.
 */
@Service
public class ClientAppMapper {

    @Inject
    private CategoryMapper categoryMapper;

    public ClientApp toEntity(ClientAppDTO dto) {
        ClientApp clientApp = new ClientApp();

        clientApp.setId(dto.getId());
        clientApp.setName(dto.getName());
        clientApp.setPackageName(dto.getPackageName());
        clientApp.setDescription(dto.getDescription());
        if(!SetUtils.isEmpty(dto.getSharedWith())) {
            clientApp.setSharedWith(toUser(dto.getSharedWith()));
        }
        clientApp.setSettings(Utils.clientAppSettingsToJson(dto.getSettings()));


        return clientApp;
    }

    public ClientAppDTO toDto(ClientApp clientApp) {
        ClientAppDTO clientAppDTO = new ClientAppDTO(clientApp);
        clientAppDTO.setCategory(categoryMapper.toDto(clientApp.getCategory()));
        clientAppDTO.setSettings(Utils.parseClientAppSettings(clientApp.getSettings()));
        return clientAppDTO;
    }

    public ClientAppDTO toDtoWithSharedWith(ClientApp clientApp) {
        ClientAppDTO clientAppDTO = toDto(clientApp);

        if (!SetUtils.isEmpty(clientApp.getSharedWith())) {
            clientAppDTO.setSharedWith(clientApp.getSharedWith().stream().map(UserDTOLite::new).collect(Collectors.toSet()));
        }
        return clientAppDTO;
    }

    public ClientApp fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientApp clientApp = new ClientApp();
        clientApp.setId(id);
        return clientApp;
    }

    public Set<User> toUser(Set<UserDTOLite> liteUsers) {
        return liteUsers
            .stream()
            .filter(userDTOLite -> userDTOLite.getId() != null)
            .map(UserDTOLite::toUser)
            .collect(Collectors.toSet());
    }
}

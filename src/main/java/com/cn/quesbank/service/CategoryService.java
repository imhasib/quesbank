package com.cn.quesbank.service;

import com.cn.quesbank.config.Constants;
import com.cn.quesbank.domain.Category;
import com.cn.quesbank.domain.Question;
import com.cn.quesbank.domain.Tag;
import com.cn.quesbank.domain.enumeration.CategoryType;
import com.cn.quesbank.repository.CategoryRepository;
import com.cn.quesbank.repository.ClientAppRepository;
import com.cn.quesbank.security.AuthoritiesConstants;
import com.cn.quesbank.security.SecurityUtils;
import com.cn.quesbank.service.dto.CategoryDTO;
import com.cn.quesbank.service.mapper.CategoryMapper;
import com.cn.quesbank.service.util.Utils;
import com.cn.quesbank.web.rest.errors.AccessDeniedException;
import com.cn.quesbank.web.rest.errors.DeleteCategoryException;
import com.cn.quesbank.web.rest.errors.ParentIdNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.ListUtils;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;


/**
 * Service Implementation for managing Category.
 */
@Service
@Transactional
public class CategoryService {

    private final Logger log = LoggerFactory.getLogger(CategoryService.class);
    private final String ENTITY_NAME = "Category";
    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;

    @Inject
    private TagService tagService;
    @Inject
    private QuestionService questionService;
    @Inject
    private ClientAppRepository clientAppRepository;

    public CategoryService(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    /**
     * Save a category.
     *
     * @param categoryDTO the entity to save
     * @return the persisted entity
     */
    public CategoryDTO save(CategoryDTO categoryDTO) {
        log.debug("Request to save Category : {}", categoryDTO);
        // TODO: Category should be created by who has category.write permission.
        Set<Tag> tags = tagService.storeTagsIfNotExist(categoryDTO.getTags());

        Category category = categoryMapper.toEntity(categoryDTO);
        category.setTags(Utils.toTagIdsStr(tags));

        return categoryMapper.toDto(save(category));
    }

    public Category save(Category category) {
        Category parent = categoryRepository.findOne(category.getParent().getId());
        if (parent == null) {
            throw new ParentIdNotFoundException(ENTITY_NAME);
        }
        category.setType(parent.getType());
        category.setParent(parent);
        category.setPath(generatePath(parent));
        category = categoryRepository.save(category);

        return category;
    }

    public Category save(String name, long parentId, CategoryType type) {
        Category parent = new Category();
        parent.setId(parentId);

        Category category = new Category();
        category.setName(name);
        category.setParent(parent);
        category.setType(type);

        return save(category);
    }

    public Optional<CategoryDTO> update(CategoryDTO categoryDto) {
        Set<Tag> tags = tagService.storeTagsIfNotExist(categoryDto.getTags());

        return Optional.of(categoryRepository.findOne(categoryDto.getId()))
            .map(cat -> {
                if (isRequesterOwnerOfCategory(cat)) {
                    cat.setName(categoryDto.getName());
                    cat.setDescription(categoryDto.getDescription());
                    cat.setTags(Utils.toTagIdsStr(tags));
                    cat.setType(categoryDto.getType());
                    cat.setIcon(categoryDto.getIcon());
                    cat.setPosition(categoryDto.getPosition());
                } else {
                    throw new AccessDeniedException(this.ENTITY_NAME);
                }

                return categoryRepository.save(cat);
            }).map(categoryMapper::toDto);
    }

    /**
     * Get all the categories.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
//    @Transactional(readOnly = true)
//    public Page<CategoryDTO> findAll(Pageable pageable) {
//        log.debug("Request to get all Categories");
//        return categoryRepository.findAll(pageable)
//            .map(categoryMapper::toDto);
//    }

    /**
     * Get one category by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Category findOne(Long id) {
        log.debug("Request to get Category : {}", id);
        Category category = categoryRepository.findOne(id);
        if (!isRequesterOwnerOfCategory(category)) {
            throw new AccessDeniedException(ENTITY_NAME);
        }
        return category;
    }

    @Transactional(readOnly = true)
    public Set<Category> findCategoriesByIds(Set<Long> ids) {
        Set<Category> categories = new HashSet<>();
        if (ids != null) {
            categories = categoryRepository.findByIdIn(ids);
        }

        return categories;
    }

    /**
     * Delete the category by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Category : {}", id);
        Category category = findOne(id);

        if (isRequesterOwnerOfCategory(category)) {
            List<Category> children = categoryRepository.findByParent_Id(id);

            for(Category child : children) {
                delete(child.getId());
            }

            if (ListUtils.isEmpty(children)) {
                int countQues = categoryRepository.countQuestionsByCategory(category);
                if (countQues > 0) {
                    throw new DeleteCategoryException("Category:'" + category.getName() + "' can't be deleted, Questions exist ", ENTITY_NAME);
                }
            }
            categoryRepository.delete(id);
        }
    }

    public Category addQuestions(Long categoryId, List<Long> questionIds) {
        Category category = findOne(categoryId);
        if (isRequesterOwnerOfCategory(category)) {
            for (Long questionId : questionIds) {
                Question question = questionService.findOne(questionId);

                if (question != null) {
                    Set<Question> questions = category.getQuestions();
                    questions.add(question);
                }
            }
            category = save(category);
        }

        return category;
    }

    // #Utility functions....

    public String generatePath(Category parent) {
        return parent.getPath() + parent.getId() + Constants.CATEGORY_PATH_SEPARATOR;
    }

    public boolean isRequesterOwnerOfCategory(Category category) {
        if (category != null) {
            if (SecurityUtils.isCurrentUserInAuthority(AuthoritiesConstants.CLIENT_APP_ADMINISTRATION)) {
                return true;
            } else {
                long rootCategoryId = Utils.parseCategoryRoot(category.getPath());
                if (rootCategoryId < 0) {
                    rootCategoryId = category.getId();
                }
                int count = clientAppRepository.countClientAppByCategoryAndCreatedByOrSharedWith(rootCategoryId, SecurityUtils.getCurrentUserId().get());
                if (count > 0) {
                    return true;
                }
            }
        }

        return false;
    }
}

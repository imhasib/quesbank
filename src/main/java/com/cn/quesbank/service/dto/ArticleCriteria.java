package com.cn.quesbank.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the Article entity. This class is used in ArticleResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /articles?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ArticleCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter title;

    private StringFilter summary;

    private StringFilter content;

    private StringFilter links;

    private StringFilter tags;

    private LongFilter parent;

    private LongFilter root;

    public ArticleCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getSummary() {
        return summary;
    }

    public void setSummary(StringFilter summary) {
        this.summary = summary;
    }

    public StringFilter getContent() {
        return content;
    }

    public void setContent(StringFilter content) {
        this.content = content;
    }

    public StringFilter getLinks() {
        return links;
    }

    public void setLinks(StringFilter links) {
        this.links = links;
    }

    public StringFilter getTags() {
        return tags;
    }

    public void setTags(StringFilter tags) {
        this.tags = tags;
    }

    public LongFilter getParent() {
        return parent;
    }

    public void setParent(LongFilter parent) {
        this.parent = parent;
    }

    public LongFilter getRoot() {
        return root;
    }

    public void setRoot(LongFilter root) {
        this.root = root;
    }

    @Override
    public String toString() {
        return "ArticleCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (summary != null ? "summary=" + summary + ", " : "") +
                (content != null ? "content=" + content + ", " : "") +
                (links != null ? "links=" + links + ", " : "") +
                (tags != null ? "tags=" + tags + ", " : "") +
                (parent != null ? "parent=" + parent + ", " : "") +
                (root != null ? "root=" + root + ", " : "") +
            "}";
    }

}

package com.cn.quesbank.service.dto;

import java.util.List;

/**
 * Created by Hasib on 2018-11-09.
 */
public class DeletedQuestionsDTOMin {
    private List<Long> ids;

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return "DeletedQuestionsDTOMin{" +
            "ids=" + ids +
            '}';
    }
}

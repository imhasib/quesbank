package com.cn.quesbank.service.dto;

import com.cn.quesbank.domain.Question;
import com.cn.quesbank.service.util.Utils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.validator.constraints.NotBlank;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Hasib on 2018-11-06.
 */
public class QuestionDTOMin implements Serializable{
    private Long id; // Question id
    @NotBlank
    private String q; // Question Text
    private List<String> a = new ArrayList<>(); // List of answers
    private int i; // Index of correct Answers
    @JsonProperty("t")
    private Set<Long> tagIds = new HashSet<>(); // Set of Tag Ids

    public QuestionDTOMin() {
    }

    public QuestionDTOMin(Question question) {
        this.setId(question.getId());
        this.setQ(question.getQuestion());
        try {
            AnswerDTO[] answers = new ObjectMapper().readValue(question.getAnswers(), AnswerDTO[].class);
            List<String> a = new ArrayList<String>();
            for (int i=0; i < answers.length; i++) {
                AnswerDTO answerDTO = answers[i];
                a.add(answerDTO.getO());
                if(answerDTO.isC()) {
                    this.setI(i);
                }
            }
            this.setA(a);

            this.setTagIds(Utils.parseTagIds(question.getTags()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Set<Long> toIds(Set<Question> questions) {
        Set<Long> questionDTOMins = new HashSet<>();

        if(questions != null) {
            for(Question question: questions) {
                questionDTOMins.add(question.getId());
            }
        }

        return questionDTOMins;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public List<String> getA() {
        return a;
    }

    public void setA(List<String> a) {
        this.a = a;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public Set<Long> getTagIds() {
        return tagIds;
    }

    public void setTagIds(Set<Long> tagIds) {
        this.tagIds = tagIds;
    }
}

package com.cn.quesbank.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientAppSettings {

    @JsonProperty("qpe")
    private int numberOfQuestionsPerExam = 10;
    @JsonProperty("et")
    private int examTime = 5; // In minute
    @JsonProperty("m")
    private float mark = 1.0f;
    @JsonProperty("nme")
    private boolean negativeMarking = false;
    @JsonProperty("nm")
    private float negativeMark = 0.5f;

    public int getNumberOfQuestionsPerExam() {
        return numberOfQuestionsPerExam;
    }

    public void setNumberOfQuestionsPerExam(int numberOfQuestionsPerExam) {
        this.numberOfQuestionsPerExam = numberOfQuestionsPerExam;
    }

    public int getExamTime() {
        return examTime;
    }

    public void setExamTime(int examTime) {
        this.examTime = examTime;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public boolean isNegativeMarking() {
        return negativeMarking;
    }

    public void setNegativeMarking(boolean negativeMarking) {
        this.negativeMarking = negativeMarking;
    }

    public float getNegativeMark() {
        return negativeMark;
    }

    public void setNegativeMark(float negativeMark) {
        this.negativeMark = negativeMark;
    }

    @Override
    public String toString() {
        return "ClientAppSettings{" +
            "numberOfQuestionsPerExam=" + numberOfQuestionsPerExam +
            ", examTime=" + examTime +
            ", mark=" + mark +
            ", negativeMarking=" + negativeMarking +
            ", negativeMark=" + negativeMark +
            '}';
    }
}

package com.cn.quesbank.service.dto;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by Hasib on 12/8/17.
 */
public class AnswerDTO {
    @NotBlank
    private String o; // Option
    private boolean c = false; // Correct

    public AnswerDTO() {
    }

    public String getO() {
        return o;
    }

    public void setO(String o) {
        this.o = o;
    }

    public boolean isC() {
        return c;
    }

    public void setC(boolean c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return "AnswerDTO{" +
            "Option='" + o + '\'' +
            ", Correct=" + c +
            '}';
    }
}

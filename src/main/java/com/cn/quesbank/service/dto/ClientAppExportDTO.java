package com.cn.quesbank.service.dto;

import com.cn.quesbank.domain.Tag;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by Hasib on 2019-04-20.
 */
public class ClientAppExportDTO implements Serializable {
    private boolean success = true;
    private List<CategoryExportDto> categories;
    private List<QuestionExportDto> questions;
    private Set<Tag> tags;
    private String date;
    private ClientAppSettings settings;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<CategoryExportDto> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryExportDto> categories) {
        this.categories = categories;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<QuestionExportDto> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionExportDto> questions) {
        this.questions = questions;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public ClientAppSettings getSettings() {
        return settings;
    }

    public void setSettings(ClientAppSettings settings) {
        this.settings = settings;
    }
}

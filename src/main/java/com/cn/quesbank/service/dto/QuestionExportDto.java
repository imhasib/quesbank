package com.cn.quesbank.service.dto;

import com.cn.quesbank.domain.Question;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.service.util.Utils;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by imhas on 2019-05-03.
 */
public class QuestionExportDto implements Serializable {
    private Long id;
    private String question;
    private List<AnswerDTO> answers = new ArrayList<>();
    @JsonProperty("tags")
    private Set<Long> tagIds = new HashSet<>();
    private Status status;
    private long categoryId;

    public QuestionExportDto(Question question) {
        this.setId(question.getId());
        this.setQuestion(question.getQuestion());
        this.setStatus(question.getStatus());
        this.setTagIds(Utils.parseTagIds(question.getTags()));
        try {
            this.setAnswers(Utils.parseAnswers(question));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<AnswerDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDTO> answers) {
        this.answers = answers;
    }

    public Set<Long> getTagIds() {
        return tagIds;
    }

    public void setTagIds(Set<Long> tagIds) {
        this.tagIds = tagIds;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }
}

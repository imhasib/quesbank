package com.cn.quesbank.service.dto;

/**
 * Created by Hasib on 3/10/2018.
 */
public class LinkDTO {
    private String title;
    private String url;

    public LinkDTO() {}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "LinkDTO{" +
            "title='" + title + '\'' +
            ", url='" + url + '\'' +
            '}';
    }
}

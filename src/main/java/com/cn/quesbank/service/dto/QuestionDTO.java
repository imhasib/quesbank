package com.cn.quesbank.service.dto;


import com.cn.quesbank.domain.Question;
import com.cn.quesbank.domain.Tag;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.service.util.Utils;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * A DTO representing a question
 */
public class QuestionDTO implements Serializable {

    private Long id;

    @NotBlank
    private String question;

    private List<AnswerDTO> answers = new ArrayList<>();

    private Set<Tag> tags = new HashSet<>();

    @NotNull
    private Status status;

    private String createdBy;

    public QuestionDTO() {
    }

    public QuestionDTO(Question question) {
        this.setId(question.getId());
        this.setQuestion(question.getQuestion());
        this.setStatus(question.getStatus());
        this.setCreatedBy(question.getCreatedBy().getEmail());
        try {
            this.setAnswers(Utils.parseAnswers(question));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<AnswerDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDTO> answers) {
        this.answers = answers;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "QuestionDTO{" +
            "id=" + id +
            ", question='" + question + '\'' +
            ", answers=" + answers +
            ", tags=" + tags +
            ", status=" + status +
            '}';
    }
}

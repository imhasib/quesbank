package com.cn.quesbank.service.dto;

import java.util.List;

public class DeletedModelTestDTOMin {
    private List<Long> ids;

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return "DeletedModelTestDTOMin{" +
            "ids=" + ids +
            '}';
    }
}

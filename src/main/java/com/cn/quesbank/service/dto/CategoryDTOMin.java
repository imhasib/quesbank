package com.cn.quesbank.service.dto;

import com.cn.quesbank.domain.Category;
import com.cn.quesbank.domain.enumeration.CategoryType;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.service.util.Utils;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;
import java.util.Set;

public class CategoryDTOMin {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("n")
    private String name;

    @JsonProperty("d")
    private String description;

    @JsonProperty("p")
    private Long parent;

    @JsonProperty("t")
    private Set<Long> tagIds = new HashSet<>();

    @JsonProperty("s")
    private Status status;

    @JsonProperty("tp")
    private CategoryType type;

    public CategoryDTOMin(Category category) {
        this.id = category.getId();
        this.name = category.getName();
        this.description = category.getDescription();
        this.parent = category.getParent().getId();
        this.status = category.getStatus();
        this.tagIds = Utils.parseTagIds(category.getTags());
        this.type = category.getType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public Set<Long> getTagIds() {
        return tagIds;
    }

    public void setTagIds(Set<Long> tagIds) {
        this.tagIds = tagIds;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public CategoryType getType() {
        return type;
    }

    public void setType(CategoryType type) {
        this.type = type;
    }
}

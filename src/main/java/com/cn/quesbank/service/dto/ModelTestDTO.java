package com.cn.quesbank.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.cn.quesbank.domain.ModelTest;
import com.cn.quesbank.domain.enumeration.ModelTestType;

/**
 * A DTO for the ModelTest entity.
 */
public class ModelTestDTO implements Serializable {

    private Long id;

    private String name;

    private String description;

    @NotNull
    private ModelTestType type;

    private String subjects;

    private String status;

    @NotNull
    private ZonedDateTime expiryDate;

    private ZonedDateTime availableDate;

    private Set<Long> questionIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ModelTestType getType() {
        return type;
    }

    public void setType(ModelTestType type) {
        this.type = type;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ZonedDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(ZonedDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public ZonedDateTime getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(ZonedDateTime availableDate) {
        this.availableDate = availableDate;
    }

    public Set<Long> getQuestionIds() {
        return questionIds;
    }

    public void setQuestionIds(Set<Long> questionIds) {
        this.questionIds = questionIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModelTestDTO modelTestDTO = (ModelTestDTO) o;
        if(modelTestDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelTestDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelTestDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", type='" + getType() + "'" +
            ", subjects='" + getSubjects() + "'" +
            ", status='" + getStatus() + "'" +
            ", expiryDate='" + getExpiryDate() + "'" +
            ", availableDate='" + getAvailableDate() + "'" +
            "}";
    }
}

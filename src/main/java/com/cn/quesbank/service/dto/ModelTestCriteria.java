package com.cn.quesbank.service.dto;

import com.cn.quesbank.domain.enumeration.Status;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;

/**
 * Created by Hasib on 2018-11-24.
 */
public class ModelTestCriteria implements Serializable {

    /**
     * Class for filtering Status
     */
    public static class StatusFilter extends Filter<Status> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;
    private StatusFilter status;
    private InstantFilter availableDate;
    private InstantFilter expiryDate;
    private InstantFilter createdDate;
    private InstantFilter lastModifiedDate;
    private StringFilter createdBy;


    public ModelTestCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StatusFilter getStatus() {
        return status;
    }

    public void setStatus(StatusFilter status) {
        this.status = status;
    }

    public InstantFilter getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(InstantFilter availableDate) {
        this.availableDate = availableDate;
    }

    public InstantFilter getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(InstantFilter expiryDate) {
        this.expiryDate = expiryDate;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "ModelTestCriteria{" +
            "id=" + id +
            ", status=" + status +
            ", availableDate=" + availableDate +
            ", expiryDate=" + expiryDate +
            ", createdDate=" + createdDate +
            ", lastModifiedDate=" + lastModifiedDate +
            ", createdBy=" + createdBy +
            '}';
    }
}

package com.cn.quesbank.service.dto;

import com.cn.quesbank.domain.Category;

import java.io.Serializable;

/**
 * Created by imhas on 2019-05-03.
 */
public class CategoryExportDto implements Serializable {
    private Long id;
    private String name;
    private Long parentId;
    private String icon;
    private int position = 0;


    public CategoryExportDto() {
    }

    public CategoryExportDto(Category category) {
        this.id = category.getId();
        this.name = category.getName();
        this.parentId = category.getParent().getId();
        this.icon = category.getIcon();
        this.position = category.getPosition();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "CategoryExportDto{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", parentId=" + parentId +
            ", icon='" + icon + '\'' +
            ", position=" + position +
            '}';
    }
}

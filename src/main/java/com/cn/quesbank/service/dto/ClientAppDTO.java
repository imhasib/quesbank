package com.cn.quesbank.service.dto;


import com.cn.quesbank.domain.ClientApp;
import com.cn.quesbank.domain.enumeration.Status;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the ClientApp entity.
 */
public class ClientAppDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String packageName;

    private String description;

    private Status status;

    private CategoryDTO category;

    private String createdBy;

    private Set<UserDTOLite> sharedWith = new HashSet<>();

    private ClientAppSettings settings;

    public ClientAppDTO() {
    }

    public ClientAppDTO(ClientApp clientApp) {
        this.id = clientApp.getId();
        this.name = clientApp.getName();
        this.packageName = clientApp.getPackageName();
        this.description = clientApp.getDescription();
        this.status = clientApp.getStatus();
        this.createdBy = clientApp.getCreatedBy().getEmail();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Set<UserDTOLite> getSharedWith() {
        return sharedWith;
    }

    public void setSharedWith(Set<UserDTOLite> sharedWith) {
        this.sharedWith = sharedWith;
    }

    public CategoryDTO getCategory() {
        return category;
    }

    public void setCategory(CategoryDTO category) {
        this.category = category;
    }

    public ClientAppSettings getSettings() {
        return settings;
    }

    public void setSettings(ClientAppSettings settings) {
        this.settings = settings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientAppDTO clientAppDTO = (ClientAppDTO) o;
        if(clientAppDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientAppDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientAppDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", packageName='" + getPackageName() + "'" +
            ", description='" + getDescription() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}

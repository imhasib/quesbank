package com.cn.quesbank.service.dto;

import com.cn.quesbank.repository.DAO.CountQuestionsByCreatedDateDAO;
import com.cn.quesbank.repository.DAO.CountQuestionsByUsersDAO;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

public class ReportDTO implements Serializable {
    private Long todaysEntry = 0L;
    private Long totalQuestions = 0L;
    private Long totalClientApps = 0L;
    private List<CountQuestionsByUsersDAO> countTotalQuestionsByUsers;
    private List<CountQuestionsByUsersDAO> countTodaysQuestionsByUsers;
    private List<CountQuestionsByCreatedDateDAO> countQuestionsByCreatedDate;

    public ReportDTO() {
    }

    public Long getTodaysEntry() {
        return todaysEntry;
    }

    public void setTodaysEntry(Long todaysEntry) {
        this.todaysEntry = todaysEntry;
    }

    public Long getTotalQuestions() {
        return totalQuestions;
    }

    public void setTotalQuestions(Long totalQuestions) {
        this.totalQuestions = totalQuestions;
    }

    public Long getTotalClientApps() {
        return totalClientApps;
    }

    public void setTotalClientApps(Long totalClientApps) {
        this.totalClientApps = totalClientApps;
    }

    public List<CountQuestionsByUsersDAO> getCountTotalQuestionsByUsers() {
        return countTotalQuestionsByUsers;
    }

    public void setCountTotalQuestionsByUsers(List<CountQuestionsByUsersDAO> countTotalQuestionsByUsers) {
        this.countTotalQuestionsByUsers = countTotalQuestionsByUsers;
    }

    public List<?> getCountQuestionsByCreatedDate() {
        return countQuestionsByCreatedDate;
    }

    public void setCountQuestionsByCreatedDate(List<CountQuestionsByCreatedDateDAO> countQuestionsByCreatedDate) {
        this.countQuestionsByCreatedDate = countQuestionsByCreatedDate;
    }

    public List<CountQuestionsByUsersDAO> getCountTodaysQuestionsByUsers() {
        return countTodaysQuestionsByUsers;
    }

    public void setCountTodaysQuestionsByUsers(List<CountQuestionsByUsersDAO> countTodaysQuestionsByUsers) {
        this.countTodaysQuestionsByUsers = countTodaysQuestionsByUsers;
    }

    public Instant getNow() {
        return Instant.now();
    }

    @Override
    public String toString() {
        return "ReportDTO{" +
            "todaysEntry=" + todaysEntry +
            ", totalQuestions=" + totalQuestions +
            ", totalClientApps" + totalClientApps +
            ", countTotalQuestionsByUsers=" + countTotalQuestionsByUsers +
            ", countTodaysQuestionsByUsers=" + countTodaysQuestionsByUsers +
            ", countQuestionsByCreatedDate=" + countQuestionsByCreatedDate +
            '}';
    }
}

package com.cn.quesbank.service.dto;

import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;

/**
 * Created by Hasib on 2019-04-18.
 */
public class ClientAppCriteria implements Serializable {
    private static final long serialVersionUID = 1L;

    private StringFilter name;
    private InstantFilter createdDate;
    private InstantFilter lastModifiedDate;
    private LongFilter createdBy;

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public LongFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(LongFilter createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "ClientAppCriteria{" +
            "name=" + name +
            ", createdDate=" + createdDate +
            ", lastModifiedDate=" + lastModifiedDate +
            ", createdBy=" + createdBy +
            '}';
    }
}

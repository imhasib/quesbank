package com.cn.quesbank.service.dto;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by Hasib on 2018-11-06.
 */
public class AnswerDTOMin {
    @NotBlank
    private String o;
    private boolean c = false;

    public String getO() {
        return o;
    }

    public void setO(String o) {
        this.o = o;
    }

    public boolean isC() {
        return c;
    }

    public void setC(boolean c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return "AnswerDTOMin{" +
            "Option='" + o + '\'' +
            ", Correct=" + c +
            '}';
    }
}

package com.cn.quesbank.service.dto;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by Hasib on 2018-11-06.
 */
public class QuestionListDTOMin {
    private List<QuestionDTOMin> questions;

    public QuestionListDTOMin() {
    }

    public QuestionListDTOMin(List<QuestionDTOMin> questions) {
        this.questions = questions;
    }

    public List<QuestionDTOMin> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTOMin> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "QuestionListDTOMin{" +
            ", questions=" + questions +
            '}';
    }
}

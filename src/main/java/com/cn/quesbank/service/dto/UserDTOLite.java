package com.cn.quesbank.service.dto;

import com.cn.quesbank.config.Constants;
import com.cn.quesbank.domain.User;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Hasib on 2019-04-06.
 */
public class UserDTOLite  implements Serializable {
    private Long id;

    @NotBlank
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 100)
    private String login;

    @Email
    @Size(min = 5, max = 100)
    private String email;

    public UserDTOLite() {
    }

    public UserDTOLite(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.email = user.getEmail();
    }

    public static User toUser(UserDTOLite userDTOLite) {
        if(userDTOLite == null) {
            return null;
        }

        User user = new User();
        user.setId(userDTOLite.getId());
        user.setLogin(userDTOLite.getLogin());
        user.setEmail(userDTOLite.getEmail());

        return user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

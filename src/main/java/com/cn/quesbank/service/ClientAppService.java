package com.cn.quesbank.service;

import com.cn.quesbank.config.Constants;
import com.cn.quesbank.domain.Category;
import com.cn.quesbank.domain.ClientApp;
import com.cn.quesbank.domain.Question;
import com.cn.quesbank.domain.Tag;
import com.cn.quesbank.domain.enumeration.CategoryType;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.repository.CategoryRepository;
import com.cn.quesbank.repository.ClientAppRepository;
import com.cn.quesbank.security.AuthoritiesConstants;
import com.cn.quesbank.security.SecurityUtils;
import com.cn.quesbank.service.dto.CategoryExportDto;
import com.cn.quesbank.service.dto.ClientAppDTO;
import com.cn.quesbank.service.dto.ClientAppExportDTO;
import com.cn.quesbank.service.dto.QuestionExportDto;
import com.cn.quesbank.service.mapper.ClientAppMapper;
import com.cn.quesbank.service.util.Utils;
import com.cn.quesbank.web.rest.errors.AccessDeniedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.ListUtils;

import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing ClientApp.
 */
@Service
@Transactional
public class ClientAppService {
    private final Logger log = LoggerFactory.getLogger(ClientAppService.class);
    private static final String ENTITY_NAME = "ClientApp";

    private final ClientAppRepository clientAppRepository;
    private final ClientAppMapper clientAppMapper;

    @Inject
    private CategoryRepository categoryRepository;
    @Inject
    private CategoryService categoryService;
    @Inject
    private TagService tagService;

    public ClientAppService(ClientAppRepository clientAppRepository, ClientAppMapper clientAppMapper) {
        this.clientAppRepository = clientAppRepository;
        this.clientAppMapper = clientAppMapper;
    }

    /**
     * Save a clientApp.
     *
     * @param clientApp the entity to save
     * @return the persisted entity
     */
    public ClientApp save(ClientApp clientApp) {
        // TODO: Authorization should be added.
        log.debug("Request to save ClientApp : {}", clientApp);
        Category category = categoryService.save(clientApp.getName(), Constants.CATEGORY_DEFAULT_CLIENT_APP_PARENT_ID, CategoryType.CA);

        clientApp.setStatus(Status.C);
        clientApp.setCategory(category);
        clientApp = clientAppRepository.save(clientApp);

        return clientApp;
    }

    public Optional<ClientAppDTO> update(ClientAppDTO clientAppDTO) {
        log.debug("Request to update ClientApp : {}", clientAppDTO);

        return Optional.of(clientAppRepository.findOne(clientAppDTO.getId()))
            .map(clientApp -> {
                // Client app should be updated only by Creator or Client App Administrator
                    if(SecurityUtils.isCurrentUserInAuthority(AuthoritiesConstants.CLIENT_APP_ADMINISTRATION)
                        || clientApp.getCreatedBy().getId() == SecurityUtils.getCurrentUserId().get()) {
                        clientApp.setName(clientAppDTO.getName());
                        clientApp.setPackageName(clientAppDTO.getPackageName());
                        clientApp.setDescription(clientAppDTO.getDescription());
                        clientApp.setSharedWith(clientAppMapper.toUser(clientAppDTO.getSharedWith()));
                        clientApp.setSettings(Utils.clientAppSettingsToJson(clientAppDTO.getSettings()));
                    } else {
                        throw new AccessDeniedException(this.ENTITY_NAME);
                    }

                return clientAppRepository.save(clientApp);
            }).map(clientAppMapper::toDtoWithSharedWith);
    }

    /**
     * Get all the clientApps.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
//    @Transactional(readOnly = true)
//    public Page<ClientAppDTO> findAll(Pageable pageable) {
//        log.debug("Request to get all ClientApps");
//        return clientAppRepository.findAll(pageable)
//            .map(clientAppMapper::toDto);
//    }

//    public Page<ClientAppDTO> findByOwner(Pageable pageable) {
//        log.debug("Request to get all Clients");
//        Page<ClientAppDTO> result = null;
//
//        if(SecurityUtils.isCurrentUserInAuthority(AuthoritiesConstants.CLIENT_APP_ADMINISTRATION)) {
//            result = findAll(pageable);
//        } else {
//            long userId = SecurityUtils.getCurrentUserId().get();
//            Page<ClientApp> clientApps = clientAppRepository.findByCreatedByOrSharedWith(pageable, userId);
//            result = clientApps.map(clientAppMapper::toDtoWithSharedWith);
//        }
//
//        return result;
//    }

    /**
     * Get one clientApp by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientApp findOne(Long id) {
        log.debug("Request to get ClientApp : {}", id);
        ClientApp clientApp = clientAppRepository.findOne(id);
        return clientApp;
    }

//    public ClientApp findOneWithCategories(Long id) {
//        log.debug("Request to get ClientApp : {}", id);
//        ClientApp clientApp = clientAppRepository.findOneWithCategoriesStatusNotIn(id, Arrays.asList(Status.D));
//        return clientApp;
//    }

    @Transactional(readOnly = true)
    public ClientApp findOneWithSharedWith(Long id) {
        log.debug("Request to get ClientApp : {}", id);
        ClientApp clientApp = clientAppRepository.findOneWithSharedWith(id);
        return clientApp;
    }

    /**
     * Delete the clientApp by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) throws AccessDeniedException {
        log.debug("Request to delete ClientApp : {}", id);
        ClientApp cleintApp = findOne(id);
        if(SecurityUtils.isCurrentUserInAuthority(AuthoritiesConstants.CLIENT_APP_ADMINISTRATION)
            || cleintApp.getCreatedBy().getId() == SecurityUtils.getCurrentUserId().get()) {
            clientAppRepository.delete(id);
        } else {
            throw new AccessDeniedException(this.ENTITY_NAME);
        }
    }

//    @Transactional
//    public Page<Question> getQuestionsByClientAppId(Pageable page, Long id) {
//        log.debug("Request to get Client App with Questions: {}", id);
//        ClientApp clientApp = findOne(id);
//        Page<Question> questions = clientAppRepository.findQuestionsByClientApp(page, clientApp);
//        return questions;
//    }

    @Transactional(readOnly = true)
    public ClientAppExportDTO prepareClientAppData(ClientApp clientApp) {
        List<CategoryExportDto> categories = new ArrayList<>();
        List<QuestionExportDto> questions = new ArrayList<>();
        Set<Long> tagIds = new HashSet<>();


        List<Category> children = categoryRepository.findByParent_Id(clientApp.getCategory().getId());
        for (Category child : children) {
            Category zeroParent = new Category();
            zeroParent.setId(0L);
            child.setParent(zeroParent);
            prepareCategoriesAndQuestionsForExport(child, categories, questions, tagIds);
        }

        Set<Tag> tags = tagService.findTagsByIds(tagIds);

        ClientAppExportDTO dto = new ClientAppExportDTO();
        dto.setCategories(categories);
        dto.setQuestions(questions);
        dto.setTags(tags);
        dto.setDate(ZonedDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        dto.setSettings(Utils.parseClientAppSettings(clientApp.getSettings()));

        return dto;
    }

    @Transactional(readOnly = true)
    public void prepareCategoriesAndQuestionsForExport(Category category, List<CategoryExportDto> categories, List<QuestionExportDto> questions, Set<Long> tagIds) {
        categories.add(new CategoryExportDto(category));
        List<Category> children = categoryRepository.findByParent_Id(category.getId());
        if (ListUtils.isEmpty(children)) {
            List<Question> qs = categoryRepository.findQuestionsByCategoryId(category.getId());
            questions.addAll(qs.stream().map(question -> {
                QuestionExportDto questionExportDto = new QuestionExportDto(question);
                questionExportDto.setCategoryId(category.getId());
                tagIds.addAll(questionExportDto.getTagIds());
                return questionExportDto;
            }).collect(Collectors.toList()));
        }

        for (Category child : children) {
            prepareCategoriesAndQuestionsForExport(child, categories, questions, tagIds);
        }
    }
}

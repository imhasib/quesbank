package com.cn.quesbank.service;

import com.cn.quesbank.repository.ClientAppRepository;
import com.cn.quesbank.repository.DAO.CountQuestionsByCreatedDateDAO;
import com.cn.quesbank.repository.QuestionRepository;
import com.cn.quesbank.service.dto.ReportDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.Instant;
import java.util.Calendar;
import java.util.List;

@Service
@Transactional
public class ReportService {
    private final Logger log = LoggerFactory.getLogger(QuestionService.class);
    @Inject
    private final QuestionRepository questionRepository;

    @Inject
    private ClientAppRepository clientAppRepository;

    public ReportService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public ReportDTO getRepot() {
        ReportDTO reportDTO = new ReportDTO();

        // Total number of questions
        Long totalQuestions = questionRepository.count();
        Long totalClinetApp = clientAppRepository.count();

        reportDTO.setTotalQuestions(totalQuestions);
        reportDTO.setTotalClientApps(totalClinetApp);

        // List of questions by created date
        List<CountQuestionsByCreatedDateDAO> entries = questionRepository.countQuestionsGroupByDay();
        reportDTO.setCountQuestionsByCreatedDate(entries);

        // List of questions for last 24 hours
        Instant today = Calendar.getInstance().toInstant();
        Calendar yesterdayCal = Calendar.getInstance();
        yesterdayCal.add(Calendar.DATE, -1);
        yesterdayCal.set(Calendar.HOUR, 0);
        yesterdayCal.set(Calendar.MINUTE, 0);
        yesterdayCal.set(Calendar.SECOND, 0);
        Instant yesterday = yesterdayCal.toInstant();
        // Total number of questions entered between specific date range
        reportDTO.setTodaysEntry(questionRepository.countByCreatedDateBetween(yesterday, today));

        // Number of questions entered by the user between specific date range
        reportDTO.setCountTodaysQuestionsByUsers(questionRepository.countQuestionsGroupByCreatedBy(yesterday, today));

        // Total number of questions per user
        reportDTO.setCountTotalQuestionsByUsers(questionRepository.countQuestionsGroupByCreatedBy());

        return reportDTO;
    }
}

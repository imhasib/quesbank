package com.cn.quesbank.service;

import com.cn.quesbank.domain.ModelTest;
import com.cn.quesbank.domain.Question;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.repository.ModelTestRepository;
import com.cn.quesbank.service.dto.ModelTestDTO;
import com.cn.quesbank.service.mapper.ModelTestMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.Set;


/**
 * Service Implementation for managing ModelTest.
 */
@Service
@Transactional
public class ModelTestService {

    private final Logger log = LoggerFactory.getLogger(ModelTestService.class);

    private final ModelTestRepository modelTestRepository;

    private final ModelTestMapper modelTestMapper;

    private final QuestionService questionService;

    public ModelTestService(ModelTestRepository modelTestRepository, ModelTestMapper modelTestMapper, QuestionService questionService) {
        this.modelTestRepository = modelTestRepository;
        this.modelTestMapper = modelTestMapper;
        this.questionService = questionService;
    }

    /**
     * Save a modelTest.
     *
     * @param modelTest the entity to save
     * @return the persisted entity
     */
    public ModelTest save(ModelTest modelTest) {
        log.debug("Request to save ModelTest : {}", modelTest);
        modelTest = modelTestRepository.save(modelTest);
        return modelTest;
    }

    /**
     * Get all the modelTests.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ModelTestDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ModelTests");
        return modelTestRepository.findAll(pageable)
            .map(modelTestMapper::toDto);
    }

    /**
     * Get one modelTest by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ModelTest findOne(Long id) {
        log.debug("Request to get ModelTest : {}", id);
        ModelTest modelTest = modelTestRepository.findOne(id);
        return modelTest;
    }

    @Transactional
    public ModelTest findOneWithQuestions(Long id) {
        log.debug("Request to get ModelTest with Questions: {}", id);
        Optional<ModelTest> modelTest = modelTestRepository.findOneWithQuestionsById(id);
        return modelTest.get();
    }

    /**
     * Clean the modelTest by id.
     *
     * @param id the id of the entity
     */
    public void clean(Long id) {
        log.debug("Request to delete ModelTest : {}", id);
        modelTestRepository.delete(id);
    }

    /**
     * Delete the modelTest by id.
     *
     * @param id the id of the entity
     */
    public ModelTest delete(Long id) {
        log.debug("Request to delete ModelTest : {}", id);
        ModelTest modelTest = findOne(id);

        if(modelTest != null) {
            modelTest.setStatus(Status.D);
        }

        modelTestRepository.save(modelTest);

        return modelTest;
    }

    public ModelTest addQuestions(Long modelTestId, List<Long> questionIds) {
        ModelTest modelTest = findOne(modelTestId);

        if(modelTest != null) {
            for(Long questionId : questionIds) {
                Question question =  questionService.findOne(questionId);

                if(question != null) {
                    Set<Question> questions = modelTest.getQuestions();
                    questions.add(question);
                }
            }
            modelTest = save(modelTest);
        }

        return modelTest;
    }
}

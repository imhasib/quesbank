package com.cn.quesbank.service;

import com.cn.quesbank.config.Constants;
import com.cn.quesbank.domain.Category;
import com.cn.quesbank.domain.Category_;
import com.cn.quesbank.domain.enumeration.CategoryType;
import com.cn.quesbank.repository.CategoryRepository;
import com.cn.quesbank.service.dto.CategoryCriteria;
import com.cn.quesbank.service.dto.CategoryDTO;
import com.cn.quesbank.service.mapper.CategoryMapper;
import com.cn.quesbank.service.util.Utils;
import com.cn.quesbank.web.rest.errors.AccessDeniedException;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

/**
 * Service for executing complex queries for Category entities in the database.
 * The main input is a {@link CategoryCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CategoryDTO} or a {@link Page} of {@link CategoryDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CategoryQueryService extends QueryService<Category> {

    private final Logger log = LoggerFactory.getLogger(CategoryQueryService.class);


    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;
    @Inject
    private CategoryService categoryService;
    @Inject
    private TagService tagService;

    public CategoryQueryService(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    /**
     * Return a {@link List} of {@link CategoryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CategoryDTO> findByCriteria(CategoryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Category> specification = createSpecification(criteria);
        return categoryMapper.toDtos(categoryRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CategoryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Category> findByCriteria(CategoryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        if (criteria.getParent() != null) {
            Category parent = categoryRepository.findOne(criteria.getParent().getEquals());
            if (CategoryType.CA.equals(parent.getType()) && !categoryService.isRequesterOwnerOfCategory(parent)) {
                throw new AccessDeniedException("Category");
            }
        }


        final Specifications<Category> specification = createSpecification(criteria);
        final Page<Category> result = categoryRepository.findAll(specification, page);
        return result;
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CategoryCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specifications<Category> specification = createSpecification(criteria);
        return categoryRepository.count(specification);
    }

    /**
     * Function to convert CategoryCriteria to a {@link Specifications}
     */
    private Specifications<Category> createSpecification(CategoryCriteria criteria) {
        Specifications<Category> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Category_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Category_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Category_.description));
            }
            if(criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), Category_.lastModifiedDate));
            }
            if(criteria.getTags() != null) {
                Set<Long> tagIds = Utils.parseObjectIds(criteria.getTags().getContains(), ",");
                for(Long tagId: tagIds) {
                    StringFilter tagFilter = new StringFilter();
                    tagFilter.setContains(Utils.wrapTags(tagId.toString()));
                    specification = specification.or(buildStringSpecification(tagFilter, Category_.tags));
                }
            }

            if (criteria.getParent() == null || criteria.getParent().getEquals() < 5L) {
                LongFilter longFilter = new LongFilter();
                longFilter.setEquals(Constants.CATEGORY_DEFAULT_OTHER_PARENT_ID);
                criteria.setParent(longFilter);
            }
            specification = specification.and(buildReferringEntitySpecification(criteria.getParent(), Category_.parent, Category_.id));
        }
        return specification;
    }
}

package com.cn.quesbank.service;

/**
 * Created by Hasib on 2018-11-24.
 */
import java.util.*;

import com.cn.quesbank.repository.ModelTestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.cn.quesbank.domain.ModelTest;
import com.cn.quesbank.domain.*; // for static metamodels
import com.cn.quesbank.service.dto.ModelTestCriteria;

import javax.persistence.criteria.*;

/**
 * Service for executing complex queries for ModelTest entities in the database.
 * The main input is a {@link ModelTestCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ModelTest} or a {@link Page} of {@link ModelTest} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ModelTestQueryService extends QueryService<ModelTest> {

    private final Logger log = LoggerFactory.getLogger(ModelTestQueryService.class);

    private final ModelTestRepository modelTestRepository;

    public ModelTestQueryService(ModelTestRepository modelTestRepository) {
        this.modelTestRepository = modelTestRepository;
    }

    /**
     * Return a {@link List} of {@link ModelTest} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ModelTest> findByCriteria(ModelTestCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ModelTest> specification = createSpecification(criteria);
        return modelTestRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ModelTest} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ModelTest> findByCriteria(ModelTestCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ModelTest> specification = createSpecification(criteria);
        return modelTestRepository.findAll(specification, page);
    }

    /**
     * Function to convert ModelTestCriteria to a {@link Specifications}
     */
    private Specifications<ModelTest> createSpecification(ModelTestCriteria criteria) {
        Specifications<ModelTest> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ModelTest_.id));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), ModelTest_.status));
            }
            if(criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ModelTest_.createdDate));
            }
            if(criteria.getCreatedBy() != null) {
                specification = specification.and(modelTestCreatedByisEquals(criteria.getCreatedBy().getEquals()));
            }

            if(criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), ModelTest_.lastModifiedDate));
            }
        }
        return specification;
    }

    public Specification<ModelTest> modelTestCreatedByisEquals(String login) {
        return new Specification<ModelTest>() {
            @Override
            public Predicate toPredicate(Root<ModelTest> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                Join<ModelTest, User> createdBy = root.join(ModelTest_.createdBy, JoinType.INNER);

                return builder.equal(createdBy.get(User_.login), login);
            }
        };
    }
}

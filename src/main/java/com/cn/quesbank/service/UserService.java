package com.cn.quesbank.service;

import com.cn.quesbank.domain.Role;
import com.cn.quesbank.domain.User;
import com.cn.quesbank.config.Constants;
import com.cn.quesbank.domain.enumeration.Status;
import com.cn.quesbank.repository.RoleRepository;
import com.cn.quesbank.repository.UserRepository;
import com.cn.quesbank.security.AuthoritiesConstants;
import com.cn.quesbank.security.SecurityUtils;
import com.cn.quesbank.service.mapper.UserMapper;
import com.cn.quesbank.service.util.RandomUtil;
import com.cn.quesbank.service.dto.UserDTO;

import com.cn.quesbank.web.rest.errors.AccessDeniedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);
    private static final String ENTITY_NAME = "User";

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final SocialService socialService;
    @Inject
    private UserMapper userMapper;
    @Inject
    private RoleRepository roleRepository;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, SocialService socialService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.socialService = socialService;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
       log.debug("Reset user password for reset key {}", key);

       return userRepository.findOneByResetKey(key)
           .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
           .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                return user;
           });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmailIgnoreCase(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                return user;
            });
    }

    public User registerUser(UserDTO userDTO, String password) {
        User newUser = new User();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(userDTO.getLogin());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        newUser.setEmail(userDTO.getEmail());
        newUser.setImageUrl(userDTO.getImageUrl());
        newUser.setLangKey(userDTO.getLangKey());
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public User createUser(UserDTO userDTO) {
        // TODO: Authorization should be added
        User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setMobile(userDTO.getMobile());
        user.setImageUrl(userDTO.getImageUrl());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        if (userDTO.getRoles() != null) {
            Set<Role> roles = userDTO.getRoles();
            user.setRoles(roles);
        }
        String encryptedPassword = passwordEncoder.encode(userDTO.getPassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        userRepository.save(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user
     * @param lastName last name of user
     * @param email email id of user
     * @param langKey language key
     * @param imageUrl image URL of user
     */
    public void updateUser(String firstName, String lastName, String email, String mobile, String langKey, String imageUrl) {
        SecurityUtils.getCurrentUserId()
            .flatMap(userRepository::findOneById)
            .ifPresent(user -> {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setMobile(mobile);
                user.setLangKey(langKey);
                user.setImageUrl(imageUrl);
                log.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update
     * @return updated user
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional.of(userRepository
            .findOne(userDTO.getId()))
            .map(user -> {
                long requesterId = SecurityUtils.getCurrentUserId().get();
                // Admin & System user shouldn't be updated by Others.
                if((user.getId() == Constants.SYSTEM_ACCOUNT_ID || user.getId() == Constants.ADMIN_ACCOUNT_ID) && requesterId != user.getId()) {
                    throw new AccessDeniedException(this.ENTITY_NAME);
                }

//                user.setLogin(userDTO.getLogin());
                user.setFirstName(userDTO.getFirstName());
                user.setLastName(userDTO.getLastName());
                user.setEmail(userDTO.getEmail());
                user.setMobile(userDTO.getMobile());
                user.setImageUrl(userDTO.getImageUrl());
                user.setActivated(userDTO.isActivated());
                user.setLangKey(userDTO.getLangKey());
                Set<Role> managedRoles = user.getRoles();
                managedRoles.clear();
                userDTO.getRoles().stream()
                    .map(role -> {
                        return roleRepository.findOne(role.getId());
                    })
                    .forEach(managedRoles::add);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(userMapper::toDto);
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            // Admin & System user shouldn't be deleted
            if(user.getId() == Constants.SYSTEM_ACCOUNT_ID || user.getId() == Constants.ADMIN_ACCOUNT_ID) {
                throw new AccessDeniedException(this.ENTITY_NAME);
            }

            socialService.deleteUserSocialConnection(user.getLogin());
            user.setStatus(Status.D);
            userRepository.save(user);
            log.debug("User flaged as status 'D': {}", user);

        });
    }

    public void changePassword(String password) {
        SecurityUtils.getCurrentUserId()
            .flatMap(userRepository::findOneById)
            .ifPresent(user -> {
                String encryptedPassword = passwordEncoder.encode(password);
                user.setPassword(encryptedPassword);
                log.debug("Changed password for User: {}", user);
            });
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        if(SecurityUtils.isCurrentUserInAuthority(AuthoritiesConstants.USER_WRITE)) {
            return userRepository.findAllByStatusNot(pageable, Status.D).map(userMapper::toDto);
        }

        return null;
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithRolesByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithRoles(Long id) {
        return userRepository.findOneWithRolesById(id);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithRoles() {
        return getUserWithRoles(SecurityUtils.getCurrentUserId().get());
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS));
        for (User user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
        }
    }

    public boolean hasRole(User user, String roleName) {
        Set<Role> roles = user.getRoles();

        for(Role role : roles) {
            if(role.getName().equals(roleName)) {
                return true;
            }
        }

        return false;
    }

}

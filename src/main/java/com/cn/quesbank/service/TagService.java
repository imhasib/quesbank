package com.cn.quesbank.service;

import com.cn.quesbank.domain.Tag;
import com.cn.quesbank.repository.TagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 * Service Implementation for managing Tag.
 *
 */
@Service
@Transactional
public class TagService {

    private final Logger log = LoggerFactory.getLogger(TagService.class);

    private final TagRepository tagRepository;

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    /**
     * Save a tag.
     *
     * @param tag the entity to save
     * @return the persisted entity
     */
    public Tag save(Tag tag) {
        log.debug("Request to save Tag : {}", tag);
        return tagRepository.save(tag);
    }

    public List<Tag> saveAll(Set<String> tags) {
        if(tags != null) {
            Iterator<String> iterator = tags.iterator();
            while(iterator.hasNext()) {
                String tagStr = iterator.next();

                Tag tag = findOneByTag(tagStr);
                if(tag == null) {
                    tag = new Tag();
                    tag.setName(tagStr);
                    tag.setDisplayName(tagStr);
                    save(tag);
                }
            }
        }

        return null;
    }

    /**
     * Get all the tags.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Tag> findAll(Pageable pageable) {
        log.debug("Request to get all Tags");
        return tagRepository.findAll(pageable);
    }

    /**
     * Get one tag by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Tag findOne(Long id) {
        log.debug("Request to get Tag : {}", id);
        return tagRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public Tag findOneByTag(String tag) {
        log.debug("Request to get Tag : {}", tag);
        return tagRepository.findOneByName(tag);
    }

    /**
     * Delete the tag by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Tag : {}", id);
        tagRepository.delete(id);
    }

    public Set<Tag> storeTagsIfNotExist(Set<Tag> tags) {
        Set<Tag> filtersTags = new HashSet<>();

        if(tags != null) {
            for(Tag tag: tags) {
                Tag t = tagRepository.findOneByName(tag.getName());

                if(t == null) {
                    t = save(new Tag().tag(tag.getName()));
                }

                filtersTags.add(t);
            }
        }

        return filtersTags;
    }

    public Set<Tag> findTagsByIds(Set<Long> tagIds) {
        Set<Tag> tags = new HashSet<>();
        if(tagIds != null) {
            tags = tagRepository.findByIdIn(tagIds);
        }

        return tags;
    }
}

package com.cn.quesbank.service;

import com.cn.quesbank.domain.ClientApp;
import com.cn.quesbank.domain.ClientApp_;
import com.cn.quesbank.domain.User;
import com.cn.quesbank.domain.User_;
import com.cn.quesbank.repository.ClientAppRepository;
import com.cn.quesbank.security.AuthoritiesConstants;
import com.cn.quesbank.security.SecurityUtils;
import com.cn.quesbank.service.dto.ClientAppCriteria;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.criteria.*;

/**
 * Created by Hasib on 2019-04-18.
 */
@Service
@Transactional(readOnly = true)
public class ClientAppQueryService extends QueryService<ClientApp> {

    private final Logger log = LoggerFactory.getLogger(ClientAppQueryService.class);

    @Inject
    private ClientAppRepository repository;

    @Transactional(readOnly = true)
    public Page<ClientApp> findByCriteria(ClientAppCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ClientApp> specification = createSpecification(criteria);
        return repository.findAll(specification, page);
    }

    private Specifications<ClientApp> createSpecification(ClientAppCriteria criteria) {
        Specifications<ClientApp> specification = Specifications.where(null);
        if (criteria != null) {
            if (SecurityUtils.isCurrentUserInAuthority(AuthoritiesConstants.CLIENT_APP_ADMINISTRATION)) {
                if (criteria.getCreatedBy() != null) {
                    specification = specification.and(clientAppCreatedByIsEqual(criteria.getCreatedBy().getEquals()));
                }
            } else {
                specification = specification.or(clientAppCreatedByIsEqual(SecurityUtils.getCurrentUserId().get()));
                specification = specification.or(clientAppSharedWithIsEqual(SecurityUtils.getCurrentUserId().get()));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ClientApp_.name));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ClientApp_.createdDate));
            }

            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), ClientApp_.lastModifiedDate));
            }
        }
        return specification;
    }

    public Specification<ClientApp> clientAppCreatedByIsEqual(long userId) {
        return new Specification<ClientApp>() {
            @Override
            public Predicate toPredicate(Root<ClientApp> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                criteriaQuery.distinct(true);
                Join<ClientApp, User> createdBy = root.join(ClientApp_.createdBy, JoinType.LEFT);

                return criteriaBuilder.equal(createdBy.get(User_.id), userId);
            }
        };
    }

    public Specification<ClientApp> clientAppSharedWithIsEqual(long userId) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            criteriaQuery.distinct(true);
            Join<ClientApp, User> sharedWith = root.join(ClientApp_.sharedWith, JoinType.LEFT);

//            Join<ClientApp, User> createdBy = root.join(ClientApp_.createdBy, JoinType.LEFT);
//            return criteriaBuilder.or(criteriaBuilder.equal(sharedWith.get(User_.id), userId), criteriaBuilder.equal(createdBy.get(User_.id), userId));

            return criteriaBuilder.equal(sharedWith.get(User_.id), userId);
        };
    }

}

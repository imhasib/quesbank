(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();

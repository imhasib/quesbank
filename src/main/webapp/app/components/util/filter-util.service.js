(function () {
    'use strict';

    angular
        .module('quesbankApp')
        .factory('FilterUtil', FilterUtil);

    function FilterUtil () {

        var service = {
            toTagIdsStr: toTagIdsStr
        };

        return service;


        function arrayToStr(tags) {
            if(tags != null && tags.length > 0) {
                return tags.join(",")
            }

            return "";
        }

        function toTagIdsStr(tags) {
            var tagIds = [];
            if(tags) {
                for(var i=0; i<tags.length; i++) {
                    var tag = tags[i];
                    if(tag.id) {
                        tagIds.push(tag.id);
                    }
                }
            }

            return arrayToStr(tagIds);
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .filter('tagName', tagName);

    function tagName() {
        return tagNameFilter;


        function tagNameFilter (input) {
            var tagNames = [];
            for(var i=0; i<input.length; i++) {
                var tag = input[i];
                tagNames.push(tag.name);
            }

            return tagNames;
        }
    }
})();

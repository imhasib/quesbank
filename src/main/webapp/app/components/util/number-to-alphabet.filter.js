(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .filter('toAlphabet', toAlphabet);

    function toAlphabet() {

        var alphabets = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

        return toAlphabetFilter;

        function toAlphabetFilter (input) {
            if(!isNaN(input) && input < 26) {
                return alphabets[input]
            }
            return input;
        }
    }
})();

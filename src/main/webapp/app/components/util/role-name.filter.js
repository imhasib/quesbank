(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .filter('roleName', roleName);

    // polyfill
    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function(search, pos) {
            return this.substr(!pos || pos < 0 ? 0 : +pos, search.length) === search;
        };
    }

    function roleName() {
        return roleNameFilter;


        function roleNameFilter (input) {
            var ROLE_PREFIX = "ROLE_";
            if (input !== null && input.startsWith(ROLE_PREFIX)) {
                input = input.substring(ROLE_PREFIX.length);
            }
            return input;
        }
    }
})();

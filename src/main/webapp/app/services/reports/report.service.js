(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .factory('ReportService', ReportService);

    ReportService.$inject = ['$http'];

    function ReportService($http) {

        var dataPromise;

        var service = {
            getReport : getReport
        };

        return service;

        function getReport() {
            if (angular.isUndefined(dataPromise)) {
                dataPromise = $http.get('api/report').then(function(result) {
                    if (result.data) {
                        var response = result.data;

                        return response;
                    }
                });
            }
            return dataPromise;
        }
    }
})();

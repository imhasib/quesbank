(function () {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('UserManagementDialogController', UserManagementDialogController);

    UserManagementDialogController.$inject = ['$stateParams', '$uibModalInstance', 'entity', 'User', 'Principal', 'Role'];

    function UserManagementDialogController($stateParams, $uibModalInstance, entity, User, Principal, Role) {
        var vm = this;
        vm.roles = [];

        function loadAllAuthorities () {
            Role.query({}, onSuccess, onError);
            function onSuccess(data) {
                vm.roles = data;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }
        loadAllAuthorities ();

        var result = Principal.hasAnyAuthority(['ROLE_ADMIN']);

        if(result) {
            vm.authorities.push({
                'value': 'ROLE_ADMIN',
                'text': 'ADMIN'
            });
        }
        vm.clear = clear;
        vm.languages = null;
        vm.save = save;
        vm.user = entity;


        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function onSaveSuccess(result) {
            vm.isSaving = false;
            $uibModalInstance.close(result);
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function save() {
            if(vm.user.password == vm.confirmPassword) {
                vm.isSaving = true;
                if (vm.user.id !== null) {
                    User.update(vm.user, onSaveSuccess, onSaveError);
                } else {
                    vm.user.langKey = 'en';
                    User.save(vm.user, onSaveSuccess, onSaveError);
                }
            } else {
                vm.doNotMatch = 'ERROR';
            }

        }


        // function processAuthorities() {
        //     if(vm.user.authorities.includes("ROLE_ADMIN")) {
        //         vm.user.authorities = ["ROLE_ADMIN", "ROLE_PUBLISHER_ADMIN", "ROLE_APP_PUBLISHER", "ROLE_USER", "ROLE_DATA_OPERATOR"];
        //     } else if(vm.user.authorities.includes("ROLE_PUBLISHER_ADMIN")) {
        //         vm.user.authorities = ["ROLE_PUBLISHER_ADMIN", "ROLE_APP_PUBLISHER", "ROLE_USER", "ROLE_DATA_OPERATOR"];
        //     } else if(vm.user.authorities.includes("ROLE_APP_PUBLISHER")) {
        //         vm.user.authorities = ["ROLE_APP_PUBLISHER", "ROLE_USER", "ROLE_DATA_OPERATOR"];
        //     } else if(vm.user.authorities.includes("ROLE_DATA_OPERATOR")) {
        //         vm.user.authorities = ["ROLE_USER", "ROLE_DATA_OPERATOR"];
        //     }
        // }
    }
})();

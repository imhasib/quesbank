(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('QuestionController', QuestionController);

    QuestionController.$inject = ['$state', 'Question', 'ParseLinks', 'AlertService', 'paginationConstants', 'pagingParams', 'FilterUtil', 'ModelTest', 'path'];

    function QuestionController($state, Question, ParseLinks, AlertService, paginationConstants, pagingParams, FilterUtil, ModelTest, path) {

        var vm = this;

        vm.path = path;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.filter = pagingParams.filter;
        vm.modelTestId = pagingParams.modelTestId;

        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        loadAll();

        function loadAll () {
            var params = {
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
                sort: sort(),
                "question.contains": vm.filter.search,
                "tags.contains": FilterUtil.toTagIdsStr(vm.filter.tags),
                "categories.contains": vm.filter.categories,
                "status.equals": vm.filter.status,
                "createdBy.equals": vm.filter.createdBy,
                "categoryId.equals": vm.filter.categoryId
            }

            if(vm.filter.createdDateFrom) {
                params["createdDate.greaterThan"] = vm.filter.createdDateFrom + "T00:00:00Z";
            }
            if(vm.filter.createdDateTo) {
                params["createdDate.lessThan"] = vm.filter.createdDateTo + "T23:59:59Z";
            }

            Question.query(params, onSuccess, onError);
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.questions = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            var params = Object.assign({
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch,
                modelTestId: vm.modelTestId
            }, vm.filter);

            $state.transitionTo($state.$current, params);
        }

        vm.addToModelTest = function(question) {
            ModelTest.addQuestions({
                modelTestId: vm.modelTestId,
                questionIds: vm.getSelectedIds()
            }, onSuccess, onError);

            function onSuccess(data, header) {
                console.log(data);
                question.addedToModelTest = true;
            }

            function onError(error) {
                console.log(error);
            }
        }


        vm.getSelectedIds = function() {
            var selectedIds = [];
            for(var i=0;i<vm.questions.length; i++) {
                if(vm.questions[i].isSelected) {
                    selectedIds.push(vm.questions[i].id);
                }
            }

            return selectedIds;
        }
    }
})();

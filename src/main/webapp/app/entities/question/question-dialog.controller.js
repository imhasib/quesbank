(function () {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('QuestionDialogController', QuestionDialogController);

    QuestionDialogController.$inject = ['$timeout', '$scope', '$uibModalInstance', 'entity', 'Question', '$http'];

    function QuestionDialogController($timeout, $scope, $uibModalInstance, entity, Question, $http) {
        var vm = this;

        vm.question = entity;
        vm.clear = clear;
        vm.save = save;
        vm.saveAndCreateNew = saveAndCreateNew;
        vm.showKeyboardLayout = false;

        vm.removeAnswer = function (i) {
            vm.question.answers.splice(i, 1);
            if (vm.question.answers.length < 4) {
                vm.question.answers.push({});
            }
        }

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.question.id !== null) {
                Question.update(vm.question, onSaveSuccess, onSaveError);
            } else {
                Question.save(vm.question, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('quesbankApp:questionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.loadTags = function (query) {
            return $http.get('api/search-tags/' + query);
        }

        function saveAndCreateNew() {
            if (vm.question.id == null) {
                vm.isSaving = true;
                Question.save(vm.question, onSaveAndCreateNewSuccess, onSaveError);
            }
        }

        function onSaveAndCreateNewSuccess(result) {
            result.createNew = true;
            onSaveSuccess(result)

            // $scope.$emit('quesbankApp:questionUpdate', result);
            // vm.isSaving = false;
            // vm.reset();
        }

        vm.reset = function () {
            vm.question = {
                id: null,
                question: "",
                answers: [{}, {}, {}, {}],
                tags: vm.question.tags,
                status: 'C'
            }
            $timeout(function () {
                angular.element('#field_question')[0].focus();
            });
        }
    }
})();

(function () {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('QuestionFilterDialogController', QuestionFilterDialogController);

    QuestionFilterDialogController.$inject = ['$timeout', '$uibModalInstance', 'filterParams', '$http', 'AlertService'];

    function QuestionFilterDialogController($timeout, $uibModalInstance, filterParams, $http, AlertService) {
        var vm = this;

        vm.filter = filterParams;

        vm.reset = reset;
        vm.applyFilters = applyFilters;
        vm.clear = clear;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function reset() {
            vm.filter = {
                search: null,
                tags: [],
                status: null,
                createdBy: null,
                clientAppId: null
            };
        }

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function applyFilters() {
            vm.isFiltering = true;
            $uibModalInstance.close(vm.filter);
            vm.isFiltering = false;
        }

        vm.loadTags = function (query) {
            return $http.get('api/search-tags/' + query);
        }


        vm.configureDatePicker = function () {
            $('#sandbox-container .input-daterange').datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true
            });
        }
    }
})();

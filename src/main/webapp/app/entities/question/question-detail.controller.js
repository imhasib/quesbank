(function () {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('QuestionDetailController', QuestionDetailController);

    QuestionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Question'];

    function QuestionDetailController($scope, $rootScope, $stateParams, previousState, entity, Question) {
        var vm = this;

        vm.question = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('quesbankApp:questionUpdate', function (event, result) {
            vm.question = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

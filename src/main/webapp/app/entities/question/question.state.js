(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('question', {
                parent: 'entity',
                url: '/question?page&sort&search&status&categories&createdBy&createdDateFrom&createdDateTo&modelTestId&cid',
                data: {
                    authorities: ['question.read'],
                    pageTitle: 'Questions'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/question/questions.html',
                        controller: 'QuestionController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: {
                        value: null,
                        squash: true
                    },
                    status: {
                        value: null,
                        squash: true
                    },
                    categories: {
                        value: null,
                        squash: true
                    },
                    tags: {
                        value: null,
                        squash: true
                    },
                    createdBy: {
                        value: null,
                        squash: true
                    },
                    createdDateFrom: {
                        value: null,
                        squash: true
                    },
                    createdDateTo: {
                        value: null,
                        squash: true
                    },
                    modelTestId : null,
                    cid: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),

                            filter: {
                                search: $stateParams.search,
                                status: $stateParams.status,
                                tags: $stateParams.tags,
                                categories: $stateParams.categories,
                                createdBy: $stateParams.createdBy,
                                createdDateFrom: $stateParams.createdDateFrom,
                                createdDateTo: $stateParams.createdDateTo,
                                categoryId: $stateParams.cid
                            },
                            modelTestId: $stateParams.modelTestId
                        };
                    }],
                    path: ['$stateParams', 'Category', function ($stateParams, Category) {
                        if ($stateParams.cid) {
                            return Category.path({id: $stateParams.cid}).$promise;
                        }
                    }]
                }
            })
            .state('question-detail', {
                parent: 'question',
                url: '/question/{id}',
                data: {
                    authorities: ['question.read'],
                    pageTitle: 'Question'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/question/question-detail.html',
                        controller: 'QuestionDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Question', function($stateParams, Question) {
                        return Question.get({id : $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'question',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('question-detail.edit', {
                parent: 'question-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['question.write']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/question/question-dialog.html',
                        controller: 'QuestionDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Question', function(Question) {
                                return Question.get({id : $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('^', {}, { reload: false });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('question.new', {
                parent: 'question',
                url: '/new',
                data: {
                    authorities: ['question.write']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/question/question-dialog.html',
                        controller: 'QuestionDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    question: null,
                                    answers: [{},{},{},{}],
                                    status: 'C',
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        if(result.createNew) {
                            $state.go('question.new', null, { reload: 'question' });
                        } else {
                            $state.go('question', null, { reload: 'question' });
                        }
                    }, function() {
                        $state.go('question');
                    });
                }]
            })
            .state('question.edit', {
                parent: 'question',
                url: '/{id}/edit',
                data: {
                    authorities: ['question.write']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/question/question-dialog.html',
                        controller: 'QuestionDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Question', function(Question) {
                                return Question.get({id : $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('question', null, { reload: 'question' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('question.delete', {
                parent: 'question',
                url: '/{id}/delete',
                data: {
                    authorities: ['question.delete']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/question/question-delete-dialog.html',
                        controller: 'QuestionDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Question', function(Question) {
                                return Question.get({id : $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('question', null, { reload: 'question' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('question.filter', {
                parent: 'question',
                url: '/filter',
                params: {
                    search: {
                        value: null,
                        squash: true
                    },
                    status: {
                        value: null,
                        squash: true
                    },
                    tags: {
                        value: null,
                        squash: true
                    },
                    createdBy: {
                        value: null,
                        squash: true
                    },
                    createdDateFrom: {
                        value: null,
                        squash: true
                    },
                    createdDateTo: {
                        value: null,
                        squash: true
                    },
                    cid: null
                },
                data: {
                    authorities: ['question.read']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/question/question-filter-dialog.html',
                        controller: 'QuestionFilterDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'md',
                        resolve: {
                            filterParams: ['Question', function(Question) {
                                return {
                                    search: $stateParams.search,
                                    status: $stateParams.status,
                                    tags: $stateParams.tags,
                                    createdBy: $stateParams.createdBy,
                                    createdDateFrom: $stateParams.createdDateFrom,
                                    createdDateTo: $stateParams.createdDateTo,
                                    categoryId: $stateParams.cid
                                };
                            }]
                        }
                    }).result.then(function(filter) {
                        var params = Object.assign({page: 1}, filter);
                        $state.go('question', params, { location: true, reload: 'question' });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('question.addToCategory', {
                parent: 'question',
                url: '/category/{cid}',
                params: {
                    cid: null
                },
                data: {
                    authorities: ['clientApp.read']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', 'Category', function ($stateParams, $state, $uibModal, Category) {
                    $uibModal.open({
                        templateUrl: 'app/entities/question/question-dialog.html',
                        controller: 'QuestionDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    question: null,
                                    answers: [{}, {}, {}, {}],
                                    status: 'C',
                                    id: null,
                                    cid: $stateParams.cid
                                };
                            }
                        }
                    }).result.then(function (result) {
                        var questions = [result.id];
                        var cid = $stateParams.cid;
                        if (cid && cid > 0) {
                            Category.addQuestions({
                                categoryId: cid,
                                questionIds: questions
                            }, onSuccess, onError);
                        }

                        function onSuccess(data, header) {
                            console.log(data);
                        }

                        function onError(error) {
                            console.log(error);
                        }

                        if (result.createNew) {
                            $state.go('question.addToCategory', {cid: $stateParams.cid}, {reload: 'question'});
                        } else {
                            $state.go('question', {cid: $stateParams.cid}, {reload: 'question'});
                        }
                    }, function () {
                        $state.go('question', {cid: $stateParams.cid}, {reload: false});
                    });
                }]
            });
    }

})();

(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('model-test', {
            parent: 'entity',
            url: '/model-test?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ModelTests'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/model-test/model-tests.html',
                    controller: 'ModelTestController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('model-test-detail', {
            parent: 'model-test',
            url: '/model-test/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ModelTest'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/model-test/model-test-detail.html',
                    controller: 'ModelTestDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'ModelTest', function($stateParams, ModelTest) {
                    return ModelTest.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'model-test',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('model-test-detail.edit', {
            parent: 'model-test-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/model-test/model-test-dialog.html',
                    controller: 'ModelTestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ModelTest', function(ModelTest) {
                            return ModelTest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('model-test.new', {
            parent: 'model-test',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/model-test/model-test-dialog.html',
                    controller: 'ModelTestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                type: 'D',
                                subjects: null,
                                status: null,
                                expiryDate: null,
                                availableDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('model-test', null, { reload: 'model-test' });
                }, function() {
                    $state.go('model-test');
                });
            }]
        })
        .state('model-test.edit', {
            parent: 'model-test',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/model-test/model-test-dialog.html',
                    controller: 'ModelTestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ModelTest', function(ModelTest) {
                            return ModelTest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('model-test', null, { reload: 'model-test' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('model-test.delete', {
            parent: 'model-test',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/model-test/model-test-delete-dialog.html',
                    controller: 'ModelTestDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ModelTest', function(ModelTest) {
                            return ModelTest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('model-test', null, { reload: 'model-test' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

(function() {
    'use strict';
    angular
        .module('quesbankApp')
        .factory('ModelTest', ModelTest);

    ModelTest.$inject = ['$resource', 'DateUtils'];

    function ModelTest ($resource, DateUtils) {
        var resourceUrl =  'api/model-tests/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.expiryDate = DateUtils.convertDateTimeFromServer(data.expiryDate);
                        data.availableDate = DateUtils.convertDateTimeFromServer(data.availableDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' },
            'addQuestions': { url: 'api/model-test/add-questions', method: 'POST', isArray: true}
        });
    }
})();

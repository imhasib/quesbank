(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('ModelTestDialogController', ModelTestDialogController);

    ModelTestDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ModelTest', 'Question'];

    function ModelTestDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ModelTest, Question) {
        var vm = this;

        vm.modelTest = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.questions = Question.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.modelTest.id !== null) {
                ModelTest.update(vm.modelTest, onSaveSuccess, onSaveError);
            } else {
                ModelTest.save(vm.modelTest, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('quesbankApp:modelTestUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.expiryDate = false;
        vm.datePickerOpenStatus.availableDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('ModelTestDeleteController',ModelTestDeleteController);

    ModelTestDeleteController.$inject = ['$uibModalInstance', 'entity', 'ModelTest'];

    function ModelTestDeleteController($uibModalInstance, entity, ModelTest) {
        var vm = this;

        vm.modelTest = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ModelTest.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

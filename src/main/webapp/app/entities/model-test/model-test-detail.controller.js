(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('ModelTestDetailController', ModelTestDetailController);

    ModelTestDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ModelTest', 'Question'];

    function ModelTestDetailController($scope, $rootScope, $stateParams, previousState, entity, ModelTest, Question) {
        var vm = this;

        vm.modelTest = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('quesbankApp:modelTestUpdate', function(event, result) {
            vm.modelTest = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

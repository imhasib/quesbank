(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('RoleDialogController', RoleDialogController);

    RoleDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Role'];

    function RoleDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Role) {
        var vm = this;
        vm.word = /^\s*\w+?[-]+?( \w+)*\s*$/;
        vm.role = entity;
        vm.clear = clear;
        vm.save = save;
        vm.authorities = Role.authorities();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.role.id !== null) {
                Role.update(vm.role, onSaveSuccess, onSaveError);
            } else {
                Role.save(vm.role, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('quesbankApp:roleUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();

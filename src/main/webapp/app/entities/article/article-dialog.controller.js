(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('ArticleDialogController', ArticleDialogController);

    ArticleDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Article', '$http'];

    function ArticleDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Article, $http) {
        var vm = this;

        vm.article = entity;
        vm.clear = clear;
        vm.save = save;
        $scope.contentEditorOptions = {
            // settings more at http://docs.ckeditor.com/#!/guide/dev_configuration
            height: '200px'
        }

        $scope.summaryEditorOptions = {
            // settings more at http://docs.ckeditor.com/#!/guide/dev_configuration
            height: '100px'
        }

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.article.id !== null) {
                Article.update(vm.article, onSaveSuccess, onSaveError);
            } else {
                Article.save(vm.article, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('quesbankApp:articleUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.loadTags = function (query) {
            return $http.get('api/search-tags/' + query);
        }

        vm.removeLink = function($index) {
            vm.article.links.splice(i, 1);
        }
    }
})();

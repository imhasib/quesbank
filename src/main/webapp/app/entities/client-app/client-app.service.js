(function() {
    'use strict';
    angular
        .module('quesbankApp')
        .factory('ClientApp', ClientApp);

    ClientApp.$inject = ['$resource'];

    function ClientApp ($resource) {
        var resourceUrl =  'api/client-apps/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' },
            'exportQuestions': {
                url: 'api/client-app/:id/export-questions',
                params: {
                    ak: null
                },
                method: 'GET',
                headers: {
                    accept: 'application/json' //or whatever you need
                },
                responseType: 'arraybuffer',
                cache: false,
                transformResponse: function(data, headers) {
                    var jsonData = null;
                    if (data) {
                        jsonData = new Blob([data], {
                            type: 'application/json' //or whatever you need, should match the 'accept headers' above
                        });
                    }

                    //server should sent content-disposition header
                    var fileName = getFileNameFromHeader(headers);
                    var result = {
                        blob: jsonData,
                        fileName: fileName
                    };

                    return {
                        response: result
                    };

                    function getFileNameFromHeader(headers) {
                        var contentDispositionHeader = headers('Content-Disposition');
                        var result = contentDispositionHeader.split(';')[1].trim().split('=')[1];
                        return result.replace(/"/g, '');
                    }
                }
            }
        });
    }
})();

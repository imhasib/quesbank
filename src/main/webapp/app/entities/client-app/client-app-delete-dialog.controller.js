(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('ClientAppDeleteController',ClientAppDeleteController);

    ClientAppDeleteController.$inject = ['$uibModalInstance', 'entity', 'ClientApp'];

    function ClientAppDeleteController($uibModalInstance, entity, ClientApp) {
        var vm = this;

        vm.clientApp = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ClientApp.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

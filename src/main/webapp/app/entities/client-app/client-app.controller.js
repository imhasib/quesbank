(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('ClientAppController', ClientAppController);

    ClientAppController.$inject = ['$state', 'ClientApp', 'ParseLinks', 'AlertService', 'paginationConstants', 'pagingParams'];

    function ClientAppController($state, ClientApp, ParseLinks, AlertService, paginationConstants, pagingParams) {

        var vm = this;

        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.filter = pagingParams.filter;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;

        loadAll();

        function loadAll () {
            var params = {
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
                sort: sort(),
                "name.contains": vm.filter.search,
                "createdBy.equals": vm.filter.createdBy,
                "clientAppId.equals": vm.filter.clientAppId
            };

            if (vm.filter.createdDateFrom) {
                params["createdDate.greaterThan"] = vm.filter.createdDateFrom + "T00:00:00Z";
            }
            if (vm.filter.createdDateTo) {
                params["createdDate.lessThan"] = vm.filter.createdDateTo + "T23:59:59Z";
            }

            ClientApp.query(params, onSuccess, onError);

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.clientApps = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        vm.export = function(clientApp) {
            var data = {id: clientApp.id};
            ClientApp.exportQuestions(data).$promise.then(
                function(data) {
                    var blob = data.response.blob;
                    var fileName = data.response.fileName || 'document.json';

                    var url = URL.createObjectURL(blob),
                        a = document.createElement('a');

                    a.href = url;
                    a.download = fileName;
                    a.target = '_blank';
                    a.click();

                });
        }
    }
})();

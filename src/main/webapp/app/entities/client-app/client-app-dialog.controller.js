(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('ClientAppDialogController', ClientAppDialogController);

    ClientAppDialogController.$inject = ['$timeout', '$scope', '$http', '$uibModalInstance', 'entity', 'ClientApp', 'Question'];

    function ClientAppDialogController ($timeout, $scope, $http, $uibModalInstance, entity, ClientApp, Question) {
        var vm = this;

        vm.clientApp = entity;
        vm.clear = clear;
        vm.save = save;
        vm.questions = Question.query();

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.clientApp.id !== null) {
                ClientApp.update(vm.clientApp, onSaveSuccess, onSaveError);
            } else {
                ClientApp.save(vm.clientApp, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('quesbankApp:clientAppUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.loadUsers = function (query) {
            return $http.get('api/search-users/' + query);
        }
    }
})();

(function () {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('ClientAppFilterDialogController', ClientAppFilterDialogController);

    ClientAppFilterDialogController.$inject = ['$timeout', '$uibModalInstance', 'filterParams', '$http', 'AlertService', 'User', 'Principal'];

    function ClientAppFilterDialogController($timeout, $uibModalInstance, filterParams, $http, AlertService, User, Principal) {
        var vm = this;

        vm.filter = filterParams;
        vm.users = [];

        vm.reset = reset;
        vm.applyFilters = applyFilters;
        vm.clear = clear;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function reset() {
            vm.filter = {
                search: null,
                createdBy: null
            };
        }

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function applyFilters() {
            vm.isFiltering = true;
            $uibModalInstance.close(vm.filter);
            vm.isFiltering = false;
        }

        vm.configureDatePicker = function () {
            $('#sandbox-container .input-daterange').datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true
            });
        }

        function getUsers() {
            User.query({
                sort: 'login,asc'
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.users = data;
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        if (Principal.hasAnyAuthority(['clientApp.administration'])) {
            getUsers();
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('client-app', {
            parent: 'entity',
            url: '/client-app?page&sort&search',
            data: {
                authorities: ['clientApp.read'],
                pageTitle: 'ClientApps'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/client-app/client-apps.html',
                    controller: 'ClientAppController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: {
                    value: null,
                    squash: true
                },
                createdBy: {
                    value: null,
                    squash: true
                },
                createdDateFrom: {
                    value: null,
                    squash: true
                },
                createdDateTo: {
                    value: null,
                    squash: true
                }
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),

                        filter: {
                            search: $stateParams.search,
                            createdBy: $stateParams.createdBy,
                            createdDateFrom: $stateParams.createdDateFrom,
                            createdDateTo: $stateParams.createdDateTo
                        }
                    };
                }]
            }
        })
            .state('client-app.detail', {
                parent: 'client-app',
                url: '/client-app/{id}',
                data: {
                    authorities: ['clientApp.read'],
                    pageTitle: 'Category'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/client-app/client-app-detail.html',
                        controller: 'ClientAppDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ClientApp', function ($stateParams, ClientApp) {
                        return ClientApp.get({id: $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'client-app',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
        .state('client-app.new', {
            parent: 'client-app',
            url: '/new',
            data: {
                authorities: ['clientApp.write']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-app/client-app-dialog.html',
                    controller: 'ClientAppDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                packageName: null,
                                description: null,
                                status: null,
                                id: null,
                                settings: {
                                    qpe: 10,
                                    et: 5,
                                    m: 1.0,
                                    nme: false,
                                    nm: 0.5
                                }
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('client-app', null, { reload: 'client-app' });
                }, function() {
                    $state.go('client-app');
                });
            }]
        })
        .state('client-app.edit', {
            parent: 'client-app',
            url: '/{id}/edit',
            data: {
                authorities: ['clientApp.write']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-app/client-app-dialog.html',
                    controller: 'ClientAppDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md',
                    resolve: {
                        entity: ['ClientApp', function(ClientApp) {
                            return ClientApp.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('client-app', null, { reload: 'client-app' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('client-app.delete', {
            parent: 'client-app',
            url: '/{id}/delete',
            data: {
                authorities: ['clientApp.write']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-app/client-app-delete-dialog.html',
                    controller: 'ClientAppDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ClientApp', function(ClientApp) {
                            return ClientApp.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('client-app', null, { reload: 'client-app' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
            .state('client-app.filter', {
                parent: 'client-app',
                url: '/filter',
                params: {
                    search: {
                        value: null,
                        squash: true
                    },
                    createdBy: {
                        value: null,
                        squash: true
                    },
                    createdDateFrom: {
                        value: null,
                        squash: true
                    },
                    createdDateTo: {
                        value: null,
                        squash: true
                    }
                },
                data: {
                    authorities: ['question.read']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/client-app/client-app-filter-dialog.html',
                        controller: 'ClientAppFilterDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'md',
                        resolve: {
                            filterParams: ['ClientApp', function (ClientApp) {
                                return {
                                    search: $stateParams.search,
                                    createdBy: $stateParams.createdBy,
                                    createdDateFrom: $stateParams.createdDateFrom,
                                    createdDateTo: $stateParams.createdDateTo
                                };
                            }]
                        }
                    }).result.then(function (filter) {
                        var params = Object.assign({page: 1}, filter);
                        $state.go('client-app', params, {location: true, reload: 'client-app'});
                    }, function () {
                        $state.go('^');
                    });
                }]
        });
    }
})();

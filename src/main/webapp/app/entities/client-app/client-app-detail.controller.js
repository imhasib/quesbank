(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('ClientAppDetailController', ClientAppDetailController);

    ClientAppDetailController.$inject = ['$scope', '$rootScope', 'previousState', 'entity', 'ClientApp'];

    function ClientAppDetailController($scope, $rootScope, previousState, entity, ClientApp) {
        var vm = this;

        vm.clientApp = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('quesbankApp:clientAppUpdate', function(event, result) {
            vm.clientApp = result;
        });
        $scope.$on('$destroy', unsubscribe);

        vm.export = function(clientApp) {
            var data = {id: clientApp.id};
            ClientApp.exportQuestions(data).$promise.then(
                function(data) {
                    //using saveAs.js (part of upcoming HTML5 API, but so far a polyfill)
                    var blob = data.response.blob;
                    var fileName = data.response.fileName || 'document.json';
                    // SaveAs is available at FileServer.js from https://github.com/eligrey/FileSaver.js/
                    saveAs(blob, fileName);
                });
        }
    }
})();

(function() {
    'use strict';
    angular
        .module('quesbankApp')
        .factory('Category', Category);

    Category.$inject = ['$resource'];

    function Category ($resource) {
        var resourceUrl =  'api/categories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' },

            'path': {
                method: 'GET',
                url: resourceUrl + '/path',
                isArray: true
            },
            'addQuestions': {url: 'api/category/add-questions', method: 'POST', isArray: true},
            // 'getQuestions': { url: 'api/client-app/:id/questions', method: 'GET', isArray: true},
        });
    }
})();

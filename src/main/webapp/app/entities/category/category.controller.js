(function() {
    'use strict';

    angular
        .module('quesbankApp')
        .controller('CategoryController', CategoryController);

    CategoryController.$inject = ['Category', 'ParseLinks', 'AlertService', 'paginationConstants', 'path' ,'$state', '$stateParams'];

    function CategoryController(Category, ParseLinks, AlertService, paginationConstants, path, $state, $stateParams) {

        var vm = this;

        vm.path = path;
        vm.categories = [];
        vm.loadPage = loadPage;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.page = 0;
        vm.links = {
            last: 0
        };
        vm.predicate = 'id';
        vm.reset = reset;
        vm.reverse = true;

        vm.parentId = $stateParams.parent;

        loadAll();

        function loadAll () {
            Category.query({
                page: vm.page,
                size: vm.itemsPerPage,
                sort: sort(),
                "parent.equals": vm.parentId
            }, onSuccess, onError);
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                for (var i = 0; i < data.length; i++) {
                    vm.categories.push(data[i]);
                }
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }


            // Category.ancestors({id: 6}, function (data) {
            //     console.log(data);
            // })
            //
            // Category.tree({id: 3}, function (data) {
            //     console.log(data);
            // })
        }

        function reset () {
            vm.page = 0;
            vm.categories = [];
            loadAll();
        }

        function loadPage(page) {
            vm.page = page;
            loadAll();
        }

        vm.filterQuestionByCategory = function (category) {
            var filter = {
                search: null,
                // tags: category.tags,
                status: null,
                createdBy: null,
                cid: category.id
            };
            $state.go('question', filter, { location: true, reload: 'question' });
        }
    }
})();

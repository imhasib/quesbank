## Prerequisite:
* Docker
## Guide line:
* Start Docker
* Ensure database URL is correct in application-prod.yml configuration file or commandline argument. Provide
    * Correct IP address/ Machine name / docker container tag
    * Database name  
    * User name & password
    * User must have right permission to access.
    * Example: For mysql
    
            jdbc:mysql://192.168.10.211:3306/quesbank?useUnicode=true&characterEncoding=utf8&useSSL=false    
    * docker name could be used instead of IP address. for example if container for the database name is quesbank-mysql then URL should be:
        
            jdbc:mysql://quesbank-mysql:3306/quesbank?useSSL=false
* Create Docker image of quesbank.
* Run container of the quesbank image. 

## Deploy project quesbank: 
We can deploy project quesbank in many different ways. Some are discussed bellow. 

First we should create docker image then run it with command or docker-compose. Docker image could be created in two ways. 

   * Using Dockerfile or,
   * Using dockerfile-maven-plugin & docker compose.

#### 1. Dockerfile: (Recommended)
There should be a Dockerfile in the directory "src/main/docker". To create Docker image using docker file first we have to generate war/jar file of quesbank project.
1. Go to the project directory and generate war/jar file. Run:

        mvn install -Pprod
   In the project directory a "target" folder should be created. There should be a war and Dockerfile.
2. Then we have to create docker image. From the project directory go to target folder and run:
        
        docker build -t quesbank:latest .  # remember the dot(.) specify the location of Dockerfile.
    An docker image name "quesbank" with tag "latest" should be created. To check the list of all images, run:
        
        docker image ls
3. Run the container:
        
        docker run -p 8080:8080 --name quesbank -e SPRING_PROFILES_ACTIVE=prod,swagger -e JHIPSTER_SLEEP=10 -e SPRING_DATASOURCE_URL=jdbc:mysql://192.168.10.211:3306/quesbank?useSSL=false -e SPRING_DATASOURCE_USERNAME=quesbank -e SPRING_DATASOURCE_PASSWORD=123456 -d quesbank
    
   Container also could be run using docker-compose file app.yml located in the src/main/docker folder of your application, run:
   
        docker-compose -f src/main/docker/app.yml up
   To check the al container list, run:
        
        docker container ls -a
   To check the logs of quesbank, Run:
        
        docker container logs quesbank
        
    

#### 2. dockerfile-maven-plugin & docker-compose:
We can also create the docker image by docker-compose. To create a Docker image of QuesBank, and push it into your Docker registry:

    With Maven, type: ./mvnw package -Pprod dockerfile:build
    With Gradle, type: ./gradlew bootRepackage -Pprod buildDocker

This will package your application with the prod profile, and install the image.
On Windows, due to lack of named pipes, you may have to tune settings for Docker and turn on “Expose daemon on tcp://localhost:2375 without TLS”.
To run this image, use the Docker Compose configuration located in the src/main/docker folder of your application:

    docker-compose -f src/main/docker/app.yml up
This command will start up your application and the services it relies on (database, search engine, JHipster Registry…). 
Or we can use the bellow command to run the container:
    
    docker run -p 8080:8080 --name quesbank -e SPRING_PROFILES_ACTIVE=prod,swagger -e JHIPSTER_SLEEP=10 -d quesbank 

#### 3. Deploy using docker-compose with mysql container:
We can also use mysql docker image. To run quesbank container along with mysql container, run: 
* Build war file: (if war file is already generated, ignore it) 
    
        mvn package -Pprod dockerfile:build
        
* Pull mysql image from docker-hub repository. run:

        docker pull mysql:5.7.20
* Then run the docker-compose file:
    
        docker-compose -f src/main/docker/app-quesbank-mysql.yml up
Warning: Some time mysql server takes time to start. So quesbank can't establish connection with mysql and catch an error "CommunicationsException: Communications link failure"
Wait some moment Then start the quesbank container. run:
    
    docker container start quesbank

#### 4. Manually deploy quesbank with mysql container:
* Lets assume quesbank image is ready. Pull docker image for Mysql, run:
    
        docker pull mysql:5.7.20 # https://hub.docker.com/_/mysql/ 
* Run mysql container:

        docker run -p 3306:3306 --name quesbank-mysql -e MYSQL_DATABASE=quesbank -e MYSQL_USER=root -e MYSQL_ROOT_PASSWORD=1234 -d mysql:5.7.20
        
  If port no 3306 of host machine has already been occupied by the mysql or some another app. Then use 3307:3306.
* Run quesbank container:
 
        docker run -p 8080:8080 --name quesbank --link quesbank-mysql:mysql -e SPRING_PROFILES_ACTIVE=prod,swagger -e SPRING_DATASOURCE_URL=jdbc:mysql://quesbank-mysql:3306/quesbank?useSSL=false -e SPRING_DATASOURCE_USERNAME=root -e SPRING_DATASOURCE_PASSWORD=1234 -e JHIPSTER_SLEEP=10 -d quesbank
        
Warning: Some time mysql server takes time to start. So quesbank can't establish connection with mysql and catch an error "CommunicationsException: Communications link failure"
Wait some moment Then start the quesbank container. run:
    
    docker container start quesbank


## Reference URL:
* [docker get started](https://docs.docker.com/get-started/part2/#recap-and-cheat-sheet-optional)
* [mysql docker hub repository](https://hub.docker.com/_/mysql/)
* [openjdk docker hub repository](https://hub.docker.com/_/openjdk/)
* [jhipster docker compose](http://www.jhipster.tech/docker-compose/)

Important Commands:
--------------------
    docker build -t friendlyhello .  # Create image using this directory's Dockerfile
    docker run -p 4000:80 friendlyhello  # Run "friendlyname" mapping port 4000 to 80
    docker run -d -p 4000:80 friendlyhello         # Same thing, but in detached mode
    docker container ls                                # List all running containers
    docker container ls -a             # List all containers, even those not running
    docker container logs friendlyhello           # To see the logs of the container
    docker container stop <hash>           # Gracefully stop the specified container
    docker container kill <hash>         # Force shutdown of the specified container
    docker container rm <hash>        # Remove specified container from this machine
    docker container rm $(docker container ls -a -q)         # Remove all containers
    docker image ls -a                             # List all images on this machine
    docker image rm <image id>            # Remove specified image from this machine
    docker image rm $(docker image ls -a -q)   # Remove all images from this machine
    docker login             # Log in this CLI session using your Docker credentials
    docker tag <image> username/repository:tag  # Tag <image> for upload to registry
    docker push username/repository:tag            # Upload tagged image to registry
    docker run username/repository:tag                   # Run image from a registry
    docker container exec -it quesbank-mysql bash   # to enter command line interface

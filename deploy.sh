#!/usr/bin/env bash
# check out latest version
git pull origin master

# clean build for production
./mvnw -Pprod clean package #-DskipTests=true

# delete ROOT directory from tomcat
rm -rf /opt/tomcat/webapps/ROOT/

# copy war to tomcat
cp target/quesbank-0.0.1-SNAPSHOT.war.original /opt/tomcat/webapps/ROOT.war

# restart tomcat
systemctl start tomcat.service
